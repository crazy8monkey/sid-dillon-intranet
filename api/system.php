<?php

class System extends API {
	
	private $summaryEmailReminder;
	private $urgentEmailSettings;
	
	private $FacebookPosts;
	private $TwitterPosts;
	private $BlogPosts;
	
	private $startDate;
	private $endDate;
	
	private $SQLCount = array();
	
	
	private $newReferralReportCount = array();
	private $soldReferralReportCount = array();
	private $paidReferralReportCount = array();
	private $lostReferralReportCount = array();	
	
	public function __construct() {
		parent::__construct();
		
		set_time_limit(0);
		ini_set('memory_limit', '-1');
	}	
	
	private function getRenderedHTML($path) {
	    ob_start();
	    include($path);
	    $var=ob_get_contents(); 
	    ob_end_clean();
	    return $var;
	}	
		
	public function GET($type) {
    	switch($type) {
        	case "session":
          		Session::init();
			
				$loggedInValue = "";
				$userTypeValue = "";
				$userStoreValue = "";
				
				$loggedIn = Session::get('loggedIn');
				$userTypeLoggedIn = Session::get('user');
				$userStoreID = Session::get('storeID');
				
				if($loggedIn == false) {
					$loggedInValue = false;
				} else {
					$loggedInValue = true;
				}
				echo json_encode(array('CurrentSessionLoggedIn' => $loggedInValue,
									   "CurrentSessionUser" => Session::get('user'),
									   "CurrentSessionUserStore" => Session::get('storeID'),
									   "CurrentSessionUserType" => Session::get('userType')));
	          break;
         case "week":
	          echo json_encode(array('CurrentWeek' => $this -> time -> getCurrentWeek()));
	          break;
          
         case "settings": 
	          echo json_encode(array('settings' => $this -> db -> select('SELECT * FROM settings')));
	          break;
          
         case "logs":  
	          echo json_encode(array("errorLogs" => Log::render()));
	          break;
          
         case "php": 
	          ob_start();
			  phpinfo();
			  $phpInfo = ob_get_contents();
			  ob_get_clean();
	
			  echo json_encode(array("systemInfo" => $phpInfo));
	          break;
          
         case "currentsession": 
         	Session::init();
		 	echo json_encode(array("currentSessionInfo" => $_SESSION));
          	break;
		 case "path":
			 echo json_encode(array("systempath" => PATH));
			 break; 
		 case "referralreporting":
			 echo json_encode(array("referralreports" => $this -> db -> select('SELECT * FROM exportdatafilehistory')));
			 break; 
		  
      }
		
	}
		
	public function PUT($type) {
		//get post data
		$input = file_get_contents('php://input');
		$postData = array();
		parse_str($input, $postData);
		
      	switch($type) {
          case "main":
            
            $this -> summaryEmailReminder = $postData['summaryEmailReminder'];
			$this -> urgentEmailSettings = $postData['urgentPostEmail'];
			  
			if($this -> ValidateMainSettings()) {
				
				$postData = array('SummaryEmailReminder' => $this -> summaryEmailReminder,
								  'urgentEmail' => $this -> urgentEmailSettings);
				
				$this -> db -> update("settings", $postData, array("settingsID" => 1));
				$this -> json -> outputJqueryJSONObject("updatedSetings", "Setings updated");	
				
			}
            
            break;
          case "todo":
            $this -> FacebookPosts = $postData["facebookpost"];
			$this -> TwitterPosts = $postData["toDoTwitter"];
			$this -> BlogPosts = $postData["blogpost"];		
			
			if($this -> ValidateToDoSettings()) {
				
				$postData = array("facebookPosts" => $this -> FacebookPosts, 
								  "TwitterPosts" => $this -> TwitterPosts,
								  "blogPost" => $this -> BlogPosts);
				$this -> db -> update("settings", $postData, array("settingsID" => 1));
				$this -> json -> outputJqueryJSONObject("updatedSetings", "To Do list setings updated");
				
			}
            break;
        }

		
	}

	private function referralStatus($a, $b) {
		$t1 = $a['ReferralStatus'];
		$t2 = $b['ReferralStatus'];
		return $t1 - $t2;
	}  

	public function POST($type) {
		try {
		$input = file_get_contents('php://input');
		$postData = array();
		parse_str($input, $postData);
		
		switch($type) {
			case "logout":
				Session::init();
				Session::destroy();
				break;
			case "reporting":
				
				switch($_GET['type']) {
					case "referrals":
						$this -> startDate = $postData['reportingStartDate'];
						$this -> endDate = $postData['reportingEndDate'];
						
						
						
						
						
						
						if(!empty($this -> startDate) and !empty($this -> endDate)) {
							$sqlStartDate = explode("/", $this -> startDate);
							$sqlEndDate = explode("/", $this -> endDate);
								
							$StartRange = $sqlStartDate[2] . '-' . $sqlStartDate[0]. '-' . $sqlStartDate[1];
							$EndRange = $sqlEndDate[2] . '-' . $sqlEndDate[0]. '-' . $sqlEndDate[1];
							
							$resultIDS = NULL;
							
							if(isset($postData['newReferrals'])) {			
								$resultIDS .= '0,'; 							
							}
							
							if(isset($postData['soldReferrals'])) {
								$resultIDS .= '1,';						
							} 
							
							if(isset($postData['paidReferrals'])) {											
								$resultIDS .= '2,';							
							} 
							
							if(isset($postData['lostReferrals'])) {											
								$resultIDS .= '3,';										
							}
							
							
							
							if($resultIDS != NULL) {
								$storeIDS = NULL;
								if(isset($postData['storeID'])) {
									foreach($postData['storeID'] as $storeID) {
										$storeIDS .= $storeID . ',';
									}
								}		
								
								if($storeIDS != NULL) {
									$orderByClause = 'ORDER BY stores.name ASC';
								} else {
									$orderByClause = 'ORDER BY referrals.submittedDate ASC';
								}
								
								
								$this -> SQLCount = $this -> db -> select("SELECT *, (stores.name) StoreName, (CASE
																												WHEN referrals.Status = 0 THEN 'Active'
																												WHEN referrals.Status = 1 THEN 'Sold'
																												WHEN referrals.Status = 2 THEN 'Paid'
																												WHEN referrals.Status = 3 THEN 'Inactive'
																										   END) ReferralStatus FROM referrals 
																						  LEFT JOIN users ON referrals.salesmanUserID = users.userID 
																						  LEFT JOIN referralmembers ON referrals.memberID = referralmembers.ClubID
																						  LEFT JOIN stores ON referrals.storeID = stores.id
																						  WHERE referrals.submittedDate BETWEEN '" .$StartRange. "' AND '" .$EndRange. "' AND referrals.Status IN (" . rtrim($resultIDS, ',') . ") " . $orderByClause);	
							}
							
							
						
						}
						
						
						if($this -> ValidateReferralReport()) {
							
							$data_array = array();
								
							
							foreach($this -> SQLCount as $singleReferral) {
								
								$submittedDate = explode("-", $singleReferral['submittedDate']);
								$soldDateString = NULL;
								if(!empty($singleReferral['isSoldDate'])) {
									$SoldDate = explode("-", $singleReferral['isSoldDate']);
									$soldDateString = $SoldDate[1] . '/'. $SoldDate[2] . '/'. $SoldDate[0];
								}
								
								
								array_push($data_array, array('RF'. $singleReferral['referralID'],
															  $singleReferral['soldStockNumber'],
															  $submittedDate[1] . '/'. $submittedDate[2] . '/'. $submittedDate[0],
															  $soldDateString,
															  $singleReferral['dealNumber'],
															  $singleReferral['FirstName'] . ' ' . $singleReferral['LastName'],
															  $singleReferral['memberFirstName'] . ' ' . $singleReferral['memberLastName'],
															  "SD" . $singleReferral['memberID'],
															  $singleReferral['StoreName'],
															  $singleReferral['firstName'] . ' ' . $singleReferral['lastName'],
															  $singleReferral['SaleComments'],
															  $singleReferral['ReferralStatus'])); 
							}
							
							
						
							//Column headers
							$csvContent = "Referral ID,Stock#,Date Submitted,Sold Date,Deal#,Referral Name,Member,Member ID,Store,Sales Rep,Sales Comments,Referral Status\n";
							
							//putting it all together
							
							foreach ($data_array as $record){
							    $csvContent.= $record[0]. ',' . $record[1] . ',' . $record[2] . ',' . $record[3] . ',' . $record[4] . ',' . $record[5] . ',' . $record[6] . ',' . $record[7] .',' . $record[8] .',' . $record[9] .',' . $record[10] .',' . $record[11]. "\n"; //Append data to csv
							}
							
							$time = explode(":", date("H:i:s", $this -> time -> NebraskaTime()));
							
							$fileExt = NULL;
							if($postData['fileType'] == '0') {
								$fileExt = ".csv";
							} else {
								$fileExt = ".xlsx";
							}
							
							$csvFileName = "Referrals-" . $sqlStartDate[0] . '.' . $sqlStartDate[1] . '.' . $sqlStartDate[2] . '-'. $sqlEndDate[0] . '.' . $sqlEndDate[1] . '.' . $sqlEndDate[2] . '-' .  '('. date("Y-m-d", $this -> time -> NebraskaTime()) . '-' . $time[0] . '.' . $time[1] . '.' .$time[2] . ')'. $fileExt;
							$csvFile = fopen($csvFileName ,'w');
							fwrite ($csvFile, $csvContent);
							fclose ($csvFile);
							

							
							copy($csvFileName, 'view/reporting/'. $csvFileName);
							unlink($csvFileName);
							
							
							Session::init();
							//fileType
							//0 = csv
							//1 = xlsx							
							$this -> db -> insert('exportdatafilehistory', array('fileName' => $csvFileName,
																				 'fileType' => (int)$postData['fileType'],
																				 'createdBy' => Session::get('user'),
																				 'createdDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
													 							 'createdTime' => date("H:i:s", $this -> time -> NebraskaTime())));
							
							//$this -> DownloadAttachment('view/reporting/'. $csvFileName);
							
							
							$this -> json -> outputJqueryJSONObject('success', array("StartDateSQL" => $sqlStartDate[2] . '-' . $sqlStartDate[0]. '-' . $sqlStartDate[1],
																					 "EndDateSQL" => $sqlEndDate[2] . '-' . $sqlEndDate[0]. '-' . $sqlEndDate[1],
																					 "ArrayCount" => $data_array, //$this -> SQLCount[0],//$referralDateRange -> fetchAll(),
																					 "NewFileDownload" => 'view/reporting/' . $csvFileName));
							
						}
						break;						
						
				}
				break;	
				
		}	
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "System API - Post Error: " . $e->getMessage();
			$TrackError -> type = "System API - POST ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		}		
	}
	
	public function uploadImages() {
		
		$funcNum = $_GET['CKEditorFuncNum'] ;
		
		$newFileName = Image::renameImage($_FILES["upload"]["name"]);
		
		$url = PATH . 'view/images/' . $newFileName;
		$message = '';
		
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
		
		Image::moveImage($newFileName, $_FILES["upload"]["tmp_name"], "view/images/");
		Image::compress($url, $url, 70);
	} 
	
	
	private function ValidateReferralReport() {
		if(!empty($this -> startDate) and !empty($this -> endDate)) {
			$startDateFormat = explode("/", $this -> startDate);
			$endDateFormat = explode("/", $this -> endDate);
			
			$startDate = new DateTime($startDateFormat[2] . '-' . $startDateFormat[0] . '-' . $startDateFormat[1]);
			$endDate = new DateTime($endDateFormat[2] . '-' . $endDateFormat[0] . '-' . $endDateFormat[1]);			
		}

		
		if($this -> validate -> emptyInput($this -> startDate)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Start Date"));
			return false;
		} else if($this -> validate -> emptyInput($this -> endDate)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("End Date"));
			return false;
		} else if ($startDate > $endDate) {
			$this -> json -> outputJqueryJSONObject('errorMessage', 'Start date cannot go beyond the End Date.');
			return false;
		} else if(count($this -> SQLCount) == 0) {
			$this -> json -> outputJqueryJSONObject('errorMessage', 'There is no data to create a report from');
			return false;
		}
		return true;
	}
	
	private function ValidateMainSettings() {
		if($this -> validate -> emptyInput($this -> summaryEmailReminder)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Summary Email Reminder"));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> summaryEmailReminder)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if($this -> validate -> emptyInput($this -> urgentEmailSettings)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Urgent Post Email"));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> urgentEmailSettings)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
		}
		return true;
	}
	
	private function ValidateToDoSettings() {
		if($this -> validate -> emptyInput($this -> FacebookPosts)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Facebook Posts"));
			return false;
		} else if($this -> validate -> onlyNumbers($this -> FacebookPosts)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Facebook Posts: Only Numbers");
			return false;
		} else if($this -> validate -> emptyInput($this -> TwitterPosts)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Twitter Posts"));
			return false;
		} else if($this -> validate -> onlyNumbers($this -> TwitterPosts)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Twitter Posts: Only Numbers");
			return false;
		} else if($this -> validate -> emptyInput($this -> BlogPosts)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Blog Posts"));
			return false;
		} else if($this -> validate -> HasLetters($this -> BlogPosts)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Blog Posts: Only Numbers");
			return false;
		}
		return true;
		
	}

	private function DownloadAttachment($downloadFile) {
		header('Content-Description: File Transfer');
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename='".basename($downloadFile)."'");
		header("Content-Transfer-Encoding: binary");
		header("Expires: 0");//            header('Content-Disposition: attachment; 
		header("Pragma: public");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Length: ' . filesize($downloadFile)); //Remove
		
		//flush();
		readfile($downloadFile); 
	   	
	}
	
}