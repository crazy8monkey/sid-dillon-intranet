<?php

class Stores extends API {
	
	private $name;
	private $facebook;
	private $twitter;
	private $youtube;
	private $instagram;
	private $googlePlus;
	private $street;
	private $city;
	private $state;
	private $zip;
	private $phone;
	private $ReferralStoreOption;
	private $ReferralDefaultEmail;
	private $ReferralDefaultUser;
	
	public function __construct() {
		parent::__construct();
	}	
	
	public function GET($type, $id = NULL) {
		$settings = $this -> db -> prepare('SELECT * FROM settings WHERE settingsID = :id');
		$settings -> execute(array('id' => 1));
		$getSettings = $settings -> fetch();
		
		$startWeek = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
		$endWeek = date('Y-m-d',time()+( 6 - date('w'))*24*3600);
		
      	switch($type) {
			case "FacebookPostList":
					$facebookPosts = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND StoreID = :storeID');
					
					$facebookPosts -> execute(array(':storeID' => $id,
							   ':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
					echo json_encode(array('StoreFacebookList' => $facebookPosts -> fetchAll()));		
				break;	
				
			case "TwitterPostsList":
				$twitterPosts = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND StoreID = :storeID');
					
				$twitterPosts -> execute(array(':storeID' => $id,
							   ':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
				echo json_encode(array('StoreTwitterList' => $twitterPosts -> fetchAll()));
				break;
			
			case "BlogPostsList":
				$blogPosts = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE socialMediaType = 3 AND isDeclined = 0 AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND StoreID = :storeID');
					
				$blogPosts -> execute(array(':storeID' => $id,
							   ':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
							   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
		
				echo json_encode(array('StoreBlogList' => $blogPosts -> fetchAll()));
				break;	
			
			case "SocialMediaPostProgress":
				if(isset($id)) {
					$progress = $this -> db -> prepare('SELECT id, name, ( 
														    (
														        (SELECT LEAST(COUNT(*), (SELECT facebookPosts FROM settings)) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = :storeID) + 
														        (SELECT LEAST(COUNT(*), (SELECT TwitterPosts FROM settings)) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = :storeID) + 
														        (SELECT LEAST(COUNT(*), (SELECT blogPost FROM settings)) FROM socialmediaposts WHERE socialMediaType = 3 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = :storeID)
														    ) / (SELECT (facebookPosts + TwitterPosts + blogPost) TotalEntries FROM settings) 
														) TotalSubmittedContent,
														(SELECT facebookPosts FROM settings) RequiredFacebook,
														(SELECT TwitterPosts FROM settings) RequiredTwitter,
														(SELECT blogPost FROM settings) RequiredBlog,
														(SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = :storeID) SumittedFacebook,
														(SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = :storeID) SumittedTwitter,
														(SELECT COUNT(*) FROM socialmediaposts WHERE socialMediaType = 3 AND isDeclined = 0 AND submittedDate BETWEEN (:startWeek) AND (:endWeek) AND StoreID = :storeID) SumittedBlog FROM stores WHERE id = :storeID');
					$progress -> execute(array(':storeID' => $id,
											   ':startWeek' => date('Y-m-d',time()+( 1 - date('w'))*24*3600),
											   ':endWeek' => date('Y-m-d',time()+( 6 - date('w'))*24*3600)));
	
					echo json_encode(array('StoreProgress' => $progress -> fetch()));	
					
				} else {
					
					
					$progress = $this -> db -> select('SELECT id, name, ceil(( 
														    (
														        (SELECT LEAST(COUNT(*), (SELECT facebookPosts FROM settings)) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN ("' . $startWeek . '") AND ("' . $endWeek . '") AND StoreID = stores.id) + 
														        (SELECT LEAST(COUNT(*), (SELECT TwitterPosts FROM settings)) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN ("' . $startWeek . '") AND ("' . $endWeek . '") AND StoreID = stores.id) + 
														        (SELECT LEAST(COUNT(*), (SELECT blogPost FROM settings)) FROM socialmediaposts WHERE socialMediaType = 3 AND isDeclined = 0 AND submittedDate BETWEEN ("' . $startWeek . '") AND ("' . $endWeek . '") AND StoreID = stores.id)
														    ) / (SELECT (facebookPosts + TwitterPosts + blogPost) TotalEntries FROM settings) * 100
														)) TotalSubmittedContent FROM stores WHERE id IN (' . $_GET['IDS'] .  ')');
					
					echo json_encode(array('UserStoreList' => $progress));
				}
				
				
				
				
				break;
				
        	case 'todo':
	            if(isset($id)) {
					$posts = $this -> db -> prepare('SELECT * FROM socialmediaposts WHERE StoreID=:StoreID and submittedDate >= :startWeek and submittedDate <= :endWeek AND isDeclined=0');
		        	$posts -> execute(array(':StoreID' => $id, 
		        					  ':startWeek' => $this -> time -> StartOfWeekSQL(), 
		        					  ':endWeek' => $this -> time -> EndOfWeekSQL()));
						
		        	$getPostCount = $posts -> rowCount();
					
					
					//percentage
					$totalItemsInToDoList = $getSettings['facebookPosts'] + $getSettings['TwitterPosts'] + $getSettings['blogPost'];
					$completedTasks = $getPostCount / $totalItemsInToDoList;
					
					//get facebook posts submitted
					$facebookSubmittedPending = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE StoreID=:StoreID AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND socialMediaType = 1 AND isDeclined = 0');
		        	$facebookSubmittedPending -> execute(array(':StoreID' => $id, 
		        					  ':startWeek' => $this -> time -> StartOfWeekSQL(), 
		        					  ':endWeek' => $this -> time -> EndOfWeekSQL()));
		        		
					//get twitter posts submitted
					$twitterSubmittedPending = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE StoreID=:StoreID AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND socialMediaType = 2 AND isDeclined = 0');
				    $twitterSubmittedPending -> execute(array(':StoreID' => $id, 
								        					  ':startWeek' => $this -> time -> StartOfWeekSQL(), 
								        					  ':endWeek' => $this -> time -> EndOfWeekSQL()));
				        
					//blog posts
					$blogPostSubmittedPending = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE StoreID=:StoreID AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND socialMediaType = 3 AND isDeclined=0');
				    $blogPostSubmittedPending -> execute(array(':StoreID' => $id, 
								        					   ':startWeek' => $this -> time -> StartOfWeekSQL(), 
								        					   ':endWeek' => $this -> time -> EndOfWeekSQL()));
					
					
					echo json_encode(array('StoreSingleToDo' => array('progressBarPercentage' => round($completedTasks * 100),
																	  'WeeklyFacebookPosts' => $facebookSubmittedPending -> fetchAll(),
																	  'WeeklyTwitterPosts' => $twitterSubmittedPending -> fetchAll(),
																	  'WeeklyBlogPosts' => $blogPostSubmittedPending -> fetchAll())));
				
				} else {
					$progressBarContent = array();
				
					$progress = $this -> db -> select('SELECT id, name, ceil(( 
														    (
														        (SELECT LEAST(COUNT(*), (SELECT facebookPosts FROM settings)) FROM socialmediaposts WHERE socialMediaType = 1 AND isDeclined = 0 AND submittedDate BETWEEN ("' . $startWeek . '") AND ("' . $endWeek . '") AND StoreID = stores.id) + 
														        (SELECT LEAST(COUNT(*), (SELECT TwitterPosts FROM settings)) FROM socialmediaposts WHERE socialMediaType = 2 AND isDeclined = 0 AND submittedDate BETWEEN ("' . $startWeek . '") AND ("' . $endWeek . '") AND StoreID = stores.id) + 
														        (SELECT LEAST(COUNT(*), (SELECT blogPost FROM settings)) FROM socialmediaposts WHERE socialMediaType = 3 AND isDeclined = 0 AND submittedDate BETWEEN ("' . $startWeek . '") AND ("' . $endWeek . '") AND StoreID = stores.id)
														    ) / (SELECT (facebookPosts + TwitterPosts + blogPost) TotalEntries FROM settings) * 100
														)) TotalSubmittedContent FROM stores');
					
					echo json_encode(array('storeList' => $progress));
				}
            
            break;
            
            case 'stores':
            	$progressBarContent = array();
			
			
                foreach($this -> db -> select("SELECT * FROM stores WHERE id IN (" . $_GET['IDS'] . ")") as $store) {
                    $posts = $this -> db -> prepare('SELECT * FROM socialmediaposts WHERE StoreID=:StoreID and submittedDate >= :startWeek and submittedDate <= :endWeek AND isDeclined=0');
                    $posts -> execute(array(':StoreID' => $store['id'], 
                                      ':startWeek' => $this -> time -> StartOfWeekSQL(), 
                                      ':endWeek' => $this -> time -> EndOfWeekSQL()));

                    $getPostCount = $posts -> rowCount();






                    $totalItemsInToDoList = $getSettings['facebookPosts'] + $getSettings['TwitterPosts'] + $getSettings['blogPost'];

                    $completedTasks = $getPostCount / $totalItemsInToDoList;



                    array_push($progressBarContent, array('storeID'=> $store['id'],
                                                          'storeName' => $store['name'],
                                                          'progressBarPercentage' => round($completedTasks * 100)));
                }

                echo json_encode(array('UserStoreList' => $progressBarContent));
            
            
            	break;
            	case 'admin':
		            if(isset($id)) {
						echo json_encode(array('StoreSingle' => $this -> db -> select('SELECT * FROM stores WHERE id=' . $id)));
					} else {
						echo json_encode(array('stores' => $this -> db -> select('SELECT * FROM stores')));	
					}	
            		break;
				case 'referral':
					if(isset($_GET['IDS'])) {
						echo json_encode(array('referralStores' => $this -> db -> select('SELECT id, name, (sum(referrals.PaidAmount)) totalSum, 
																								   (SELECT count(*) FROM referrals WHERE Status = 0 AND stores.id = referrals.storeID) ActiveCount, 
																								   (SELECT count(*) FROM referrals WHERE Status = 1 AND stores.id = referrals.storeID) SoldCount, 
																								   (SELECT count(*) FROM referrals WHERE Status = 2 AND stores.id = referrals.storeID) PaidCount, 
																								   (SELECT count(*) FROM referrals WHERE Status = 3 AND stores.id = referrals.storeID) InactiveCount FROM stores LEFT JOIN referrals ON stores.id = referrals.storeID WHERE stores.isReferralClubStore = 1 AND referrals.submittedDate >= "' . date('Y') . '-01-01" AND referrals.submittedDate <= "' . date('Y') . '-12-31" AND referrals.storeID IN (' . $_GET['IDS'] . ') GROUP By referrals.storeID')));
					} else {
						$referralStores = $this -> db -> prepare('SELECT id, name, (sum(referrals.PaidAmount)) totalSum, 
																								   (SELECT count(*) FROM referrals WHERE Status = 0 AND stores.id = referrals.storeID) ActiveCount, 
																								   (SELECT count(*) FROM referrals WHERE Status = 1 AND stores.id = referrals.storeID) SoldCount, 
																								   (SELECT count(*) FROM referrals WHERE Status = 2 AND stores.id = referrals.storeID) PaidCount, 
																								   (SELECT count(*) FROM referrals WHERE Status = 3 AND stores.id = referrals.storeID) InactiveCount FROM stores LEFT JOIN referrals ON stores.id = referrals.storeID WHERE isReferralClubStore = 1  AND referrals.submittedDate >= :startYear AND referrals.submittedDate <= :endYear GROUP BY referrals.storeID');
						
						$referralStores -> execute(array(":startYear" => date('Y') . '-01-01',
												   		 ":endYear" => date('Y') . '-12-31'));
												   
						echo json_encode(array('referralStores' => $referralStores -> fetchAll()));	
					}
					break;
					
				case 'referralAdminStores':
					echo json_encode(array('referralStores' => $this -> db -> select('SELECT * FROM stores WHERE id IN (' . $_GET['IDS'] . ')')));
					
					break;
        }
      
			
	}


	
	public function POST() {
		$input = file_get_contents('php://input');
		$postData = array();
		parse_str($input, $postData);
		
		$this -> name = $postData['storeName'];
		$this -> facebook = $postData['storeFacebookUrl'];
		$this -> twitter = $postData['storeTwitterURL'];
		$this -> youtube = $postData['storeYouTubeUrl'];
		$this -> instagram = $postData['storeInstagramUrl'];
		$this -> googlePlus = $postData['storeGooglePlusUrl'];
		$this -> street = $postData['storeStreet'];
		$this -> city = $postData['storeCity'];
		$this -> state = $postData['storeState'];
		$this -> zip = $postData['storeZip'];
		$this -> phone = $postData['storePhone'];
		
		
		
		if($this -> ValidateSingleStore()) {
			$isReferralClub = 0;
				
			if(isset($postData['isReferralClub'])) {
				$isReferralClub = 1;
			} else { 
				$isReferralClub = 0;
			}
			
			
			
			$this -> db -> insert('stores', array('name' => $this -> name, 
									  				   'facebookLink' => $this -> facebook,
									  				   'twitterURL' => $this -> twitter,
									  				   'youtubeURL' => $this -> youtube,
													   'instagramURL' => $this-> instagram, 
													   'GooglePlusURL' => $this -> googlePlus,
													   'Street' => $this -> street,
													   'City' => $this -> city,
													   'State' => $this-> state,
													   'Zip' => $this -> zip,
													   'Phone' => preg_replace("/[^0-9]/", "", $this -> phone),
													   'isReferralClubStore' => $isReferralClub));
													 
				$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> adminStoreList());
		}
		
	}
	
	public function PUT($id) {
		
		
		$input = file_get_contents('php://input');
		$postData = array();
		parse_str($input, $postData);
		
		$this -> name = $postData['storeName'];
		$this -> facebook = $postData['storeFacebookUrl'];
		$this -> twitter = $postData['storeTwitterURL'];
		$this -> youtube = $postData['storeYouTubeUrl'];
		$this -> instagram = $postData['storeInstagramUrl'];
		$this -> googlePlus = $postData['storeGooglePlusUrl'];
		$this -> street = $postData['storeStreet'];
		$this -> city = $postData['storeCity'];
		$this -> state = $postData['storeState'];
		$this -> zip = $postData['storeZip'];
		$this -> phone = $postData['storePhone'];
		
		if(isset($postData['isReferralClub'])) {
			$this -> ReferralStoreOption = $postData['isReferralClub'];	
		}
		
		
		$this -> ReferralDefaultEmail = $postData['referralEmailDefault'];	
		$this -> ReferralDefaultUser = $postData['referralUserDefault'];
		
		
		if($this -> ValidateSingleStore()) {
			
			$isReferralClub = 0;
			$ReferralClubDefaultUser = NULL;	
			
			if(isset($postData['isReferralClub'])) {
				$updateData = array('name' => $this -> name, 
									'facebookLink' => $this -> facebook,
									'twitterURL' => $this -> twitter,
									'youtubeURL' => $this -> youtube,
									'instagramURL' => $this-> instagram, 
									'GooglePlusURL' => $this -> googlePlus,
									'Street' => $this -> street,
									'City' => $this -> city,
									'State' => $this-> state,
									'Zip' => $this -> zip,
									'Phone' => preg_replace("/[^0-9]/", "", $this -> phone),
									'isReferralClubStore' => 1,
									'ReferralStoreEmailDefault' => $this -> ReferralDefaultEmail,
									'ReferralDefaultUser' => $this -> ReferralDefaultUser);	
				
				
				$this->db->update('stores', $updateData, array('id' => $id));	
			} else {
				$updateData = array('name' => $this -> name, 
									'facebookLink' => $this -> facebook,
									'twitterURL' => $this -> twitter,
									'youtubeURL' => $this -> youtube,
									'instagramURL' => $this-> instagram, 
									'GooglePlusURL' => $this -> googlePlus,
									'Street' => $this -> street,
									'City' => $this -> city,
									'State' => $this-> state,
									'Zip' => $this -> zip,
									'Phone' => preg_replace("/[^0-9]/", "", $this -> phone),
									'isReferralClubStore' => 0,
									'ReferralStoreEmailDefault' => NULL,
									'ReferralDefaultUser' => NULL);	
				
				
				$this->db->update('stores', $updateData, array('id' => $id));	
			}	
				
				
			$this -> json -> multipleJSONOjbects(array("storeUpdated" => "Store Updated",
												   "updatedStoreName" => $this -> name));
		}
	}
	
	public function DELETE($id) {
		$sth = $this -> db -> prepare("DELETE FROM stores WHERE id = :id");
		$sth -> execute(array('id' => $id));	
		
		$sth = $this -> db -> prepare("DELETE FROM permissions WHERE storeID = :id");
		$sth -> execute(array('id' => $id));
	}
	
	private function ValidateSingleStore() {
		if($this -> validate -> emptyInput($this -> name)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Store Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> facebook)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Facebook URL"));
			return false;
		} else if($this -> validate -> emptyInput($this -> twitter)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Twitter URL"));
			return false;
		} else if($this -> validate -> emptyInput($this -> youtube)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Youtube URL"));
			return false;
		} else if($this -> validate -> emptyInput($this -> instagram)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Instagram URL"));
			return false;
		} else if($this -> validate -> emptyInput($this -> googlePlus)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Google + URL"));
			return false;
		} else if($this -> validate -> emptyInput($this -> street)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Address Street"));
			return false;
		} else if($this -> validate -> emptyInput($this -> city)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Address City"));
			return false;
		} else if($this -> validate -> emptyInput($this -> state)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Address State"));
			return false;
		} else if($this -> validate -> emptyInput($this -> zip)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Address Zip"));
			return false;
		} else if($this -> validate -> emptyInput($this -> phone)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Phone #"));
			return false;
		} else if(isset($this -> ReferralStoreOption) and $this -> validate -> emptyInput($this -> ReferralDefaultEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Referral Default Email"));
			return false;
		} else if(isset($this -> ReferralStoreOption) and $this -> validate -> emptyDropDown($this -> ReferralDefaultUser)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Referral Default User"));
			return false;
		}
		return true;
		
	}

}