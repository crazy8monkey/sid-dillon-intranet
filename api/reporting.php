<?php

class Reporting extends API {
	
	public function __construct() {
		parent::__construct();
	}	
			
	public function CLEAR() {
		$reportingFiles = $this -> db -> select('SELECT * FROM exportdatafilehistory');
		
		$AllFiles = $reportingFiles;
		//echo count($reportingFiles -> fetchAll());
		if(count($reportingFiles) > 0) {
			
			foreach($AllFiles as $file) {
				if(file_exists('view/reporting/'. $file['fileName'])) {
					unlink('view/reporting/'. $file['fileName']);
				}
			}
			
			$sth = $this -> db -> prepare('TRUNCATE TABLE exportdatafilehistory');
        	$sth -> execute();
		}
		
	}



	
}