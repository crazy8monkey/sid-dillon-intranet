<?php

class API {
	
	public $postData = array();
	
    public function __construct() {
    	
        $this -> msg = new Message();
        $this -> json = new JSONparse();
        $this -> validate = new Validation();
		$this -> pages = new Pages();
        $this -> redirect = new Redirect();
		$this -> time = new Time();
		$this -> email = new Email();
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

    public function __sleep() {
        return array();
    }

    public function __wakeup() {
    	
        $this -> msg = new Message();
        $this -> json = new JSONparse();
        $this -> validate = new Validation();
        $this -> redirect = new Redirect();
		$this -> currentTime = new Time();
		$this -> email = new Email();
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }
    
	

}