<?php

class Website extends API {
	
	public function __construct() {
		parent::__construct();
	}	
		
	public function GET($store) {
		echo json_encode(array('StoreWebsites' => $this -> db -> select('SELECT * FROM websites WHERE storeID=' . $store)));
	}
		
	public function POST($id) {
		try {
		$input = file_get_contents('php://input');
		$postData = array();
		parse_str($input, $postData);
		
			
		$websiteID = $postData['websiteID'];
		$name = $postData['webName'];
		$thirdParty = $postData['thirdparty'];
					
		foreach($postData['webName'] as $key => $website) {
			if(!empty($postData['websiteID'][$key])) {
				$updateData = array('name' => $postData['webName'][$key],
									'ThirdParty' => $postData['thirdparty'][$key]);	
				$this->db->update('websites', $updateData, array('websiteID' => $postData['websiteID'][$key]));
								
			} else {
				$this -> db -> insert('websites', array('name' => $postData['webName'][$key], 
									  				  'ThirdParty' => $postData['thirdparty'][$key],
									  				  'storeID' => $id));
			}
		}	
		
		
		$this -> json -> outputJqueryJSONObject('success', 'Websites Updated');		
		
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Post Website Error: " . $e->getMessage();
			$TrackError -> type = "POST WEBSITE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("errorMessage", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("errorMessage", $e->getMessage());	
			}
		}
	}

	public function DELETE($id) {
		$sth = $this -> db -> prepare("DELETE FROM websites WHERE websiteID = :id");
		$sth -> execute(array('id' => $id));	
	}

}