<?php
class Inventory extends API {
	
	private $_csv;
	private $_inventoryList;
	private $_storesArray = array();
	
	public function __construct() {
		parent::__construct();
		$this -> mapCSVFile();
		
		
		set_time_limit(0);
		ini_set('memory_limit', '-1');
	}
		
	public function INSERT() {
		
		$vinNumberCheck = array();
		foreach($this -> _csv as $inventory) {
				
			if(!in_array($inventory['VIN'], $vinNumberCheck)) {
				
				$storeLine = str_replace("Sid Dillon ","",$inventory['Dealer Name']);
				
				
				$store = $this -> db -> prepare('SELECT * FROM stores WHERE name LIKE :name');
		        $store->execute(array(':name' => '%'.str_replace("BNH ","",$storeLine).'%'));
		        $getStore = $store -> fetch();
				
				
				
				$this -> db -> insert('inventorystaging', array('type' => $inventory['Type'], 
														  	 	'stock' => $inventory['Stock'],
														  	 	'vinNumber' => $inventory['VIN'],
														  	 	'year' => $inventory['Year'],
															 	'make' => $inventory['Make'], 
															 	'model' => $inventory['Model'],
																'body' => $inventory['Body'],
																'trim' => $inventory['Trim'],
																'exteriorColor' => $inventory['ExteriorColor'],
																'store' => $getStore['id'],
																'InventoryLineJSON' => base64_encode(json_encode($inventory)),
																'lastUpdated' => $inventory['Date_Modified']));				
			}			
			
			array_push($vinNumberCheck, $inventory['VIN']);	
		}
		//update production table
		$this -> BulkUpdateInventory();
	}
		
	public function CLEAR() {
		$sth = $this -> db -> prepare('TRUNCATE TABLE inventorystaging');
        $sth -> execute();
	}
	
	public function BulkInsertInventory() {
		try {
			$row = 1;
			
			$vinNumberCheck = array();
			
			foreach($this -> _csv as $inventory) {
			
				if(in_array($inventory['VIN'], $vinNumberCheck)) {
					$inventoryStaging = InventoryStaging::WithVIN(trim($inventory['VIN']));
				} else {		
					$inventoryStaging = New InventoryStaging();
					
					$storeInfo = str_replace("Sid Dillon ","",$inventory['Dealer Name']);
					$stores = Stores::WithName(str_replace("BNH ","",$storeInfo));
					
					$inventoryStaging -> store = $stores -> getStoreID();
					
				}
				
				
				$inventoryStaging -> type = $inventory['Type'];
				$inventoryStaging -> stock = $inventory['Stock'];
				$inventoryStaging -> vinNumber = $inventory['VIN'];
				$inventoryStaging -> year = $inventory['Year'];
				$inventoryStaging -> make = $inventory['Make'];
				$inventoryStaging -> model = $inventory['Model'];
				$inventoryStaging -> body = $inventory['Body'];
				$inventoryStaging -> trim = $inventory['Trim'];
				$inventoryStaging -> exteriorColor = $inventory['ExteriorColor'];			
				$inventoryStaging -> JSONInventoryString = base64_encode(json_encode($inventory));
				$inventoryStaging -> lastUpdated = $inventory['Date_Modified'];
				
				
				if(in_array($inventory['VIN'], $vinNumberCheck)) {
					$inventoryStaging -> UpdateStaging();
				} else {
					$inventoryStaging -> InsertStaging();
				}
				
				
				
				
				array_push($vinNumberCheck, $inventory['VIN']);
				
				$row++;
			}	

			//update production inventory list
			$this -> BulkUpdateInventory();
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Bulk Inventory Insert Error: " . $e->getMessage();
			$TrackError -> type = "BULD INVENTORY INSERT ERROR";
			$TrackError -> SendMessage();
			
			print_r($e->getMessage());	
		}
		//$this -> readInArray();
		//$this -> readinPlainText();	
	}
	
	
	private function BulkUpdateInventory() {
		try {
			$totalUpdated = '';
			$newRowsAdded = '';
			$scriptErrors = '';
			$deletedCount = '';
			$totalProductionCount = '';
			
			
			//adding new inventory
			$BulkAdd = $this -> db -> prepare('INSERT INTO inventory (type, stock, vinNumber, year, make, model, body, trim, exteriorColor, store, InventoryLineJSON, lastUpdated)
										   SELECT s.type, s.stock, s.vinNumber, s.year, s.make, s.model, s.body, s.trim, s.exteriorColor, s.store, s.InventoryLineJSON, s.lastUpdated FROM inventorystaging s
										   LEFT OUTER JOIN inventory p ON (s.vinNumber = p.vinNumber) WHERE p.vinNumber IS NULL');
			$BulkAdd -> execute();		
			$newRowsAdded = $BulkAdd -> rowCount();
	
			//deleting old inventory
			
			//http://stackoverflow.com/questions/3384127/delete-sql-rows-where-ids-do-not-have-a-match-in-another-table
			$BulkRemove = $this -> db -> prepare('DELETE p FROM inventory p LEFT JOIN inventorystaging s ON s.vinNumber = p.vinNumber WHERE s.vinNumber IS NULL');
        	$BulkRemove -> execute();
			$deletedCount  = $BulkRemove -> rowCount();
	
			//updating inventory
			$BulkUpdate = $this -> db -> prepare('UPDATE inventory INNER JOIN inventorystaging ON inventory.vinNumber = inventorystaging.vinNumber 
																	SET inventory.type = inventorystaging.type, 	
																	    inventory.stock = inventorystaging.stock, 
																	    inventory.vinNumber = inventorystaging.vinNumber, 
																	    inventory.year = inventorystaging.year, 
																	    inventory.make = inventorystaging.make,
																	    inventory.model = inventorystaging.model,
																	    inventory.body = inventorystaging.body,
																	    inventory.trim = inventorystaging.trim,
																	    inventory.exteriorColor = inventorystaging.exteriorColor,
																	    inventory.store = inventorystaging.store,
																	    inventory.InventoryLineJSON = inventorystaging.InventoryLineJSON,
																	    inventory.lastUpdated = inventorystaging.lastUpdated
																	WHERE inventory.vinNumber = inventorystaging.vinNumber AND inventory.lastUpdated <> inventorystaging.lastUpdated');
        	$BulkUpdate -> execute();
			$totalUpdated = $BulkUpdate -> rowCount();
	
	
			//counting total inventory
			$totalProductionCount = $this -> db -> query("SELECT COUNT(*) FROM inventory") -> fetchColumn();
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Update Inventory Insert Error: " . $e->getMessage();
			$TrackError -> type = "UPDATE INVENTORY INSERT ERROR";
			$TrackError -> SendMessage();
			
			$scriptErrors .= $e->getMessage();
			print_r($e->getMessage());	
		}
		
		if(LIVE_SITE == true) {
			$content = array();
				
			$content['todays-count'] = count($this -> _csv);
			$content['finished-time'] = date("h:i:s a", $this -> time -> NebraskaTime());
			$content['new'] = $newRowsAdded;
			$content['removed'] = $deletedCount;
			$content['update'] = $totalUpdated;
			$content['total-production-count'] = $totalProductionCount;
			$content['errors'] = $scriptErrors;
			$this -> email -> to = "adam.schmidt@siddillon.com";
			$this -> email -> subject = "Daily Inventory Update Script";
			$this -> email -> UpdateInventoryUpdate($content);			
		}

		echo "<table>";
		echo "<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Todays CSV Count:</td><td style='font-family:arial; font-size:16px;'>" . count($this -> _csv) . "</td></tr>";
		echo "<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Update Script Finished:</td><td style='font-family:arial; font-size:16px;'>" . date("h:i:s a", $this -> time -> NebraskaTime()) . "</td></tr>";
		echo "<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>New Record Count:</td><td style='font-family:arial; font-size:16px;'>". $newRowsAdded. "</td></tr>";
		echo "<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Deleted Record Count:</td><td style='font-family:arial; font-size:16px;'>". $deletedCount. "</td></tr>";
		echo "<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Updated Record Count:</td><td style='font-family:arial; font-size:16px;'>". $totalUpdated. "</td></tr>";
		echo "<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Production Inventory Count:</td><td style='font-family:arial; font-size:16px;'>". $totalProductionCount. "</td></tr>";
		//display errors
		if($scriptErrors !== '') {
			echo "<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Errors: </td><td style='font-family:arial; font-size:16px;'>". $scriptErrors. "</td></tr>";								
		}	
		
		echo "</table>";
	
	
		
		
	}
	
	
	public function clearInventoryTable() {
		$inventoryObject = New InventoryHandler();
		$inventoryObject -> truncateTable();
		echo "ah HA!!! its cleared!";
	}

	
	private function readInArray() {
		$inventoryCSV = array_map('str_getcsv', file(INVENTORY_PATH));
		return print_r($inventoryCSV);
	}
	
	private function mapCSVFile() {
		$csv = array_map('str_getcsv', file(INVENTORY_PATH));
	    array_walk($csv, function(&$a) use ($csv) {
	      $a = array_combine($csv[0], $a);
	    });
	    array_shift($csv);
		
		$this -> _csv = $csv;
	}
	
	
	private function readinPlainText() {
		$row = 1;
		if (($handle = fopen(INVENTORY_PATH, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $num = count($data);
		        echo "<p> $num fields in line $row: <br /></p>\n";
		        $row++;
		        for ($c=0; $c < $num; $c++) {
		            echo $data[$c] . "<br />\n";
		        }
		    }
		    fclose($handle);
		}
	}

}
?>