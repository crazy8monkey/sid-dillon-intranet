<?php

class SocialMedia extends API {
	
	private $Content;
	private $keywords;
	private $type;
	private $DeclinedNotes;
	private $IsUrgentPost;
	
	public function __construct() {
		parent::__construct();
	}	
		
	public function GET($type, $id = NULL) {
      try {
	      switch($type) {
		  	case "NewPosts":
				echo json_encode(array('NewPosts' => $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.storeID = stores.id WHERE isPosted = 0 AND isApproved = 0 AND isDeclined = 0 ORDER BY isUrgent DESC')));
				break;	
			
			case "ApprovedPosts":
				echo json_encode(array('ApprovedPosts' => $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.storeID = stores.id WHERE isPosted = 0 AND isApproved = 1 ORDER BY isUrgent DESC')));
				break;	
				
			case "DeclinedPosts":
				echo json_encode(array('DeclinedPosts' => $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.storeID = stores.id WHERE isPosted = 0 AND isDeclined = 1 ORDER BY isUrgent DESC')));
				break;
				
	        case "PostedPosts":
				echo json_encode(array('PostedPosts' => $this -> db -> select('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID LEFT JOIN stores ON socialmediaposts.storeID = stores.id WHERE isPosted = 1 ORDER BY isUrgent DESC')));
				break;
				
			case "Single":
				$postSingle = $this -> db -> prepare('SELECT socialID,
															 socialMediaType, 
															 CONCAT(users.firstName, " ", users.lastName) SubmittedByFullName,
															 submittedDate,
															 isDeclined,
															 isPosted,
															 isApproved,
															 Content,
															 KeyWords,
															 isPostActive,
															 (stores.name) StoreName,
															 ApprovedDeclinedBy.StatusText,
															 (ApprovedDeclinedBy.FullName) ApprovedDeclinedByFullName,
															 (ApprovedDeclinedBy.DateEntered) ApprovedDeclinedDate,
															 isDeclinedNotes,
															 isPostedDate,
															 isPostedDateTime,
															 (stores.id) RelatedStoreID,
															 isUrgent,
															 isUrgentDate
															 FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID 
															 LEFT JOIN socialmediastatus ON socialmediaposts.socialID = socialmediastatus.mediaPostID 
															 LEFT JOIN stores ON socialmediaposts.StoreID = stores.id
															 LEFT JOIN (
															 			 SELECT mediaPostID, 
															 			        StatusText,
															 			 		CONCAT(users.firstname, " ", users.lastName) FullName, 
															 			 		(socialmediastatus.statusDate) DateEntered,
															 			 		(socialmediastatus.statusSubmittedBy) ApprovedDeclinedDate FROM socialmediastatus 
															 			 		LEFT JOIN users ON socialmediastatus.statusSubmittedBy = users.userID WHERE mediaPostID = :id
															 		   ) ApprovedDeclinedBy ON socialmediaposts.socialID = ApprovedDeclinedBy.mediaPostID WHERE socialID = :id');
				$postSingle -> execute(array(":id" => $id));
				echo json_encode(array('PostSingle' => $postSingle-> fetch()));				
				break;
	      }
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Social Media API - GET error: " . $e->getMessage();
			$TrackError -> type = "Social Media API - GET ERROR";
			$TrackError -> SendMessage();
			echo $e->getMessage();
		}
	}
		
	public function PUT($type, $id) {
		$input = file_get_contents('php://input');
		$postData = array();
		parse_str($input, $postData);
		
		
		switch($type) {
			case "draft":
				$this -> Content = $postData['hiddenSocialMediaContent'];
				$this -> keywords = $postData['socialMediaKeyWords'];
				
				if($this -> ValidatePost()) {
					$postData = array('Content' => $this -> Content,
								      'KeyWords' => $this -> keywords);
						
					$this->db->update('socialmediaposts', $postData, array('socialID' => $id));		
					$this -> json -> outputJqueryJSONObject('DraftSaved', "Post Draft Saved");
				}
				break;
				
			case "approve":
				Session::init();
		            	
				$postData = array('isApproved' => 1);
				$this->db->update('socialmediaposts', $postData, array('socialID' => $id));
				
				$this -> db -> insert('socialmediastatus', array('StatusText' => 'Approved', 
																 'statusSubmittedBy' => Session::get('user'),
												  				 'statusDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
												  				 'statusTime' => date("H:i:s", $this -> time -> NebraskaTime()),
																 'mediaPostID' => $id));	
																 
																 
				$userApproved = $this -> db -> prepare('SELECT * FROM socialmediastatus 
															 			 		LEFT JOIN users ON socialmediastatus.statusSubmittedBy = users.userID WHERE mediaPostID = :id');
				
				$userApproved -> execute(array(":id" => $id));
				$statusUpdates = $userApproved -> fetch();																												 
				
				
																 
				$this -> json -> outputJqueryJSONObject('redirect', array('FullName' => $statusUpdates['firstName'] . ' ' . $statusUpdates['lastName'],
																		  'DateSubmitted' => $statusUpdates['statusDate']));		
																		  
				$this -> MarkInactive($id);														  										 											 
																 
				break;
				
			case "makepostlive":
				$postData = array('isPosted' => 1,
								  'isPostedDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
								  'isPostedDateTime' => date("H:i:s", $this -> time -> NebraskaTime()));
				$this->db->update('socialmediaposts', $postData, array('socialID' => $id));	
				
				
				$this -> json -> outputJqueryJSONObject('finished', array("msg" => "Post Marked Live",
																		  'date' => date("Y-m-d", $this -> time -> NebraskaTime()),
																		  'time' => date("H:i:s", $this -> time -> NebraskaTime())));	
				break;
			
			case "decline":	
				
				$this -> DeclinedNotes = $postData['socialMediaDeclinedNotes'];
						
				if($this -> ValidateDeclineNotes()) {
					Session::init();
					
					$postData = array('isDeclined' => 1,
					 				  'isDeclinedNotes' => $this -> DeclinedNotes);
									  
					
					$this -> db -> insert('socialmediastatus', array('StatusText' => 'Declined', 
																	 'statusSubmittedBy' => Session::get('user'),
													  				 'statusDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
													  				 'statusTime' => date("H:i:s", $this -> time -> NebraskaTime()),
																	 'mediaPostID' => $id));	
																	 
					$this->db->update('socialmediaposts', $postData, array('socialID' => $id));	
					
					
					$userApproved = $this -> db -> prepare('SELECT * FROM socialmediastatus 
															 			 		LEFT JOIN users ON socialmediastatus.statusSubmittedBy = users.userID WHERE mediaPostID = :id');
				
					$userApproved -> execute(array(":id" => $id));
					$statusUpdates = $userApproved -> fetch();
					
					$this -> json -> outputJqueryJSONObject('redirect', array('FullName' => $statusUpdates['firstName'] . ' ' . $statusUpdates['lastName'],
																		      'DateSubmitted' => $statusUpdates['statusDate'],
																			  "ReasonNotes" => $this -> DeclinedNotes));												 											 
					
					
					$this -> MarkInactive($id);
					
					if(LIVE_SITE == true) {
						$sth = $this -> db -> prepare('SELECT * FROM socialmediaposts LEFT JOIN users ON socialmediaposts.submittedBy = users.userID WHERE socialID = :id');
						$sth->execute(array(':id' => $id));
						$getObject = $sth -> fetch();
						
						$content = array();
						$content['type'] = $_GET['type'];
						$content['post-type'] = $getObject['Content'];
						$content['reason-notes'] = $this -> DeclinedNotes;
						
						$this -> email -> to = $getObject['email'];
						$this -> email -> subject = ucfirst($_GET['type']) . " Post Declined";
						$this -> email -> DeclinedPost($content);			
					}
					
					
				}
			
				
				break;
				
				
		}
		
	}
	
	public function POST($storeID) {
		$input = file_get_contents('php://input');
		$postData = array();
		parse_str($input, $postData);	
		
		Session::init();
		
		$this -> Content = $postData['hiddenSocialMediaContent'];
		$this -> keywords = $postData['socialMediaKeyWords'];
		$this -> type = $postData['hiddenSocialMediaType'];
		$this -> IsUrgentPost = $postData['socialMediaIsUrgent'];
		
		$sqlDate = explode("/", $this -> IsUrgentPost);
			
		try {
			if($this -> ValidatePost()) {
				$this -> db -> insert('socialmediaposts', array('socialMediaType' => $this -> type, 
																'Content' => $this -> Content,
											  				    'KeyWords' => $this -> keywords,
											  				    'StoreID' => $storeID,
															    'submittedDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
															    'isUrgent' => (!empty($this -> IsUrgentPost) ? 1 : 0), 
															    'isUrgentDate' => (!empty($this -> IsUrgentPost) ? $sqlDate[2] . '-' . $sqlDate[0] . '-' . $sqlDate[1] : NULL),
															    'time' => date("H:i:s", $this -> time -> NebraskaTime()),
																'submittedBy' => Session::get('user')));
				
				if(LIVE_SITE == true) {
					if(!empty($this -> IsUrgentPost)) {
						$getStore = $this -> db -> prepare("SELECT name FROM stores WHERE id = :id");
						$getStore -> execute(array(":id" => $storeID));
						$storeObj = $getStore -> fetch();
						
						$getSettings = $this -> db -> prepare("SELECT * FROM settings WHERE settingsID = 1");
						$getSettings -> execute();
						$settingsObj = $getSettings -> fetch();
						
						$content = array();
						$content['storeName'] = $storeObj['name'];
						$content['urgentTime'] = $this -> time -> formatDate($sqlDate[2] . '-' . $sqlDate[0] . '-' . $sqlDate[1]);
							
						$this -> email -> to = $settingsObj['urgentEmail'];
						$this -> email -> subject = "Urgent Social Media Post";
						$this -> email -> UrgentPost($content);								
					}

				}
				
														 
				$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> StoreSingleLoginPage());
			}
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Post Media Post Error: " . $e->getMessage();
			$TrackError -> type = "POST MEDIA POST ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("errorMessage", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("errorMessage", $e->getMessage());	
			}
			
			
		}
	}
	
	private function ValidatePost() {
		if($this -> validate -> emptyInput($this -> Content)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Post Content"));
			return false;
		} else if(!empty($this -> IsUrgentPost)) {
			
			if(!preg_match("([0-9]{2}\/[0-9]{2}\/[0-9]{4})", $this -> IsUrgentPost)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', 'Needs to be in MM/DD/YYY Format');
				return false;
			}
		}
		return true;
	}


	private function ValidateDeclineNotes() {
		if($this -> validate -> emptyInput($this -> DeclinedNotes)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Decline Notes"));
			return false;
		}
		return true;
	}


	private function MarkInactive($id) {
		$postData = array('isPostActive' => 0);				
		$this->db->update('socialmediaposts', $postData, array('socialID' => $id));
	}
	

}