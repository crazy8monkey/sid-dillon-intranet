<?php
class Notification extends API {
	
	private $_storeList;
	private $_usersList;
	private $_completedMessageContent;
	private $_completedMessageCount;
	private $_singleEmailText;
	
	
	public function __construct() {
		parent::__construct();
		$this -> setCompletedMessages();
	}
	
	public function SEND($type) {
		$results = "";	
		switch($type) {
			case "todo":
				
				$settings = $this -> db -> prepare('SELECT * FROM settings WHERE settingsID = :id');
				$settings -> execute(array('id' => 1));
				$getSettings = $settings -> fetch();
				
				$singleUserEmailText = "";
				$totalPercentageValue = "";
				$storeCount = 0;
				$emailText = "";
				$email = new Email();
				
				$storeIDs = array();
				$usersReceivingPermissions = array();
				
				foreach($this -> db -> select('SELECT * FROM stores') as $store) {
					array_push($storeIDs, $store['id']);
					
					$storePercentages = ($this -> GetFacebookCount($store['id']) + $this -> GetTwitterCount($store['id']) + $this -> GetBlogCount($store['id'])) / ($getSettings['facebookPosts'] + $getSettings['TwitterPosts'] + $getSettings['blogPost']);
					
					//summary to do list 
					$emailText .= "<tr><td style='padding:15px; background:white; border-top:1px solid #ececec'>
									<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%'>
										<tr>
											<td style='padding:0px; vertical-align: top'>
												<table cellpadding='0' cellspacing='0' width='100%'>
													<tr><td style='font-weight:bold; font-family:arial; font-size:20px; padding:0px 0px 10px 0px'>" . $store['name'] . "</td></tr>
													<tr><td style='padding:15px 0px 0px 0px; font-family:arial; padding:0px;'>Facebook - " . $this -> GetFacebookCount($store['id']) . "/" . $getSettings['facebookPosts'] . "</td></tr>
													<tr><td style='font-family:arial;'>Twitter - " . $this -> GetTwitterCount($store['id']) . "/" . $getSettings['TwitterPosts'] . "</td></tr>
													<tr><td style='font-family:arial;'>Website Blog Posts - " . $this -> GetBlogCount($store['id']) . "/" . $getSettings['blogPost'] . "</td></tr>
												</table>
											</td>
											<td style='padding:0px 0px 10px 10px; font-weight:bold; font-family:arial; font-size:20px; text-align:right; vertical-align: top'>". round($storePercentages * 100) . "%</td>
										</tr>
									</table>
								</td></tr>";
					
					
				}
				
				foreach($storeIDs as $id) {
					foreach($this -> db -> select('SELECT * FROM users WHERE store = ' . $id . ' AND isReceivingNotifications = 1') as $user) {
						$storeIDS = NULL;
						$storeNames = NULL;
						$percentageMain = NULL;
						$overalPercentage = NULL;
						
						
						foreach($this -> db -> select('SELECT * FROM permissions WHERE PermissionType = "Marketing" AND userID = '. $user['userID']) as $userPermissionView) {
							$storeIDS .= $userPermissionView['storeID'] . ', ';
							$percentageMain .= round(($this -> GetFacebookCount($userPermissionView['storeID']) + $this -> GetTwitterCount($userPermissionView['storeID']) + $this -> GetBlogCount($userPermissionView['storeID'])) / ($getSettings['facebookPosts'] + $getSettings['TwitterPosts'] + $getSettings['blogPost']) * 100) . '%, ';
							$overalPercentage += round(($this -> GetFacebookCount($userPermissionView['storeID']) + $this -> GetTwitterCount($userPermissionView['storeID']) + $this -> GetBlogCount($userPermissionView['storeID'])) / ($getSettings['facebookPosts'] + $getSettings['TwitterPosts'] + $getSettings['blogPost']) * 100);
							$storeCount++;
						}
						array_push($usersReceivingPermissions, array('userName' => $user['firstName'] . ' ' . $user['lastName'],
																	 'storeIDs' => rtrim($storeIDS, ', '),
																	 "storePercentages" => rtrim($percentageMain, ', '),
																	 "OveralPercentage" => round($overalPercentage / $storeCount). "%",
																	 "StoreCount" => $storeCount,
																	 "UserEmail" => $user['email']));		
						$storeCount = 0;											 
					}
				} 
			
				echo "user Permission array<pre>";
				print_r($usersReceivingPermissions);
				echo "</pre> Emails";
				
				
				//generates the single email content
				foreach($usersReceivingPermissions as $emailSetup) {
					$this -> _singleEmailText = "";
					echo "<div style='color:red; font-size:20px; margin:10px 0px'>" . $emailSetup['userName'] ."</div>";
					$storeIDSLoop = explode(",", $emailSetup['storeIDs']);
					
					foreach($storeIDSLoop as $id) {
						$getStore = $this -> db -> prepare('SELECT * FROM stores WHERE id = :id');
						$getStore -> execute(array(':id' => $id));
						$getStoreObject = $getStore -> fetch();
						
						$storePercentage = ($this -> GetFacebookCount($id) + $this -> GetTwitterCount($id) + $this -> GetBlogCount($id)) / ($getSettings['facebookPosts'] + $getSettings['TwitterPosts'] + $getSettings['blogPost']);
						
						$this -> _singleEmailText .= "<tr><td style='padding:15px; background:white; border-top:1px solid #ececec'><table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%'>
											<tr>
												<td style='padding:0px; vertical-align: top'>
													<table cellpadding='0' cellspacing='0' width='100%'>
														<tr><td style='font-weight:bold; font-family:arial; font-size:20px; padding:0px 0px 10px 0px'>" . $getStoreObject['name'] . "</td></tr>
														<tr><td style='padding:15px 0px 0px 0px; font-family:arial; padding:0px;'>Facebook - " . $this -> GetFacebookCount($id) ."/" . $getSettings['facebookPosts'] . "</td></tr>
														<tr><td style='font-family:arial;'>Twitter - " . $this -> GetTwitterCount($id) ."/" . $getSettings['TwitterPosts'] . "</td></tr>
														<tr><td style='font-family:arial;'>Website Blog Posts - " . $this -> GetBlogCount($id). "/" . $getSettings['blogPost'] . "</td></tr>
													</table>
												</td>
												<td style='padding:0px 0px 10px 10px; font-weight:bold; font-family:arial; font-size:20px; text-align:right; vertical-align: top'>" . round($storePercentage * 100). "%</td>
											</tr>
										</table></td></tr>";
						if(round($storePercentage * 100) == 100) {
							$this -> _singleEmailText .= "<tr><td style='padding:15px; font-family:arial; font-size:16px;border-top:1px solid #ececec'>". implode($this -> CompletedToDoListMessages($randomCompleteMessage)) . "</td></tr>";
						}				
						
					}
					
					echo "<table style='width: 450px;'>" . $this -> _singleEmailText . "</table>";
					//send the email
					$this -> SingleIncompleteToDoListReminder($emailSetup['UserEmail'], $emailSetup['OveralPercentage']);
				}	
				
			
				if(LIVE_SITE == true) {
					$email -> to = $getSettings['SummaryEmailReminder'];
					$email -> subject = "Incomplete To Do list";
					$email -> sendIncompleteEmailMaster($emailText);			
				}
				
				break;
		}
		
		
	}	

	public function userEmailTest() {
		$content = array();
		$content['username'] = "ah HA!!";
		$content['password'] = "blah";
		
		$this -> email -> to = "adam.schmidt@siddillon.com";
		$this -> email -> subject = "Your Credentials";
		$this -> email -> userNotification($content);
	}
	
	
	private function GetFacebookCount($storeID) {
		$facebook = $this -> db -> prepare('SELECT * FROM socialmediaposts WHERE StoreID=:StoreID AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND socialMediaType = 1 AND isDeclined = 0');
		$facebook -> execute(array(':StoreID' => $storeID, 
				        					  ':startWeek' => $this -> time -> StartOfWeekSQL(), 
				        					  ':endWeek' => $this -> time -> EndOfWeekSQL()));
		return $facebook -> rowCount();
	}
	
	private function GetTwitterCount($storeID) {
		$twitter = $this -> db -> prepare('SELECT * FROM socialmediaposts WHERE StoreID=:StoreID AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND socialMediaType = 2 AND isDeclined = 0');
		$twitter -> execute(array(':StoreID' => $storeID, 
								  ':startWeek' => $this -> time -> StartOfWeekSQL(), 
								  ':endWeek' => $this -> time -> EndOfWeekSQL()));
		return $twitter -> rowCount();
	}
	
	private function GetBlogCount($storeID) {
		$blog = $this -> db -> prepare('SELECT * FROM socialmediaposts WHERE StoreID=:StoreID AND submittedDate >= :startWeek AND submittedDate <= :endWeek AND socialMediaType = 3 AND isDeclined=0');
		$blog -> execute(array(':StoreID' => $storeID, 
										           ':startWeek' => $this -> time -> StartOfWeekSQL(), 
										           ':endWeek' => $this -> time -> EndOfWeekSQL()));
		return $blog -> rowCount();
	}
	
	
	private function SingleIncompleteToDoListReminder($email, $percentage) {
		if(LIVE_SITE == true) {
			$this -> email -> to = $email;
			$this -> email -> subject = "Your weekly to do list status";
			$this -> email -> sendIncompleteEmailSingle($this -> _singleEmailText, $percentage);			
		}
		
	}
	
	
	private function CompletedToDoListMessages($id) {
										
		return $this -> _completedMessageContent[$id];
		
		
	}
	
	private function setCompletedMessages() {
		$this -> _completedMessageContent = array(array('text' => 'Congratulations on completing 100% ...wish I was as awesome as you!',
														'image' => ''),
												  array('text' => "100%! You did it! This is why...",
														'image' =>'<br /><br /><img src="' . PATH . 'public/images/bigdill.jpg" width="100%" />'),
												  array('text' => 'As a gesture for my appreciation for all your hard work',
														'image' => '<br /><br /><img src="' . PATH . 'public/images/fistBump.jpg" width="100%" />'),
												  array('text' => "100% Complete this week!  Working so hard you're starting to have the eye of the tiger",
														'image' => '<br /><br /><img src="' . PATH . 'public/images/highFiveTiger.jpg" width="100%" />'),
												  array('text' => 'Another week down, another 100%',
														'image' => '<br /><br /><img src="' . PATH . 'public/images/baby.jpg" width="100%" />'),
												  array('text' => "You're incredible! 100% Completion this week!",
														'image' => '<br /><br /><img src="' . PATH . 'public/images/dogHighFive.jpg" width="100%" />'),
												  array('text' => '100% Done!  All work totally complete, bet you feel pretty sweet!',
														'image' => ''),
												  array('text' => "Keep working this hard and I'm going to need sunglasses to see this companies future! 100%!",
														'image' => ''),
												  array('text' => 'Just when we thought that being more impressed was impossible, you do it again! 100%!',
														'image' => ''),
											      array('text' => '100% Complete! Someone asked me about you the other day, ran out of breathe way too many good things to say!',
														'image' => ''),
												  array('text' => '100% Done this week. If only we had a 100 of you!',
														'image' => ''),
												  array('text' => "You didn't do well, you did amazing! 100%!!!",
														'image' => ''),
												  array('text' => 'Congratulations! 100% Complete!',
														'image' => ''),
												  array('text' => 'Great work! 100% again!',
														'image' => ''));
										
		$this -> _completedMessageCount = count($this -> _completedMessageContent);								
	}
	

}
?>