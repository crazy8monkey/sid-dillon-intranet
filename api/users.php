<?php

class Users extends API {
	
	private $userID;
	private $firstName;
	private $lastName;
	private $Username;
	private $UserEmail;
	private $UserType;
	private $UserStore;
	private $JobTitle;
	private $password;
	private $IsReferral;
	
	private $finalStoreValue;
	private $finalJobTitleValue;
	
	private $TrueFalseValue;
	private $userIsActive;
	private $deactivationReason;
	
	public function __construct() {
		parent::__construct();
	}	
		
	public function GET($type, $id = NULL) {
		switch($type) {
			//user single
			case "user";
				if(isset($id)) {
					echo json_encode(array('userSingle' => $this -> db -> select('SELECT * FROM users LEFT JOIN stores ON users.store = stores.id WHERE userID=' . $id)));	
				}
				break;
			//get users by store
			case "store";
				if(isset($id)) {
					echo json_encode(array('storeUsers' => $this -> db -> select('SELECT * FROM users WHERE store=' . $id)));
				}
				break;
				
			//get all users	
			case "all";
				echo json_encode(array('users' => $this -> db -> select('SELECT * FROM users LEFT JOIN stores ON users.store = stores.id ORDER BY users.firstName ASC')));	
				break;
				
			case "referrals":
				echo json_encode(array('StoreReferralUsers' => $this -> db -> select('SELECT * FROM users LEFT JOIN stores ON users.store = stores.id WHERE store IN ('. $_GET['storeID'] .') AND isReferralUser = 1 ORDER BY users.firstName ASC')));
				break;
			case "storeadmins":
				echo json_encode(array('StoreAdmins' => $this -> db -> select('SELECT * FROM users WHERE userType IN (2, 3) AND store = ' . $id)));
				break;
		}
	}
		
	public function POST($type) {
		try {
			
			//get post data
			$input = file_get_contents('php://input');
			$postData = array();
			parse_str($input, $postData);
			
			//get settings	
			$settings = $this -> db -> prepare('SELECT * FROM settings WHERE settingsID = :id');
			$settings -> execute(array('id' => 1));
			$getSettings = $settings -> fetch();
			
			
			switch($type) {
		      case "request":
					$this -> firstName = $postData['userFirstName'];
					$this -> lastName = $postData['userLastName'];
					$this -> UserEmail = $postData['userEmail'];
					$this -> JobTitle = $postData['JobTitle'];
					$this -> UserStore = $postData['userStore'];	
					
					if($this ->ValidateRequest()) {
						$securityToken = Hash::generateSecurityToken();
						$this -> db -> insert('users', array('firstName' => $this -> firstName, 
														  	 'lastName' => $this -> lastName,
														  	 'email' => $this -> UserEmail,
														  	 'userName' => 0,
															 'password' => 0, 
															 'JobTitle' => $this -> JobTitle,
															 'isReceivingNotifications' => 0,
															 'securityToken' => $securityToken,
															 'store' => $this -> UserStore,
															 'userType' => 3,
															 'salt' => 0,
															 'isActive' => 0));		
															 		
						$this -> json -> outputJqueryJSONObject('success', "Thank you for requesting access.<br /><br />Your request is being processed and you will recieve an email when it's approved.<br /><br />If you have any questions or comments call Jarrod at 402-505-4226.");				
						
						
						//send request email
						if(LIVE_SITE == true) {
							
							
							//get storename
							$store = $this -> db -> prepare('SELECT * FROM stores WHERE id = :id');
							$store -> execute(array('id' => $this -> UserStore));
							$getStore = $store -> fetch();
							
							$jobTitleText = "";
							
							switch($this -> UserStore) {
								case 1:
									$jobTitleText = "General Manager";
									break;
								case 2:
									$jobTitleText = "Sales Manager";
									break;
								case 3:
									$jobTitleText= "Salesman";
									break;
								case 4:
									$jobTitleText = "Internet Salesman";
									break;								        
								case 5:
									$jobTitleText = "Service Manager";
									break;
								case 6:
									$jobTitleText = "Service Advisor";
									break;
								case 7:
									$jobTitleText = "Service Tech";
									break;
								case 8:
									$jobTitleText = "Parts Manager";
									break;					        		    
								case 9:
									$jobTitleText = "Parts Counter";
									break;
								case 10:
									$jobTitleText = "Office Manager";
									break;
								case 11:
									$jobTitleText = "Office Personal";
									break;
								case 12:
									$jobTitleText = "Receptionist";
									break;
								case 13:
									$jobTitleText = "Marketing/Advertising";
									break;
								case 14:
									$jobTitleText = "Finance Manager";
									break;
								case 15:
									$jobTitleText = "Finance";
									break;
								case 16:
									$jobTitleText = "Bodyshop Manager";
									break;
								case 17:
									$jobTitleText = "Bodyshop Consultant";
									break;
								case 18:
									$jobTitleText = "Bodyshop Tech";
									break;
								case 19:
									$jobTitleText = "Other";
									break;
							}
							
							$content = array();
							$content['first-name'] = $this -> firstName;
							$content['last-name'] = $this -> lastName;
							$content['email'] = $this -> UserEmail;
							$content['user-type'] = $jobTitleText;
                          	                          
							$content['store'] = $getStore['name'];
							
							
							$email = new Email();
							$email -> to = $getSettings['SummaryEmailReminder'];
							$email -> subject = "New Intranet Request Access";
							$email -> NewRequest($content);
						}
					}
					
					break;
                case "passwordReset":
                	$this -> UserEmail = $postData['userEmail'];
                	
                	$getUser = $this -> db -> prepare("SELECT * FROM users WHERE email = :email");
					$getUser -> execute(array(":email" => $this -> UserEmail));
					$getUserObject = $getUser -> fetch();
                
                	if($this -> ValidatePasswordReset()) {
                    	$securityToken = Hash::generateSecurityToken();
                      	$this->db->update('users', array('securityToken' => $securityToken), array('userID' => $getUserObject['userID']));		

                      	$this -> json -> outputJqueryJSONObject('validEmail', '<strong>Authentication Successful</strong></br/> An email has been set with instructions on resetting your password');

	                    if(LIVE_SITE == true) {
	                    	$resetLink = PATH . '#/passwordReset?token=' . $securityToken . '&u=' . $getUserObject['userID'];	
	                        $email = new Email();
	                        $email -> to = $this -> UserEmail;
	                        $email -> subject = "Reset Your Password";
	                        $email -> resetPassword($resetLink);			
	                    }
                  	}
                
                	break;
					
				case "login":
					$username = $postData['userNameLogin'];	
					$password = $postData['userNamePassword'];
		
					$sth = $this->db->prepare("SELECT * FROM users WHERE userName = :username AND isActive=1");
					$sth->execute(array(':username' => $username));
					
					$data = $sth->fetch();							 
					$count =  $sth->rowCount();
					
					if ($count > 0) {
						
			            if(Password::ValidatePasswordAttempt($data['password'], $data['salt'], $password)) {
							Session::init();
			            	Session::set('loggedIn', true);
			            	Session::set('user', $data['userID']);
							Session::set('userType', $data['userType']);
							Session::set('storeID', $data['store']);
							
							$postData = array('lastLoginDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
											  'lastLoginDateTime' => date("H:i:s", $this -> time -> NebraskaTime()));
							
							$this->db->update('users', $postData, array('userID' => $data['userID']));	
							
								
							//redirect to main app section
							$this -> json -> outputJqueryJSONObject('finished', array('loggedIn' => Session::get('loggedIn'),
																					  'UserType' => Session::get('userType'),
																					  'UserID' => Session::get('user'),
																					  'UserStore' => Session::get('storeID'),
																					  'redirect' => $this -> pages -> adminMainPage()));	
						} else {
							$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> wrongCredentialMatch(). " Incorrect Password");
						}
					} else {
						$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> wrongCredentialMatch(). " No Username");			
					}
						
					break;
				
				case "newuser":	
					$this -> firstName = $postData['userFirstName'];
					$this -> lastName = $postData['userLastName'];
					$this -> UserEmail = $postData['userEmail'];
					$this -> UserType = $postData['userType'];
					$this -> UserStore = $postData['userStore'];	
					$this -> Username = $postData['userNameLogin'];
					$this -> Username = $postData['userNameLogin'];
					$this -> password = $postData['userPassword'];
					$this -> JobTitle = $postData['JobTitle'];
					
					if($this -> ValidateNewUser()) {
						
						$this -> UserTypeValue($this -> UserType, $this -> UserStore, $this -> JobTitle);
						
							
						
						$salt = Hash::generateSalt();	
						
						$lastInsertedID = $this -> db -> insert('users', array('firstName' => $this -> firstName, 
												  				'lastName' => $this -> lastName,
												  				'email' => $this -> UserEmail,
												  				'userName' => $this -> Username,
												  				'password' => Password::SetPassword($this-> password, $salt), 
																'userType' => $this -> UserType,
																'store' => $this -> finalStoreValue,
							            						'JobTitle' => $this -> finalJobTitleValue,
							            						'isReceivingNotifications' => $this -> TrueFalseValue($postData['emailNotifications']),
																'isReferralUser' => $this -> TrueFalseValue($postData['referralUser']),
																'salt' => $salt,
																'isActive' => 1));
						
						
						foreach($postData['hiddenMarketingStoreIDPermission'] as $id) {
							if(isset($postData['storeID'. $id. 'Marketing'])) {
								$this -> db -> insert('permissions', array('PermissionType' => "Marketing", 
														  				   'Enabled' => 1,
														  				   'storeID' => $id,
														  				   'userID' => $lastInsertedID));
							}	
						}
						
						foreach($postData['hiddenReferralStoreIDPermission'] as $id) {
							if(isset($postData['storeID'. $id. 'Referral'])) {
								$this -> db -> insert('permissions', array('PermissionType' => "Referral", 
														  				   'Enabled' => 1,
														  				   'storeID' => $id,
														  				   'userID' => $lastInsertedID));
							}	
						}
	
						foreach($postData['hiddenReferralAdminStoreIDPermission'] as $id) {
							if(isset($postData['storeID'. $id. 'ReferralAdmin'])) {
								$this -> db -> insert('permissions', array('PermissionType' => "ReferralAdmin", 
														  				   'Enabled' => 1,
														  				   'storeID' => $id,
														  				   'userID' => $lastInsertedID));
							}	
						}
	
						//sends an email to the new user
						if(LIVE_SITE == true) {
							$content = array();
							$content['username'] = $this -> Username;
							$content['password'] = $this -> password;
							
		                    $email = new Email();
							$email -> to = $this -> UserEmail;
							$email -> subject = "Your Credentials";
							$email -> UserSignupNotification($content);
						}
						
						
															 
						$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> adminUsersList());									 
					}
							
					break;
				case "newreferraluser":
					$this -> firstName = $postData['userFirstName'];
					$this -> lastName = $postData['userLastName'];
					$this -> UserEmail = $postData['userEmail'];
					$this -> UserType = 3;
					$this -> UserStore = $postData['userStore'];	
					$this -> JobTitle = $postData['JobTitle'];
					
					
					if($this -> ValidateReferralUser()) {
						$this -> UserTypeValue($this -> UserType, $this -> UserStore, $this -> JobTitle);
						
						$securityToken = Hash::generateSecurityToken();
						
						$lastInsertedID = $this -> db -> insert('users', array('firstName' => $this -> firstName, 
												  				'lastName' => $this -> lastName,
												  				'email' => $this -> UserEmail,
												  				'userName' => 'Z',
												  				'password' => 'z', 
												  				'userType' => 3,
																'store' => $this -> UserStore,
							            						'JobTitle' => $this -> JobTitle,
							            						'securityToken' => $securityToken,
							            						'isReceivingNotifications' => 0,
							            						'isReferralUser' => 1,
																'salt' => 0,
																'isActive' => 1));
																
						$this -> db -> insert('permissions', array('PermissionType' => "Referral", 
														  		   'Enabled' => 1,
														  		   'storeID' => $this -> UserStore,
														  		   'userID' => $lastInsertedID));										
						
						//email notificaitons  
						if(LIVE_SITE == true) {
			                $link = PATH . '#/new-credentials?token=' . $securityToken . '&u=' . $lastInsertedID;	
							$email = new Email();
			                $email -> to = $this -> UserEmail;
			                $email -> subject = "Setup your credentials";
			            	$email -> resetPassword($link, "Click on the link below to create your Sid Dillon Intranet user credentials.");
			            }  
						
						$this -> json -> outputJqueryJSONObject('success', "<strong>Account Created</strong><br />An email has been sent out for them to create their credentials");
						
					}
					
					break;
				
			}
		
			
			
											 
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "User API - Post Error: " . $e->getMessage();
			$TrackError -> type = "User API - POST USER ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		}
		
	}

	public function PUT($type, $id) {
		try {
			//get post data
			$input = file_get_contents('php://input');
			$postData = array();
			parse_str($input, $postData);
				
			//get row in users table
			$getUser = $this -> db -> prepare("SELECT * FROM users WHERE userID = :id");
			$getUser -> execute(array(":id" => $id));
			$getUserObject = $getUser -> fetch();	
			
			$email = new Email();	
				
			switch($type) {
	        	case "newpassword":            
					$this -> password = $postData['newPassword'];
						
					if($this -> ValidateNewPassword()) {
						$salt = Hash::generateSalt();	
							
						$postData = array('password' => Password::SetPassword($this-> password, $salt), 
										  'salt' => $salt, 
										  'securityToken' => '');
					
						$this->db->update('users', $postData, array('userID' => $id));	
							
						if(LIVE_SITE == true) {
							$content = array();
							$content['username'] = $getUserObject['userName'];
							$content['password'] = $this -> password;
										 
							$email -> to = $getUserObject['email'];
							$email -> subject = "Your Credentials";
							$email -> UserSignupNotification($content);
						}
							
						$this -> json -> outputJqueryJSONObject('success', "Your new password has been saved");
								
							
					}
	          	break;
				
	            case "credientials":
	            	$this -> Username = $postData['newUsername'];
	                $this -> password = $postData['newPassword'];
	
	                if($this -> ValidateNewCredentials()) {
	                    $salt = Hash::generateSalt();	
	
	                    $postData = array('userName' => $this -> Username,
	                                      'password' => Password::SetPassword($this-> password, $salt), 
	                                      'salt' => $salt,
	                                      'securityToken' => '');
	
	                    $this->db->update('users', $postData, array('userID' => $id));	
	                    $this -> json -> outputJqueryJSONObject('success', "Your credentials has been set. You now have access to Sid Dillon Intranet");
	
	
	                    if(LIVE_SITE == true) {
	                        $content = array();
	                        $content['username'] = $this -> Username;
	                        $content['password'] = $this -> password;
	
	                        $email -> to = $getUserObject['email'];
	                        $email -> subject = "Your Credentials";
	                        $email -> UserSignupNotification($content);
	                    }
	
	                }
	            
	            
	            	break;
				case "edituser":
					$this -> userID = $id;
	                $this -> firstName = $postData['userFirstName'];
	                $this -> lastName = $postData['userLastName'];
	                $this -> Username = $postData['userNameLogin'];
	                $this -> UserEmail = $postData['userEmail'];
	                $this -> password = $postData['userPassword'];			
					$this -> JobTitle = $postData['JobTitle'];
					$this -> UserType = $postData['userType'];
					$this -> UserStore = $postData['userStore'];
					$this -> userIsActive = $postData['userIsActive'];
					$this -> deactivationReason = $postData['deactiveReason'];
					
					
					if($this -> ValidateProfile()) {
						$this -> UserTypeValue($this -> UserType, $this -> UserStore, $this -> JobTitle);
						
						
						//permissions
						
						//marketing
						$marketingPermissionsChecked = array();
			            foreach($postData['hiddenMarketingStoreIDPermission'] as $storeID) {
			                if(isset($postData['storeID'. $storeID. 'Marketing'])) {
			                	array_push($marketingPermissionsChecked, array("Name" => 'storeID'. $storeID. 'Marketing',
																			   'Value' => $postData['storeID'. $storeID. 'Marketing'],
																			   "StoreID" => $storeID, 
																			   "UserID" => $id));	
							}
						}
		
						$this -> SetUserPermissions("Marketing", $postData['hiddenMarketingStoreIDPermission'], $marketingPermissionsChecked);
			                    	                    
			            //referrals
			            $referralPermissionsChecked = array();
			            foreach($postData['hiddenMarketingStoreIDPermission'] as $storeID) {
			                if(isset($postData['storeID'. $storeID. 'Referral'])) {
			                	array_push($referralPermissionsChecked, array("Name" => 'storeID'. $storeID. 'Referral',
																			  'Value' => $postData['storeID'. $storeID. 'Referral'],
																			  "StoreID" => $storeID, 
																			  "UserID" => $id));	
							}	
						}
			                    
						$this -> SetUserPermissions("Referral", $postData['hiddenReferralStoreIDPermission'], $referralPermissionsChecked);
				
			            //referral admin
			            $referralAdminPermissionsChecked = array();
			            foreach($postData['hiddenReferralAdminStoreIDPermission'] as $storeID) {
			            	if(isset($postData['storeID'. $storeID. 'ReferralAdmin'])) {
			                	array_push($referralAdminPermissionsChecked, array("Name" => 'storeID'. $storeID. 'ReferralAdmin',
																				   'Value' => $postData['storeID'. $storeID. 'ReferralAdmin'],
																				   "StoreID" => $storeID, 
																				   "UserID" => $id));	
							}
						}
			                    
						$this -> SetUserPermissions("ReferralAdmin", $postData['hiddenReferralAdminStoreIDPermission'], $referralAdminPermissionsChecked);
						
						$dbActiveStats = NULL;
						
						if($postData['userIsActive'] == 'on') {
							$dbActiveStats = 1;
						} else {
							$dbActiveStats = 2;
						}
						
						
						
						if($this -> password !=='') {
							
							
							
	                        $salt = Hash::generateSalt();	
	                        $postData = array('firstName' => $this -> firstName,
	                                          'lastName' => $this -> lastName,
	                                          'email' => $this -> UserEmail,
	                                          'userName' => $this -> Username,
	                                          'store' => $this -> finalStoreValue,
		            						  'JobTitle' => $this -> finalJobTitleValue,
		            						  'isReceivingNotifications' => $this -> TrueFalseValue($postData['emailNotifications']),
											  'isReferralUser' => $this -> TrueFalseValue($postData['referralUser']),
											  'userType' => $this -> UserType,
	                                          'password' => Password::SetPassword($this-> password, $salt),
	                                          'salt' => $salt,
											  'isActive' => $dbActiveStats,
											  'deactiveReasonStatus' => str_replace("\n", '<br />',$this -> deactivationReason));
	                    } else {
	                        $postData = array('firstName' => $this -> firstName,
	                                          'lastName' => $this -> lastName,
	                                          'email' => $this -> UserEmail,
	                                          'store' => $this -> finalStoreValue,
		            						  'JobTitle' => $this -> finalJobTitleValue,
		            						  'isReceivingNotifications' => $this -> TrueFalseValue($postData['emailNotifications']),
											  'isReferralUser' => $this -> TrueFalseValue($postData['referralUser']),
											  'userType' => $this -> UserType,
	                                          'userName' => $this -> Username,
											  'isActive' => $dbActiveStats,
											  'deactiveReasonStatus' => str_replace("\n", '<br />',$this -> deactivationReason));
	                    }
						
						$this->db->update('users', $postData, array('userID' => $id));							
	                    $this -> json -> outputJqueryJSONObject('userUpdated', $this -> firstName . " " . $this -> lastName);
						
					}
					
					break; 
	            case "profile":
	            	$this -> userID = $id;
	                $this -> firstName = $postData['userFirstName'];
	                $this -> lastName = $postData['userLastName'];
	                $this -> Username = $postData['userNameLogin'];
	                $this -> UserEmail = $postData['userEmail'];
	                $this -> password = $postData['userPassword'];			
					$this -> JobTitle = $postData['JobTitle'];
					$this -> UserType = $getUserObject['userType'];
					$this -> UserStore = $getUserObject['store'];
					
	                if($this -> ValidateProfile()) {
	                	
						$this -> UserTypeValue($this -> UserType, $this -> UserStore, $this -> JobTitle);
	                    
	                    if($this -> password !=='') {
							
	                        $salt = Hash::generateSalt();	
	                        $postData = array('firstName' => $this -> firstName,
	                                          'lastName' => $this -> lastName,
	                                          'email' => $this -> UserEmail,
	                                          'userName' => $this -> Username,
	                                          'JobTitle' => $this -> finalJobTitleValue,
	                                          'password' => Password::SetPassword($this-> password, $salt),
	                                          'salt' => $salt);
	                    } else {
	                        $postData = array('firstName' => $this -> firstName,
	                                          'lastName' => $this -> lastName,
	                                          'email' => $this -> UserEmail,
	                                          'JobTitle' => $this -> finalJobTitleValue,
	                                          'userName' => $this -> Username);
	                    }
								
	                    $this->db->update('users', $postData, array('userID' => $id));							
	                    $this -> json -> outputJqueryJSONObject('userUpdated', "User Updated");
	                }
	            	break;
					
	          case "approve":
		          	
					$this -> userID = $id;
					$this -> firstName = $postData['userFirstName'];
		            $this -> lastName = $postData['userLastName'];
		            $this -> UserEmail = $postData['userEmail'];
		            $this -> UserType = $postData['userType'];
		            $this -> JobTitle = $postData['JobTitle'];
		            $this -> UserStore = $postData['userStore'];
		            		
		            if($this -> ValidateAproveUser()) {
						
						$this -> UserTypeValue($this -> UserType, $this -> UserStore, $this -> JobTitle);
							
						//permissions
							
						//marketing
						$marketingPermissionsChecked = array();
			            foreach($postData['hiddenMarketingStoreIDPermission'] as $storeID) {
			                if(isset($postData['storeID'. $storeID. 'Marketing'])) {
			                	array_push($marketingPermissionsChecked, array("Name" => 'storeID'. $storeID. 'Marketing',
																			   'Value' => $postData['storeID'. $storeID. 'Marketing'],
																			   "StoreID" => $storeID, 
																			   "UserID" => $id));	
							}
						}
		
						$this -> SetUserPermissions("Marketing", $postData['hiddenMarketingStoreIDPermission'], $marketingPermissionsChecked);
			                    	                    
			            //referrals
			            $referralPermissionsChecked = array();
			            foreach($postData['hiddenMarketingStoreIDPermission'] as $storeID) {
			                if(isset($postData['storeID'. $storeID. 'Referral'])) {
			                	array_push($referralPermissionsChecked, array("Name" => 'storeID'. $storeID. 'Referral',
																			  'Value' => $postData['storeID'. $storeID. 'Referral'],
																			  "StoreID" => $storeID, 
																			  "UserID" => $id));	
							}	
						}
			                    
						$this -> SetUserPermissions("Referral", $postData['hiddenReferralStoreIDPermission'], $referralPermissionsChecked);
				
			            //referral admin
			            $referralAdminPermissionsChecked = array();
			            foreach($postData['hiddenReferralAdminStoreIDPermission'] as $storeID) {
			            	if(isset($postData['storeID'. $storeID. 'ReferralAdmin'])) {
			                	array_push($referralAdminPermissionsChecked, array("Name" => 'storeID'. $storeID. 'ReferralAdmin',
																				   'Value' => $postData['storeID'. $storeID. 'ReferralAdmin'],
																				   "StoreID" => $storeID, 
																				   "UserID" => $id));	
							}
						}
			                    
						$this -> SetUserPermissions("ReferralAdmin", $postData['hiddenReferralAdminStoreIDPermission'], $referralAdminPermissionsChecked);
						
						
						//email notificaitons  
						if(LIVE_SITE == true) {
			                $link = PATH . '#/new-credentials?token=' . $getUserObject['securityToken'] . '&u=' . $id;	
			
			                $email -> to = $this -> UserEmail;
			                $email -> subject = "Setup your credentials";
			            	$email -> resetPassword($link, "Click on the link below to create your Sid Dillon Intranet user credentials.");
			            }  
			            
			            $postData = array('firstName' => $this -> firstName,
			            				  'lastName' => $this -> lastName,
			            				  'email' => $this -> UserEmail,
			            				  'userType' => $this -> UserType,
			            				  'store' => $this -> finalStoreValue,
			            				  'JobTitle' => $this -> finalJobTitleValue,
			            				  'isReceivingNotifications' => $this -> TrueFalseValue($postData['emailNotifications']),
										  'isReferralUser' => $this -> TrueFalseValue($postData['referralUser']),
										  'isActive' => 1);
			            
			            
			            
			            $this->db->update('users', $postData, array('userID' => $id));
						
						$this -> json -> outputJqueryJSONObject("redirect", "#/admin/settings/users/" . $id . "/edit");
						
					}  
					
		            break;
				case "referraluser":
					$this -> userID = $id;
					$this -> firstName = $postData['userFirstName'];
					$this -> lastName = $postData['userLastName'];
					$this -> UserEmail = $postData['userEmail'];
					$this -> UserStore = $postData['userStore'];	
					$this -> JobTitle = $postData['JobTitle'];
					$this -> Username = $postData['userNameLogin'];
	                $this -> password = $postData['userPassword'];
					
					
					if($this -> ValidateReferralUser()) {
						
						if($this -> password !=='') {
							
	                        $salt = Hash::generateSalt();	
							
							$postData = array('firstName' => $this -> firstName,
				            				  'lastName' => $this -> lastName,
				            				  'email' => $this -> UserEmail,
				            				  'userType' => 3,
				            				  'store' => $this -> UserStore,
				            				  'JobTitle' => $this -> JobTitle,
											  'userName' => $this -> Username,
											  'password' => Password::SetPassword($this-> password, $salt),
	                                          'salt' => $salt);		
											  
							if(LIVE_SITE == true) {
		                        $content = array();
		                        $content['username'] = $this -> Username;
		                        $content['password'] = $this -> password;
		
		                        $email -> to = $this -> UserEmail;
		                        $email -> subject = "Your Credentials";
		                        $email -> UserSignupNotification($content);
		                    }				  
											  					
							
	                    } else {
	                        $postData = array('firstName' => $this -> firstName,
				            				  'lastName' => $this -> lastName,
				            				  'email' => $this -> UserEmail,
				            				  'userType' => 3,
				            				  'store' => $this -> UserStore,
				            				  'JobTitle' => $this -> JobTitle,
											  'userName' => $this -> Username);
	                    }
						
						
						
						
						
						$this->db->update('users', $postData, array('userID' => $id));
						
						$this -> json -> outputJqueryJSONObject("success", array("firstName" => $this -> firstName,
																				 "lastName" => $this -> lastName));				  
					}
					
					break;
				case "deactivate":
					$this -> deactivationReason = $postData['deactiveReason'];
					
					if($this -> ValidateDeactivateUser()) {	
						$postData = array('isActive' => 2,
										  'deactiveReasonStatus' => $this -> deactivationReason);
			            
			            $this->db->update('users', $postData, array('userID' => $id));
						$this -> json -> outputJqueryJSONObject("success", true);
					}
				
					break;
				
			}

		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "User API - Put Error: " . $e->getMessage();
			$TrackError -> type = "User API - PUT USER ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		}	
	}

	public function DELETE($id) {
		$sth = $this -> db -> prepare("DELETE FROM users WHERE userID = :id");
		$sth -> execute(array('id' => $id));	
		
		$permissionDelete = $this -> db -> prepare("DELETE FROM permissions WHERE userID = :userID");
		$permissionDelete -> execute(array(':userID' => $id));
		
	}

	
	public function ValidateDeactivateUser() {
		if($this -> validate -> emptyInput($this -> deactivationReason)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Deactivation Reason"));
			return false;
		}
		return true;
	}
	


	private function ValidateRequest() {
		$emailCheck = $this->db->prepare('SELECT email FROM users WHERE email = ?');
		$emailCheck->execute(array($this -> UserEmail));
		
		
		if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("First Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Last Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Email"));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if(count($emailCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateEmail());
			return false;
		}  else if($this -> validate -> emptyDropDown($this -> JobTitle)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Your Job title"));
			return false;
		} else if($this -> validate -> emptyDropDown($this -> UserStore)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Store"));
			return false;
		}
		return true;
	}

	private function ValidatePasswordReset() {
		$getCredentials = $this -> db -> prepare("SELECT * FROM users WHERE email = :email");
		$getCredentials -> execute(array(":email" => $this -> UserEmail));
		$getCredentialObject = $getCredentials -> fetch();
				
		$userID = $getCredentialObject['userID'];
				
		if($this -> validate -> emptyInput($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailRequiredRetriveCredentials());
			return false;
		} else if(!Email::verifyEmailAddress($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if($this -> validate -> noEmailMatch($this -> UserEmail, $getCredentialObject['email'])) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "<strong>Authentication Denied</strong><br />That email is not associated with any account.  Please contact Jarrod @ 402-505-4226 for account reset help.");
			return false;
		}
		return true;
	}

	private function ValidateNewPassword() {
		if($this -> validate -> emptyInput($this -> password)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Password"));
			return false;
		} else if($this -> validate -> passwordLength($this -> password, 6)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> passwordLengthMessage("6"));
			return false;
		}
		return true;
	}
	

	private function ValidateNewCredentials() {
		$userNameCheck = $this->db->prepare('SELECT userName FROM users WHERE userName = ?');
	    $userNameCheck->execute(array($this -> Username));
			
		if($this -> validate -> emptyInput($this -> Username)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Username"));
			return false;
		} else if (count($userNameCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateUsername());	
			return false;
		} else if($this -> validate -> emptyInput($this -> password)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Password"));
			return false;
		} else if($this -> validate -> passwordLength($this -> password, 6)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> passwordLengthMessage("6"));
			return false;
		}
		return true;
	}
	
	private function ValidateProfile() {
		$emailCheck = $this->db->prepare('SELECT email FROM users WHERE email = :email and userID <> :userID');
	    $emailCheck->execute(array(':userID' => $this -> userID, ':email' => $this -> UserEmail));
	
	    $userNameCheck = $this->db->prepare('SELECT userName FROM users WHERE userName = :userName and userID <> :userID');
	    $userNameCheck->execute(array(':userName' => $this -> Username, ':userID' => $this -> userID));
		
		if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("First Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Last Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Email"));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if($this -> isNotAdminUser($this -> UserType) and $this -> validate -> emptyDropDown($this -> JobTitle)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Job Title"));
			return false;
		} else if(count($emailCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateEmail());
			return false;	
		} else if($this -> validate -> emptyDropDown($this -> UserType)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("User Type"));
			return false;
		} else if($this -> isNotAdminUser($this -> UserType) and $this -> validate -> emptyDropDown($this -> UserStore)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Store"));
			return false;
		} else if($this -> validate -> emptyInput($this -> Username)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Username"));
			return false;
		} else if (count($userNameCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateUsername());	
			return false;
		} else if ($this -> password !=='' and $this -> validate -> passwordLength($this -> password, 6)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> passwordLengthMessage("6"));
			return false;
		} else if(isset($this -> userIsActive)) {
			if($this -> userIsActive == '0' and $this -> validate -> emptyInput($this -> deactivationReason)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Deactive Reason"));
			return false;
			}
		}
		return true;	
	}

	private function ValidateAproveUser() {
		$emailCheck = $this->db->prepare('SELECT email FROM users WHERE email = :email and userID <> :userID');
	    $emailCheck->execute(array(':userID' => $this -> userID, ':email' => $this -> UserEmail));
		
		if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("First Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Last Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Email"));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if($this -> isNotAdminUser($this -> UserType) and $this -> validate -> emptyDropDown($this -> JobTitle)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Job Title"));
			return false;
		} else if(count($emailCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateEmail());
			return false;	
		} else if($this -> validate -> emptyDropDown($this -> UserType)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("User Type"));
			return false;
		} else if($this -> isNotAdminUser($this -> UserType) and $this -> validate -> emptyDropDown($this -> UserStore)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Store"));
			return false;
		} 
		return true;	
	}


	private function ValidateNewUser() {
		$emailCheck = $this->db->prepare('SELECT email FROM users WHERE email = ?');
	    $emailCheck->execute(array($this->UserEmail));
	
	
	    $userNameCheck = $this->db->prepare('SELECT userName FROM users WHERE userName = ?');
	    $userNameCheck->execute(array($this->Username));
				
		//no first name entered
		if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("First Name"));
			return false;
			//not using letters
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Last Name"));
			return false;
			// email is required
		} else if($this -> validate -> emptyInput($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Email"));
			return false;
			//email verification 
		} else if($this -> validate -> correctEmailFormat($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
			//duplicate email check
		} else if($this -> isNotAdminUser($this -> UserType) and $this -> validate -> emptyDropDown($this -> JobTitle)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Job Title"));
			return false;
		} else if(count($emailCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateEmail());
			return false;
			//empty password
		} else if($this -> validate -> emptyDropDown($this -> UserType)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("User Type"));
			return false;
		} else if($this -> isNotAdminUser($this -> UserType) and $this -> validate -> emptyDropDown($this -> UserStore)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Store"));
			return false;
		} else if($this -> validate -> emptyInput($this -> Username)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Username"));
			return false;
		} else if ($this -> validate -> emptyInput($this -> password)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Password"));
			return false;
		} else if ($this -> validate -> passwordLength($this -> password, 6)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> passwordLengthMessage("6"));
			return false;
			//password match check
		} else if (count($userNameCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateUsername());	
			return false;
			//no errors create new user
		}
		return true;
	}

	public function ValidateReferralUser() {
		if(isset($this -> userID)) {
			$emailCheck = $this->db->prepare('SELECT email FROM users WHERE email = :email AND userID <> :userID');
	    	$emailCheck->execute(array(":email" => $this->UserEmail, ":userID" => $this -> userID));
			
			$userNameCheck = $this->db->prepare('SELECT userName FROM users WHERE userName = :userName AND userID <> :userID');
	    	$userNameCheck->execute(array(":userName" => $this->Username, ":userID" => $this -> userID));
			
		} else {
			$emailCheck = $this->db->prepare('SELECT email FROM users WHERE email = ?');
	    	$emailCheck->execute(array($this->UserEmail));	
			
			$userNameCheck = $this->db->prepare('SELECT userName FROM users WHERE userName = ?');
	    	$userNameCheck->execute(array($this->Username));
		}
				
		
		if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("First Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Last Name"));
			return false;
			// email is required
		} else if($this -> validate -> emptyInput($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Email"));
			return false;
			//email verification 
		} else if($this -> validate -> correctEmailFormat($this -> UserEmail)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
			//duplicate email check
		} else if(count($emailCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateEmail());
			return false;
		} else if($this -> validate -> emptyDropDown($this -> JobTitle)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Job Title"));
			return false;
		} else if($this -> validate -> emptyDropDown($this -> UserStore)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Store"));
			return false;
		} else if(isset($this -> userID)) {
			if($this -> validate -> emptyInput($this -> Username)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', "Username cannot be empty");
				return false;
			} else if (count($userNameCheck -> fetchAll())) {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateUsername());	
				return false;
				//no errors create new user
			} else if(!empty($this -> password)) {
				if ($this -> validate -> passwordLength($this -> password, 6)) {
					$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> passwordLengthMessage("6"));
					return false;
					//password match check
				}
			}
		} 
		
		
		return true;
	}


	private function SetUserPermissions($permissionType, $content, $permissionsChecked = array()) {
		$storeIDS = array();
		$storeIDString = NULL;
		foreach($content as $storeID) {
			foreach($permissionsChecked as $isChecked) {
				
				
				
				$permissionTypeCheck = $this->db->prepare("SELECT * FROM permissions WHERE userID = :userID AND PermissionType = :PermissionType AND storeID = :storeID");
                $permissionTypeCheck -> execute(array(':userID' => $isChecked['UserID'], 
                    								  ':storeID' => $isChecked['StoreID'],
													  ':PermissionType' => $permissionType));	
					
				$getPermissionRow = $permissionTypeCheck->fetch();							 
                $getPermissionCount =  $permissionTypeCheck->rowCount();
						
				if(isset($isChecked['Value'])) {
					
					if (!in_array($isChecked['StoreID'], $storeIDS)) {
						array_push($storeIDS, $isChecked['StoreID']);
					}	
						
					if($getPermissionRow == 0) {
						$this -> db -> insert('permissions', array('PermissionType' => $permissionType, 
		                                                           'Enabled' => 1,
		                                                           'storeID' => $isChecked['StoreID'],
		                                                           'userID' => $this -> userID));									
																   
					}
				}	
				
						
			}			
        }
		
		if(count($storeIDS) > 0) {
			$permissionDelete = $this -> db -> prepare("DELETE FROM permissions WHERE storeID NOT IN (" . implode(', ', $storeIDS) . ") AND PermissionType = :PermissionType AND userID = :userID");
		    $permissionDelete -> execute(array(":PermissionType" => $permissionType,
											   ":userID" => $this -> userID));
		} else {
			$permissionDelete = $this -> db -> prepare("DELETE FROM permissions WHERE PermissionType = :PermissionType AND userID = :userID");
	    	$permissionDelete -> execute(array(":PermissionType" => $permissionType,
										       ":userID" => $this -> userID));	
		}
	}

	private function TrueFalseValue($value) {
		$trueFalsevalue = NULL;
		if($value == '0') {
	    	$trueFalsevalue = 0;
	    } else {
	    	$trueFalsevalue = 1; 
	    } 
		
		return $trueFalsevalue;
	}
	
	private function UserTypeValue($userTypeValue, $storeValue, $jobTitleValue) {
		if($userTypeValue == 1) {
			$this -> finalStoreValue = 0;
			$this -> finalJobTitleValue = 0;
	    } else {
	        $this -> finalStoreValue = $storeValue;
	        $this -> finalJobTitleValue = $jobTitleValue;
	    }
	
	}

	private function isNotAdminUser($userType) {
		return $userType == 2 or $userType == 3;
	}

}