<?php

class Referral extends API {
	
	private $_referralID;
	private $_dealNumber;
	private $_soldStockNumber;
	private $_soldDate;
	private $_saleComments;
	private $_lostNotes;
	
	private $TierLevelName;
	private $TierLevelAmount;
	
	private $_isSidDillonEmployee;
	private $_firstname;
	private $_lastname;
	private $_email;
	private $_phone;
	private $_address;
	private $_city;
	private $_state;
	private $_zip;
	
	private $_clubID;
	
	private $_isMemberActive;
	private $_deactiveReason;
	
	private $_notifyMember;
	
	
	public function ImportMembers() {
		$referralMembers = array_map('str_getcsv', file('ReferralMembers.csv'));
	    array_walk($referralMembers, function(&$a) use ($referralMembers) {
	      $a = array_combine($referralMembers[0], $a);
	    });
		
		
		 array_shift($referralMembers);
		 
		 
		 
		 foreach($referralMembers as $memberSingle) {
		
			$this -> db -> insert('referralmembers  members', array('memberFirstName' => $memberSingle, 
															  		'memberLastName' => $memberSingle,
															  		'memberEmail' => $memberSingle,
															  		'phone' => preg_replace("/[^0-9]/", "", $memberSingle),
															  		'address' => $memberSingle,
																	'city' => $memberSingle, 
																	'state' => $memberSingle,
																	'zip' => $memberSingle,
																	'ClubID' => $memberSingle,
																	'userName' => 0,
																	'password' => 0,
																	'salt' =>'',
																	'securityToken' => '',
																	'isSidDillonRelation' => 0,
																	'memberStartedDate' => $memberSingle,
																	'memberStartedTime' => $memberSingle));	
			
		}
		 
		 
		
	}

	public function ImportReferrals() {
		$referrals = array_map('str_getcsv', file('referrals03.csv'));
	    array_walk($referrals, function(&$a) use ($referrals) {
	      $a = array_combine($referrals[0], $a);
	    });
		
		
		 array_shift($referrals);
		 
		 
		 foreach($referrals as $memberSingle) {
		
			$this -> db -> insert('referrals  members', array('submittedDate' => $memberSingle['Date Submitted']));	
			
		}
		
	}
	
	public function __construct() {
		parent::__construct();
	}	
		
	public function GET($type, $id = NULL) {
		switch($type) {
			case "store";
				$yearPayoutBreakDown = $this -> db -> prepare("SELECT name, COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startJanuary AND submittedDate <= :endJanuary AND storeID = :storeID), 0) January,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startJanuary AND submittedDate <= :endJanuary AND storeID = :storeID) JanuaryCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startFebruary AND submittedDate <= :endFebruary AND storeID = :storeID), 0) February,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startFebruary AND submittedDate <= :endFebruary AND storeID = :storeID) FebruaryCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startMarch AND submittedDate <= :endMarch AND storeID = :storeID), 0) March,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startMarch AND submittedDate <= :endMarch AND storeID = :storeID) MarchCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startApril AND submittedDate <= :endApril AND storeID = :storeID), 0) April,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startApril AND submittedDate <= :endApril AND storeID = :storeID) AprilCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startMay AND submittedDate <= :endMay AND storeID = :storeID), 0) May,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startMay AND submittedDate <= :endMay AND storeID = :storeID) MayCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startJune AND submittedDate <= :endJune AND storeID = :storeID), 0) June,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startJune AND submittedDate <= :endJune AND storeID = :storeID) JuneCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startJuly AND submittedDate <= :endJuly AND storeID = :storeID), 0) July,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startJuly AND submittedDate <= :endJuly AND storeID = :storeID) JulyCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startAugust AND submittedDate <= :endAugust AND storeID = :storeID), 0) August,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startAugust AND submittedDate <= :endAugust AND storeID = :storeID) AugustCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startSeptember AND submittedDate <= :endSeptember AND storeID = :storeID), 0) September,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startSeptember AND submittedDate <= :endSeptember AND storeID = :storeID) SeptemberCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startOctober AND submittedDate <= :endOctober AND storeID = :storeID), 0) October,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startOctober AND submittedDate <= :endOctober AND storeID = :storeID) OctoberCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startNovember AND submittedDate <= :endNovember AND storeID = :storeID), 0) November,
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startNovember AND submittedDate <= :endNovember AND storeID = :storeID) NovemberCount,
																			COALESCE((SELECT sum(PaidAmount) FROM referrals WHERE submittedDate >= :startDecember AND submittedDate <= :endDecember AND storeID = :storeID), 0) December, 
																			(SELECT COUNT(*) FROM referrals WHERE submittedDate >= :startDecember AND submittedDate <= :endDecember AND storeID = :storeID) DecemberCount FROM stores WHERE id = :storeID");
				$yearPayoutBreakDown -> execute(array(':storeID' => $id, 
							        				  ':startJanuary' => $this -> time -> StartOfJanuary(), 
							        				  ':endJanuary' => $this -> time -> EndOfJanuary(),
													  ':startFebruary' => $this -> time -> StartOfFebruary(),
													  ':endFebruary' => $this -> time -> EndOfFebruary(),
													  ':startMarch' => $this -> time -> StartOfMarch(),
													  ':endMarch' => $this -> time -> EndOfMarch(),
													  ':startApril' => $this -> time -> StartOfApril(),
													  ':endApril' => $this -> time -> EndOfApril(),
													  ':startMay' => $this -> time -> StartOfMay(),
													  ':endMay' => $this -> time -> EndOfMay(),
													  ':startJune' => $this -> time -> StartOfJune(),
													  ':endJune' => $this -> time -> EndOfJune(),
													  ':startJuly' => $this -> time -> StartOfJuly(),
													  ':endJuly' => $this -> time -> EndOfJuly(),
													  ':startAugust' => $this -> time -> StartOfAugust(),
													  ':endAugust' => $this -> time -> EndOfAugust(),
													  ':startSeptember' => $this -> time -> StartOfSeptember(),
													  ':endSeptember' => $this -> time -> EndOfSeptember(),
													  ':startOctober' => $this -> time -> StartOfOctober(),
													  ':endOctober' => $this -> time -> EndOfOctober(),
													  ':startNovember' => $this -> time -> StartOfNovember(),
													  ':endNovember' => $this -> time -> EndOfNovember(),
													  ':startDecember' => $this -> time -> StartOfDecember(),
													  ':endDecember' => $this -> time -> EndOfDecember()));
										
					
					echo json_encode(array('storeReferrals' => $yearPayoutBreakDown -> fetch()));	
					break;
					
				case "single":
					echo json_encode(array('ReferralSingle' => $this -> db -> select('SELECT * FROM referrals LEFT JOIN referralmembers ON referrals.memberID = referralmembers.ClubID LEFT JOIN users ON referrals.salesmanUserID = users.userID WHERE referralID =' . $id)));	
					break;	
				case "count":
					echo json_encode($this -> db -> select('SELECT (SELECT COUNT(*) FROM referrals WHERE submittedDate >= "' . date('Y') . '-01-01" AND submittedDate <= "' . date('Y') . '-12-31" AND salesmanUserID = ' . $id . ' LIMIT 1) ReferralCount'));
					break;
				case "salesman":
					echo json_encode(array('SalesmanReferrals' => $this -> db -> select('SELECT * FROM referrals LEFT JOIN referralmembers ON referrals.memberID = referralmembers.ClubID WHERE submittedDate >= "' . date('Y') . '-01-01" AND submittedDate <= "'. date('Y') . '-12-31" AND salesmanUserID = ' . $id . ' ORDER BY submittedDate DESC')));
					break;
				case "tiers":
					echo json_encode(array('tiers' => $this -> db -> select('SELECT * FROM referraltier')));	
					break;
				case "allmembers":
					echo json_encode(array('members' => $this -> db -> select('SELECT * FROM referralmembers ORDER BY memberFirstName ASC')));
					break;
				case "membersingle":
					echo json_encode(array('memberSingle' => $this -> db -> select('SELECT * FROM referralmembers WHERE ClubID = ' . $id)));
					break;
				case "membersinglepaid":
					echo json_encode(array('memberSinglePaid' => $this -> db -> select('SELECT * FROM referrals WHERE Status = 2 AND submittedDate >= "' . date('Y') . '-01-01" AND submittedDate <= "' . date('Y') . '-12-31" AND memberID = ' . $id)));
					break;
				case "membersingleactive":
					echo json_encode(array('memberSingleActive' => $this -> db -> select('SELECT * FROM referrals WHERE Status IN (0, 1) AND submittedDate >= "' . date('Y') . '-01-01" AND submittedDate <= "' . date('Y') . '-12-31" AND memberID = ' . $id)));
					break;
				case "membersingleinactive":
					echo json_encode(array('memberSingleInactive' => $this -> db -> select('SELECT * FROM referrals WHERE Status = 3 AND submittedDate >= "' . date('Y') . '-01-01" AND submittedDate <= "' . date('Y') . '-12-31" AND memberID = ' . $id)));
					break;			
		}
	}

	public function GETMONTH($month, $id) {
		$startOfMonth = NULL;
		$endOfMonth = NULL;

		switch($month) {
			case "january":
				$startOfMonth = $this -> time -> StartOfJanuary(); 
				$endOfMonth = $this -> time -> EndOfJanuary();
				break;
				
			case "february":
				$startOfMonth = $this -> time -> StartOfFebruary();
				$endOfMonth = $this -> time -> EndOfFebruary();
				break;
				
			case "march":
				$startOfMonth = $this -> time -> StartOfMarch();
				$endOfMonth = $this -> time -> EndOfMarch();
				break;
				
			case "april":
				$startOfMonth = $this -> time -> StartOfApril();
				$endOfMonth = $this -> time -> EndOfApril();
				break;
				
			case "may":
				$startOfMonth = $this -> time -> StartOfMay();
				$endOfMonth = $this -> time -> EndOfMay();
				break;
				
			case "june":
				$startOfMonth = $this -> time -> StartOfJune();
				$endOfMonth = $this -> time -> EndOfJune();
				break;
				
			case "july":
				$startOfMonth = $this -> time -> StartOfJuly();
				$endOfMonth = $this -> time -> EndOfJuly();
				break;
				
			case "august":
				$startOfMonth = $this -> time -> StartOfAugust();
				$endOfMonth = $this -> time -> EndOfAugust();
				break;
				
			case "september":
				$startOfMonth = $this -> time -> StartOfSeptember();
				$endOfMonth = $this -> time -> EndOfSeptember();
				break;
				
			case "october":
				$startOfMonth = $this -> time -> StartOfOctober();
				$endOfMonth = $this -> time -> EndOfOctober();
				break;
				
			case "november":
				$startOfMonth = $this -> time -> StartOfNovember();
				$endOfMonth = $this -> time -> EndOfNovember();
				break;
				
			case "december":
				$startOfMonth = $this -> time -> StartOfDecember();
				$endOfMonth = $this -> time -> EndOfDecember();
				break;
		}
		
		$monthReferrals = $this -> db -> prepare("SELECT * FROM referrals LEFT JOIN referralmembers ON referrals.memberID = referralmembers.ClubID WHERE submittedDate >= :startMonth AND submittedDate <= :endMonth AND storeID = :storeID ORDER BY submittedDate DESC");
		$monthReferrals -> execute(array(':storeID' => $id, 
							        	 ':startMonth' => $startOfMonth, 
							        	 ':endMonth' => $endOfMonth));
										
					
		echo json_encode(array('storeMonthReferrals' => $monthReferrals -> fetchAll()));	
		
		
	}


	public function PUT($type, $id) {
		try {
			//get post data
			$input = file_get_contents('php://input');
			$postData = array();
			parse_str($input, $postData);	
			
			
			
		
			$SubmittedSoldDate = explode("/", $this -> _soldDate);
				
			switch($type) {
				case "sold":
					
					$this -> _referralID = $id;
					$this -> _dealNumber = $postData['dealNumber'];
					$this -> _soldStockNumber = $postData['soldStockNumber'];
					$this -> _soldDate = $postData['soldDate'];
					
					$SubmittedSoldDate = explode("/", $this -> _soldDate);
					
					if($this -> ValidateSoldStatus()) {
						$postData = array('SoldDataEntered' => 0,
										  'Status' => 1,
										  'dealNumber' => $this -> _dealNumber, 
										  'soldStockNumber' => $this -> _soldStockNumber, 
										  'isSoldDate' => $SubmittedSoldDate[2] . '-' . $SubmittedSoldDate[0] . '-' . $SubmittedSoldDate[1],
										  'LostNotes' => NULL);
						
						
						
						
						
						$this->db->update('referrals', $postData, array('referralID' => $id));	
						$this -> json -> outputJqueryJSONObject('success', array('dealNumber' => $this -> _dealNumber,
																				 'soldStockNumber' => $this -> _soldStockNumber,
																				 'isSoldDate' => $SubmittedSoldDate[2] . '-' . $SubmittedSoldDate[0] . '-' . $SubmittedSoldDate[1])); 
												
					}
					
					break;
				case "salemansold":
					$this -> _referralID = $id;
					$this -> _dealNumber = $postData['dealNumber'];
					$this -> _soldStockNumber = $postData['soldStockNumber'];
					$this -> _soldDate = $postData['soldDate'];
					
					$SubmittedSoldDate = explode("/", $this -> _soldDate);
					
					if($this -> ValidateSoldStatus()) {
						$postData = array('SoldDataEntered' => 1,
										  'LostDataEntered' => 0,
										  'dealNumber' => $this -> _dealNumber, 
										  'soldStockNumber' => $this -> _soldStockNumber, 
										  'isSoldDate' => $SubmittedSoldDate[2] . '-' . $SubmittedSoldDate[0] . '-' . $SubmittedSoldDate[1]);
						
						$this->db->update('referrals', $postData, array('referralID' => $id));	
						$this -> json -> outputJqueryJSONObject('success', "<strong>Sale Data Entered</strong><br />Thank you for entering the sale data, this referral will be reviewed.");
					}
					
					break;
				case "salesmanlost":
					$this -> _lostNotes = $postData['lostNotes'];
					
					if($this -> ValidateLostReasons()) {
						$postData = array('SoldDataEntered' => 0,
										  'LostDataEntered' => 1,
										  'dealNumber' => NULL, 
										  'soldStockNumber' => NULL, 
										  'isSoldDate' => NULL,
										  'LostNotes' => $postData['lostNotes']);
										  
						$this->db->update('referrals', $postData, array('referralID' => $id));	
						$this -> json -> outputJqueryJSONObject('success', "<strong>Lost Reason Entered</strong><br />Thank you for entering your lost reason, this referral will be reviewed.");				  
					}
					
					break;
				case "paid":
					$NotifyUser = NULL;
					
					if(!isset($postData['NotifyUser'])) {
						$this -> json -> outputJqueryJSONObject('errorMessage', "Would you like to notify the referral member?");
					} else {
						$NotifyUser = $postData['NotifyUser'];
						$TierLevels = $this -> db -> select('SELECT * FROM referraltier');
	
						$getPaidReferralsByMember = $this -> db -> prepare('SELECT * FROM referrals WHERE memberID = :memberID AND Status = 2 AND submittedDate >= :startCurrentYear AND submittedDate <= :endCurrentYear');
				    	$getPaidReferralsByMember -> execute(array(':memberID' => $_GET['memberID'],
																   ':startCurrentYear' => date('Y') . '-01-01',
																   ':endCurrentYear' => date('Y') . '-12-31'));
						$PaidReferralsCount = $getPaidReferralsByMember -> fetchAll();
						
						
						$nextTierLevel = (count($PaidReferralsCount) + 1);
						
						
						
						$getTierLevel = $this -> db -> prepare('SELECT * FROM referraltier WHERE referralTierID = :referralTierID');
						
						if(count($TierLevels) >= $nextTierLevel) {
							$getTierLevel -> execute(array(':referralTierID' => $nextTierLevel));	
						} else {
							$getTierLevel -> execute(array(':referralTierID' => count($TierLevels)));
						}
						
						$getTierAmount = $getTierLevel -> fetch();
						
						
						$postData = array('Status' => 2, 	
										  'SoldDataEntered' => 0,								  
										  'PaidAmount' => $getTierAmount['Amount'],
										  'referralWinnerCount' => $nextTierLevel);
							
						
						$this -> db -> update('referrals', $postData, array('referralID' => $id));
						
						if($NotifyUser == '1'){
							//email notificaitons  
							if(LIVE_SITE == true) {
								
								$memberSingle = $this -> db -> prepare("SELECT * FROM referralmembers WHERE ClubID = :clubID");	
								$memberSingle -> execute(array(':clubID' => $_GET['memberID']));	
								$getMember = $memberSingle -> fetch();
								
								$content = array();
								
								$content['todays-date'] = date("F j, Y / g:i a", $this -> time -> NebraskaTime());
								$content['id'] = $id;
								$content['win-count'] = $nextTierLevel;
								$content['win-amount'] = $getTierAmount['Amount'];
								
								$email = new Email();
					            $email -> to = $getMember['memberEmail'];
					        	$email -> subject = "Paid Referral";
					            $email -> PaidReferral($content);
					        } 
						}	
						
						$this -> json -> outputJqueryJSONObject('success', array('level' => $nextTierLevel,
																					 'earnedAmount' => $getTierAmount['Amount']));
						
						
						
					}
					break;
				case "salecomments":
					$this -> _saleComments = $postData['saleComments'];
					
					$postData = array('SaleComments' => $this -> _saleComments);
					
					$this -> db -> update('referrals', $postData, array('referralID' => $id));
					$this -> json -> outputJqueryJSONObject('success', "Sale Comments Updated");
					
					
					break;
					
				case "lost":
					$this -> _lostNotes = $postData['lostNotes'];
					
					
					
					
					if($this -> ValidateLostReasons()) {
						$postData = array('LostDataEntered' => 0,
										  'Status' => 3,
										  'LostNotes' => $this -> _lostNotes,
										  'dealNumber' => NULL, 
										  'soldStockNumber' => NULL, 
										  'isSoldDate' => NULL);
					
						$this -> db -> update('referrals', $postData, array('referralID' => $id));
						$this -> json -> outputJqueryJSONObject('success', array("lostNotes" => $this -> _lostNotes));
						
						//email notificaitons  
						if(LIVE_SITE == true) {
							//get member ID	
							$memberID = $this -> db -> prepare('SELECT memberID FROM referrals WHERE referralID = :id');
							$memberID -> execute(array(':id' => $id));
							$getMemberID = $memberID -> fetch();
							
							//get email
							$memberSingle = $this -> db -> prepare('SELECT memberEmail FROM referralmembers WHERE ClubID = :clubID');
							$memberSingle -> execute(array(':clubID' => $getMemberID['memberID']));
							$getMemberEmail = $memberSingle -> fetch();
							
				            $link = REFERRAL_PATH . 'login/credentials/create';	
				
							//REFERRAL_PATH
							$email = new Email();
				            $email -> to = $getMemberEmail['memberEmail'];
				        	$email -> subject = "Your Referral is lost";
				            $email -> ReferralLost($this -> _lostNotes);
				        } 
						
						
					}
					
					
					break;
				case "tier":
					$this -> TierLevelName = $postData['tierLevelName'];
					$this -> TierLevelAmount = $postData['tierPrice'];
					
					
						$postData = array('Level' => $this -> TierLevelName,
										  'Amount' => $this -> TierLevelAmount);
										  
						$this -> db -> update('referraltier', $postData, array('referralTierID' => $id));
						$this -> json -> outputJqueryJSONObject('success', array('msg' => $this -> TierLevelName. ' Updated',
																				 'NewAmount' => $this -> TierLevelAmount,
																				 'NewTierName' => $this -> TierLevelName));
					break;
				case "referralmember":
					$this -> _clubID = $id;
					$this -> _isSidDillonEmployee = $postData['isSidDillonEmployee'];
					$this -> _isMemberActive = $postData['isMemberActiveStatus'];
					
					$this -> _firstname = $postData['memberFirstName'];
					$this -> _lastname = $postData['memberLastName'];
					$this -> _email = $postData['memberEmail'];
					$this -> _phone = $postData['memberPhone'];
					$this -> _address = $postData['memberAddress'];
					$this -> _city = $postData['memberCity'];
					$this -> _state = $postData['memberState'];
					$this -> _zip = $postData['memberZip'];
				
					$this -> _deactiveReason = $postData['deactiveReason'];
				
					if($this -> ValidateMember()) {
						
						$dbSidDillonEmployee = NULL;
						$dbIsActiveMember = NULL;
						
						if($this -> _isSidDillonEmployee == '0') {
					    	$dbSidDillonEmployee = 0;
					    } else {
					    	$dbSidDillonEmployee = 1; 
					    } 
						
						
						if($this -> _isMemberActive == '0') {
							$dbIsActiveMember = 0;	
						} else {
							$dbIsActiveMember = 1;
						}
						
						
						
						$postData = array('memberFirstName' => $this -> _firstname,
										  'memberLastName' => $this -> _lastname,
										  'memberEmail' => $this -> _email,
										  'phone' => preg_replace("/[^0-9]/", "", $this -> _phone),
										  'address' => $this -> _address,
										  'city' => $this -> _city,
										  'state' => $this -> _state,
										  'zip' => $this -> _zip,
										  'isSidDillonRelation' => $dbSidDillonEmployee,
										  'ActiveState' => $dbIsActiveMember,
										  'DeactiveStatusReason' => str_replace("\n", '<br />',$this -> _deactiveReason));
			
						
						$this -> db -> update('referralmembers', $postData, array('ClubID' => $this -> _clubID));
						$this -> json -> outputJqueryJSONObject('success', array('msg' => 'Referral Member Updated',
																				 'SidDillonEmployee' => $dbSidDillonEmployee,
																				 'ActiveMember' => $dbIsActiveMember));
																				 
																				 
						$updateReferralsBymember = $this -> db -> prepare('UPDATE referrals SET referralSidDillonRelation = :status WHERE memberID = :memberID');
						$updateReferralsBymember -> execute(array(':status' => $dbSidDillonEmployee, ':memberID' => $this -> _clubID));
					}
				
					break;
				case "salesmanchange":
					
					if($_GET['UserID'] == "0") {
						$this -> json -> outputJqueryJSONObject('errorMessage', "Referral needs to have a salesman assigned");
					} else {
						
						
						
						$postData = array('salesmanUserID' => $_GET['UserID']);
					
						$this -> db -> update('referrals', $postData, array('referralID' => $id));	
						$this -> json -> outputJqueryJSONObject('success', true);
						
						
						if(LIVE_SITE == true) {
					
							//get salesman
							$userSingle = $this -> db -> prepare("SELECT * FROM users WHERE userID = :id");
							$userSingle -> execute(array(':id' => $id));
							$getUser = $userSingle -> fetch();
						
							//get session user
							Session::init();
							
							$sessionUser = $this -> db -> prepare("SELECT * FROM users WHERE userID = :id");
							$sessionUser -> execute(array(':id' => Session::get('user')));
							$getSessionUser = $sessionUser -> fetch();	
							
							$content = array();
							$content['id'] = $id;							
							$content['name'] = $getSessionUser['firstName'] . ' ' . $getSessionUser['lastName'];
							$content['login'] = PATH;
							
							$email = new Email();
				            $email -> to = $getUser['email'];
				        	$email -> subject = "Referral Reassigned";
				            $email -> ReassignedReferral($content);
				        } 
						
						
						
					}
					
					
					
					break;
				case "resetstatus":
						$postData = array('LostNotes' => NULL,
										  'Status' => 0);
				  
						$this -> db -> update('referrals', $postData, array('referralID' => $id));
						$this -> json -> outputJqueryJSONObject('success', true);
					
					break;
				case "deactivate":
					$this -> _deactiveReason = $postData['deactiveReason'];
					
					if($this -> ValidateDeactivateUser()) {	
						$postData = array('ActiveState' => 0,
										  'DeactiveStatusReason' => str_replace("\n", '<br />',$this -> _deactiveReason));
			            
						
						$this -> db -> update('referralmembers', $postData, array('referralMemberID' => $id));
						$this -> json -> outputJqueryJSONObject("success", true);
					}
					break;
				
			}
		
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Referral API - Put Error: " . $e->getMessage();
			$TrackError -> type = "Referral API - PUT USER ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		}	
	}


	public function POST($type) {
		//get post data
		$input = file_get_contents('php://input');
		$postData = array();
		parse_str($input, $postData);	
		
		
		switch($type) {
			case "newmember":
				
				$this -> _isSidDillonEmployee = $postData['isSidDillonEmployee'];
				$this -> _firstname = $postData['memberFirstName'];
				$this -> _lastname = $postData['memberLastName'];
				$this -> _email = $postData['memberEmail'];
				$this -> _phone = $postData['memberPhone'];
				$this -> _address = $postData['memberAddress'];
				$this -> _city = $postData['memberCity'];
				$this -> _state = $postData['memberState'];
				$this -> _zip = $postData['memberZip'];
				
				
				
				if($this -> ValidateMember()) {
					
					$dbSidDillonEmployee = NULL;
						
					if($this -> _isSidDillonEmployee == '0') {
					    $dbSidDillonEmployee = 0;
					} else {
						$dbSidDillonEmployee = 1; 
					} 
					
					//prents duplicate id numbers
					$ClubIDNumber = "";	
						
					do {
					    	
					    $numbers = '0123456789';
						
						$SDNumber = substr(str_shuffle($numbers), 0, 5);
						$clubIDCheck = $this->db->prepare("SELECT * FROM referralmembers WHERE ClubID = :ClubID");
						$clubIDCheck -> execute(array(':ClubID' => $SDNumber));
										
						//$data = $sth->fetch();							 
						$duplicateIDCheck =  $clubIDCheck->rowCount();
						
						
						
						$ClubIDNumber = $SDNumber;
					} while ($duplicateIDCheck > 0);
						
					
					
					$memberID = $this -> db -> insert('referralmembers', array('memberFirstName' => $this -> _firstname, 
															  				   'memberLastName' => $this -> _lastname,
															  				   'memberEmail' => $this -> _email,
															  				   'phone' => preg_replace("/[^0-9]/", "", $this -> _phone),
															  				   'address' => $this -> _address,
																			   'city' => $this-> _city, 
																			   'state' => $this -> _state,
																			   'zip' => $this -> _zip,
																			   'ClubID' => $ClubIDNumber,
																			   'userName' => 0,
																			   'password' => 0,
																			   'salt' =>'',
																			   'securityToken' => '',
																			   'isSidDillonRelation' => $dbSidDillonEmployee,
																			   'memberStartedDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
																			   'memberStartedTime' => date("H:i:s", $this -> time -> NebraskaTime())));	
																			   
					$this -> json -> outputJqueryJSONObject('success', true);														   
																			   
					//email notificaitons  
					if(LIVE_SITE == true) {
			            $link = REFERRAL_PATH . 'login/credentials/create';	
			
						//REFERRAL_PATH
						$email = new Email();
			            $email -> to = $this -> _email;
			        	$email -> subject = "Referrals: Create your credentials";
			            $email -> NewReferralMember($link, 'SD' . $ClubIDNumber);
			        }  														   
				}
				break;	
		}
	}

	private function ValidateDeactivateUser() {
		if($this -> validate -> emptyInput($this -> _deactiveReason)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Deactivation Reason"));
			return false;
		}
		return true;
	}

	private function ValidateMarkAsPaidForm() {
		
	}

	private function ValidateSoldStatus() {
		$submittedDateCheck = $this->db->prepare('SELECT * FROM referrals WHERE referralID = :referralID');
	    $submittedDateCheck->execute(array(':referralID' => $this -> _referralID));
		$getSubmmitedDate = $submittedDateCheck -> fetch();
		
		if(!empty($this -> _soldDate)) {
			$SubmittedSoldDate = explode("/", $this -> _soldDate);
			
			$currentSubmittedDate = new DateTime($getSubmmitedDate['submittedDate']);
			$enteredSubmittedDate = new DateTime($SubmittedSoldDate[2] . '-' . $SubmittedSoldDate[0] . '-' . $SubmittedSoldDate[1]);			
		}

		
		
		if($this -> validate -> emptyInput($this -> _dealNumber)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Deal Number"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _soldStockNumber)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Sold Stock Number"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _soldDate)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Sold Date"));
			return false;
		} else if ($currentSubmittedDate > $enteredSubmittedDate) {
			$this -> json -> outputJqueryJSONObject('errorMessage', 'Sold Date cannot be before Submitted Date.');
			return false;
		}
		return true;
	} 

	private function ValidateMember() {
		//if editing member	
		if(isset($this -> _clubID)) {
			$emailCheck = $this->db->prepare('SELECT memberEmail FROM referralmembers WHERE memberEmail = :email AND ClubID <> :ClubID');
	    	$emailCheck->execute(array(':ClubID' => $this -> _clubID, ':email' => $this -> _email));	
		//if new member
		} else {
			$emailCheck = $this->db->prepare('SELECT memberEmail FROM referralmembers WHERE memberEmail = ?');
			$emailCheck->execute(array($this -> _email));
		}
		
		if($this -> _isMemberActive == '0' and $this -> validate -> emptyInput($this -> _deactiveReason)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Deactive Reason Status"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _firstname)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("First Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _lastname)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Last Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _email)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Email"));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> _email)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if(count($emailCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> duplicateEmail());
			return false;	
		} else if($this -> validate -> emptyInput($this -> _phone)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Phone"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _address)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Address"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _city)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("City"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _state)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("State"));
			return false;
		} else if($this -> validate -> emptyInput($this -> _zip)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Zip"));
			return false;
		}
		return true;
	}

	private function ValidateLostReasons() {
		if($this -> validate -> emptyInput($this -> _lostNotes)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Reason"));
			return false;
		}
		return true;
	}


}