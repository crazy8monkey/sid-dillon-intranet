<?php

require 'config/paths.php';
require 'config/database.php';
require 'config/globals.php';

require_once LIBS . 'init.php';
require_once APIS . 'init.php';


// Load the Bootstrap
$bootstrap = new Bootstrap();
// Optional Path Settings
$bootstrap->setControllerPath('api');
//$bootstrap->setModelPath();
//$bootstrap->setDefaultFile();
//$bootstrap->setErrorFile();

$bootstrap->init();