<?php
class Password {
	private $salt;
	
	public static function ValidatePasswordAttempt($pass, $salt, $passwordAttempted) {
        return Hash::create('sha256', $passwordAttempted, $salt) == $pass;
    }

    public static function SetPassword($password, $salt) {
        return Hash::create('sha256', $password, $salt);
    }
	


}