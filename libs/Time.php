<?php

//http://www.pontikis.net/tip/?id=18

class Time {
	
	private $nebraskTime;
	
	public function __construct() {
		date_default_timezone_set('UTC');		
	}
	
	public function NebraskaTime() {
		$this -> nebraskTime = new DateTime("now", new DateTimeZone('America/Chicago'));		
		return $this -> nebraskTime -> getTimestamp() + $this -> nebraskTime -> getOffset();
	}
	

	public function StartOfWeek() {return $this -> formateDateByDigits(strtotime( "monday this week". "America/Chicago" ));}	
	public function EndOfWeek() {return $this -> formateDateByDigits(strtotime( "saturday this week". "America/Chicago"));}
	
	public function StartOfWeekSQL() {return $this -> formateDateByMYSQLDate(strtotime( "monday this week". "America/Chicago" ));}	
	public function EndOfWeekSQL() {return $this -> formateDateByMYSQLDate(strtotime( "saturday this week". "America/Chicago" ));}	
	
	public function StartOfYearSQL() {return $this -> formateDateByMYSQLDate(strtotime('first day of January '.date('Y') ));}	
	public function EndOfYearSQL() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of December '.date('Y') ));}

	
	
	//january
	public function StartOfJanuary() {return $this -> formateDateByMYSQLDate(strtotime('first day of January '.date('Y') ));}	
	public function EndOfJanuary() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of January '.date('Y') ));}
	//february
	public function StartOfFebruary() {return $this -> formateDateByMYSQLDate(strtotime('first day of February '.date('Y') ));}	
	public function EndOfFebruary() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of February '.date('Y') ));}
	//march
	public function StartOfMarch() {return $this -> formateDateByMYSQLDate(strtotime('first day of March '.date('Y') ));}	
	public function EndOfMarch() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of March '.date('Y') ));}
	//april
	public function StartOfApril() {return $this -> formateDateByMYSQLDate(strtotime('first day of April '.date('Y') ));}	
	public function EndOfApril() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of April '.date('Y') ));}
	//may
	public function StartOfMay() {return $this -> formateDateByMYSQLDate(strtotime('first day of May '.date('Y') ));}	
	public function EndOfMay() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of May '.date('Y') ));}
	//june
	public function StartOfJune() {return $this -> formateDateByMYSQLDate(strtotime('first day of June '.date('Y') ));}	
	public function EndOfJune() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of June '.date('Y') ));}
	//july
	public function StartOfJuly() {return $this -> formateDateByMYSQLDate(strtotime('first day of July '.date('Y') ));}	
	public function EndOfJuly() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of July '.date('Y') ));}
	//august
	public function StartOfAugust() {return $this -> formateDateByMYSQLDate(strtotime('first day of August '.date('Y') ));}	
	public function EndOfAugust() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of August '.date('Y') ));}
	//septebmer
	public function StartOfSeptember() {return $this -> formateDateByMYSQLDate(strtotime('first day of September '.date('Y') ));}	
	public function EndOfSeptember() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of September '.date('Y') ));}
	//october
	public function StartOfOctober() {return $this -> formateDateByMYSQLDate(strtotime('first day of October '.date('Y') ));}	
	public function EndOfOctober() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of October '.date('Y') ));}
	//november
	public function StartOfNovember() {return $this -> formateDateByMYSQLDate(strtotime('first day of November '.date('Y') ));}	
	public function EndOfNovember() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of November '.date('Y') ));}
	//december
	public function StartOfDecember() {return $this -> formateDateByMYSQLDate(strtotime('first day of December '.date('Y') ));}	
	public function EndOfDecember() {return $this -> formateDateByMYSQLDate(strtotime( 'last day of December '.date('Y') ));}
	
	
	
	public function formatDate($value) {
		$date = DateTime::createFromFormat('Y-m-d', $value);
		$date -> setTimezone(new DateTimeZone('America/Chicago'));
		$formatedDate = $date->format('F j, Y');
		return $formatedDate;
	}
	
	
	
	public function SaveUTCDayStamp() {
		$date = DateTime::createFromFormat('Y-m-d', $value);
		$date -> setTimezone(new DateTimeZone('America/Chicago'));
		$formatedDate = $date->format('Y-m-d');
		return $formatedDate;
	}
	
	
	public function SaveUTCTimeStamp() {
		$date = DateTime::createFromFormat('H:i:s', $value);
		$date -> setTimezone(new DateTimeZone('America/Chicago'));
		$formatedDate = $date->format('g:i a');
		return $formatedDate;
	}
	
	
	public function formatTime($value) {
		$date = DateTime::createFromFormat('H:i:s', $value);
		$date -> setTimezone(new DateTimeZone('America/Chicago'));
		$formatedDate = $date->format('g:i a');
		return $formatedDate;	
	}
	
	public function formateDateByDigits($time) {
		return date("m/d/y", $time);
	}
	
	public function formateDateByMYSQLDate($time) {
		return date("Y-m-d", $time);
	}
	
	public function getCurrentWeek() {
		return $this -> StartOfWeek() . " - " . $this -> EndOfWeek();
	}
	
	
}

?>