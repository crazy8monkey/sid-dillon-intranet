<?php

class Pages {
	
	public function save($type) {
		return PATH . 'admin/save/' . $type;
	}
	
	public function delete(string $type) {
		return PATH . 'admin/delete/' . $type;
	}
	
	public function loginFormURL() {return PATH . 'login/login';}
	
	public function adminMainPage() {return PATH . '#/admin/';}
	public function adminUsersList() {return PATH . '#/admin/settings/users';}
	public function adminStoreList() {return PATH . '#/admin/settings/stores';}
	
	public function adminSideReadOnlyView($type, $id) {return PATH . '#/admin/marketing/' . $type . '-posts/'. $id .'/view';}
	
	
	public function StoreSingleLoginPage() {return PATH . '#/admin/';}
	
	public function loginPage() {return PATH . 'login#/'; }
}