<?php

class Hash
{
	
	/**
	 *
	 * @param string $algo The algorithm (md5, sha1, whirlpool, etc)
	 * @param string $data The data to encode
	 * @param string $salt The salt (This should be the same throughout the system probably)
	 * @return string The hashed/salted data
	 */
	public static function create($algo, $data, $salt)
	{
		
		$context = hash_init($algo, HASH_HMAC, $salt);
		hash_update($context, $data);
		
		return hash_final($context);
		
	}
	
	public static function createRandomString($type) {
		$alpha_numeric = '';
		$number = '';	
		switch($type) {
			case "password";
				$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
				$number = 6;
				break;
			case "warranty";
				$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
				$number = 10;
				break;
		}
		
		
		$finalString = substr(str_shuffle($alpha_numeric), 0, $number);
		return $finalString;
		
	}
	
	public static function createWarrantyVerificationID() {
		$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		
		$finalImage = substr(str_shuffle($alpha_numeric), 0, 10);
		return $finalImage;
		
	}
	
	public static function generateSecurityToken(){
        return substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,256))),1,256);
    }
	

    public static function generateSalt(){
        return substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,128))),1,28);
    }
	
}