<?php

//require 'libs/Observer/Popup.php';

class Database extends PDO {

	public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_CHARSET, $DB_USER, $DB_PASS) {
		parent::__construct($DB_TYPE . ':host=' . $DB_HOST . ';dbname=' . $DB_NAME .';port=3306;charset=' . $DB_CHARSET,  $DB_USER, $DB_PASS);
		//line that tells the pdo to catch errors
		parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
	}
	
	/**
	 * select
	 * @param string $sql An SQL string
	 * @param array $array Paramters to bind
	 * @param constant $fetchMode A PDO Fetch mode
	 * @return mixed
	 */
	public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC)
	{
		try {
			//prepares the sql statement
			$sth = $this->prepare($sql);
			foreach ($array as $key => $value) {
				$sth->bindValue("$key", $value);
			}
			
			$sth->execute();
			return $sth->fetchAll($fetchMode);		
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "MySql Select Error: " . $e->getMessage();
			$TrackError -> type = "MYSQL SELECT ERROR:";
			$TrackError -> SendMessage();
		}
		
		
	}
	
	
	/**
	 * insert
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
     * @return id of inserted function
	 */
	public function insert($table, $data)
	{
			ksort($data);
			
			$fieldNames = implode('`, `', array_keys($data));
			$fieldValues = ':' . implode(', :', array_keys($data));
			
			$sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");
	
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
			$sth->execute();
			
			return $this->lastInsertId();
			
		
			//sorts an array in order
			
		
	}
	
	/**
	 * update
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
	 * @param string $where the WHERE query part
	 */
	public function update($table, $data, $where)
	{
			ksort($data);
			ksort($where);
			
			$fieldDetails = NULL;
			$whereDetails = NULL;
			
			foreach($data as $key=> $value) {
				$fieldDetails .= "`$key`=:$key,";
			}
			
			foreach($where as $key2 => $value2) {
				$whereDetails .= "`$key2`=:$key2 AND";
			}
			$fieldDetails = rtrim($fieldDetails, ',');
			$whereDetails = rtrim($whereDetails, 'AND');
			
			$sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $whereDetails");
			
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
			
			foreach($where as $key2 => $value2) {
				$sth->bindValue(":$key2", $value2);
			}
			
			$sth->execute();
		
		//$sth->execute();
	}
	
	/**
	 * delete
	 * 
	 * @param string $table
	 * @param string $where
	 * @param integer $limit
	 * @return integer Affected Rows
	 */
	public function delete($table, $where, $limit = 1)
	{
		return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
	}
	
	public function drop($table) {
		$sth = $this->prepare("DROP TABLE " . $table);
		$sth->execute();
	}
	
	
	
	

}//class Database
?>