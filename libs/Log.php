<?php

class Log
{
	
	/**
	 *
	 * @param string $algo The algorithm (md5, sha1, whirlpool, etc)
	 * @param string $data The data to encode
	 * @param string $salt The salt (This should be the same throughout the system probably)
	 * @return string The hashed/salted data
	 */
	public static function UserAgent($msg)
	{
		//chmod($logfile, 777);
 
		$UserAgentTxt = fopen("user_agent_log.txt", "a+");
		fwrite($UserAgentTxt, $msg);  
		fclose($UserAgentTxt); 
		
	}
	
	public static function render() {
		$fh = fopen("user_agent_log.txt", 'r');	
		$logging = "user_agent_log.txt";
    	$pageText = fread($fh, filesize($logging));
    	return nl2br($pageText);
	}
	
}