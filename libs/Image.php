<?php
Class Image {
	
	public Static function deleteImage($imageName) {
		unlink($imageName);
	}
	/**
	 * renameImage
	 * @param imageFile $imageUploaded
	 * @param string $recipeName
	 * @return renamed image
	 */
	public static function renameImage($imageUploaded) {
		
		
		
		$firstStageImage = $imageUploaded;
		$kaboom = explode(".", $firstStageImage);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);

		$finalImage = $firstStageImage . '-' . strtotime("now") . '.' . $fileExt;
		//preg_replace('#[^a-z.0-9]#i', '-', $recipeName . '.' . $fileExt);
		return $finalImage;
	}
	
	public static function getFileExt($image) {
		$fileExtStart = $image;
		$kaboom = explode(".", $fileExtStart);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);
		return $fileExt;
	}
	/**
	 * moveImage
	 * @param imageFile $imageUploaded
	 * @param imageFileTemp $recipeName
	 * @param session $imagePathDirectory
	 * @param string $recipeName
	 */
	public static function moveImage($image, $imageTemp, $ImagePathDirectory) {				
		$fileName = $image;
		$fileTmpLoc = $imageTemp;
		// Path and file name
		$pathAndName = $ImagePathDirectory.$fileName;
		// Run the move_uploaded_file() function here
		move_uploaded_file($fileTmpLoc, $pathAndName);
	}
	

	public static function resizeImage($target, $newcopy, $w, $h) {
	    list($w_orig, $h_orig) = getimagesize($target);
   		$scale_ratio = $w_orig / $h_orig;
   		if (($w / $h) > $scale_ratio) {
    	       $w = $h * $scale_ratio;
    	} else {
    	       $h = $w / $scale_ratio;
    	}
		
		$fileExtStart = $target;
		$kaboom = explode(".", $fileExtStart);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);
		
    	$img = "";
    	$ext = strtolower($fileExt);
    	if ($ext == "gif"){ 
    	$img = imagecreatefromgif($target);
    	} else if($ext =="png"){ 
    	$img = imagecreatefrompng($target);
    	} else { 
    	$img = imagecreatefromjpeg($target);
    	}
    	$tci = imagecreatetruecolor($w, $h);
    	// imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    	imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    	if ($ext == "gif"){ 
    	    imagegif($tci, $newcopy);
    	} else if($ext =="png"){ 
    	    imagepng($tci, $newcopy);
    	} else { 
    	    imagejpeg($tci, $newcopy);
    	}
	}
	
	public static function receiptImage($target, $newcopy, $w, $h) {
		Image::resizeImage($target, $newcopy, $w, $h);
	}
	
	
	/**
	 * moveImage
	 * @param imageFile $image
	 * @param int $crop_width
	 * @param int $crop_height
	 * @param session $imageDirectory
	 * @param string $recipeName
	 */
	public static function cropImage($image, $crop_width, $crop_height, $imageDirectory) {
		
		//http://salman-w.blogspot.com/2009/04/crop-to-fit-image-using-aspphp.html
		$sourceImage = $imageDirectory . $image;
		
		//lower case the file extension
		$string = $image;
		$new_string = pathinfo($string, PATHINFO_FILENAME) . '.' . strtolower(pathinfo($string, PATHINFO_EXTENSION));
		
		$kaboom = explode(".", $new_string);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);		
		
		list($source_width, $source_height) = getimagesize($sourceImage);
		
		switch ($fileExt) {
			case 'jpg';
				$source_gdim = imagecreatefromjpeg($sourceImage);
				break;
			case 'jpeg';
				$source_gdim = imagecreatefromjpeg($sourceImage);
				break;
			case 'gif';
				$source_gdim = imagecreatefromgif($sourceImage);
				break;
			case 'png';
				$source_gdim = imagecreatefrompng($sourceImage);
				break;
		}
		
		//adjust image width
		
		$source_aspect_ratio = $source_width / $source_height;
		$desired_aspect_ratio = $crop_width / $crop_height;
		
		if ($source_aspect_ratio > $desired_aspect_ratio) {
		    /*
		     * Triggered when source image is wider
		     */
		    $temp_height = $crop_height;
		    $temp_width = ( int ) ($crop_height * $source_aspect_ratio);
		} else {
		    /*
		     * Triggered otherwise (i.e. source image is similar or taller)
		     */
		    $temp_width = $crop_width;
		    $temp_height = ( int ) ($crop_width / $source_aspect_ratio);
		}
		
		/*
		 * Resize the image into a temporary GD image
		 */
		
		$temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
		imagecopyresampled($temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height);
		
		
		$x0 = ($temp_width - $crop_height) / 2;
		$y0 = ($temp_height - $crop_height) / 2;
		$desired_gdim = imagecreatetruecolor($crop_height, $crop_height);
		
		imagecopy($desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $crop_width, $crop_height);
		
		switch ($fileExt) {
			case 'jpg';
				//header('Content-type: image/jpg');
				imagejpeg($desired_gdim, $sourceImage);
				break;
			case 'jpeg';
				//header('Content-type: image/jpeg');
				imagejpeg($desired_gdim, $sourceImage);
				break;	
			case 'gif';
				//header('Content-type: image/gif');
				imagegif($desired_gdim, $sourceImage);
				break;
			case 'png';
				//header('Content-type: image/png');
				imagepng($desired_gdim, $sourceImage);
				break;
		}	
	}
	public static function copyImage($source, $destination, $quantity) {
		
		list($w_orig, $h_orig) = getimagesize($source);
		if($w_orig > 1000) {
			Image::resizeImage($source, $source, 1000, 1000);
		}
		
		
		
		
		copy($source, $destination);
		
		Image::compress($destination, $destination, $quantity);
	}


	public static function compress($source, $destination, $quality) {

	    $info = getimagesize($source);
		
		switch ($info['mime']) {
			case 'image/jpg';
				$image = imagecreatefromjpeg($source);
				imagejpeg($image, $destination, $quality);
				break;
			case 'image/jpeg';
				$image = imagecreatefromjpeg($source);
				imagejpeg($image, $destination, $quality);
				break;
			case 'image/gif';
				$image = imagecreatefromgif($source);
				imagegif($image, $destination, $quality);
				break;
			case 'image/png';
				$image = imagecreatefrompng($source);
			
				// Do required operations
		
				// Turn off alpha blending and set alpha flag
				imagealphablending($image, true);
				imagesavealpha($image, true);
			
				imagepng($image, $destination, ($quality/10));
				break;
		}

		return $destination;
	}

//$source_img = 'source.jpg';
//$destination_img = 'destination .jpg';

//$d = compress($source_img, $destination_img, 90);

	
	
	/**
	 * validateImageWidth
	 * @param imageFile $image
	 * @param int $crop_width
	 * @param int $crop_height
	 * @param session $imageDirectory
	 * @param string $recipeName
	 */
	public static function getImageWidth($imageDirectoryFolder, $image) {
		list($width) = getimagesize($imageDirectoryFolder . $image);
		return $width;
	}
}//class image
?>