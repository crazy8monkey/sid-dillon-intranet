<?php

Class Email {
			
	public $to;
	public $subject;
	private $msg;
	private $header;
	
	public function __construct() {
		$this -> header = 'From: Sid Dillon Intranet <SidDillonIntranet@donotreply.com>'. "\r\n";
		$this -> header .= 'MIME-Version: 1.0' . "\r\n";
		$this -> header .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
		$this -> header .= 'Bcc: netadmin@siddillon.com, adam.schmidt@siddillon.com' . "\r\n";	
	}
	
	private function emailLogo() {
		return "<img src='" . PATH . "public/images/SidDillonLogoEmail.png' />";
	}

	public function UserSignupNotification($content) {
		
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding:15px; background:black; color:white; font-family: arial; text-align:center; font-size:30px'>
											Sid Dillon Intranet
										</td>
									</tr>
									<tr>
										<td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial;'>Your User Credentials</td>
									</tr>
									<tr>
										<td style='padding:30px; background:white'>
											<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='350' align='center'>
												<tr>
													<td style='font-weight:bold; font-family:arial; font-size:25px'>Username:</td>
													<td style='font-family:arial; font-size:25px'>" .$content['username'] . "</td>
												</tr>
												<tr>
													<td style='font-weight:bold; font-family:arial; font-size:25px; padding:0px 0px 25px 0px'>Password:</td>
													<td style='font-family:arial; font-size:25px; padding:0px 0px 25px 0px'>" . $content['password'] . "</td>
												</tr>
												<tr>
													<td colspan='2' style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . PATH . "login#/' style='text-decoration:underline; color:white;'>
															Login
														</a>
													</td>
												</tr>
												<tr>
													<td colspan='2' style='font-family:arial; text-align:center; padding:20px 0px 0px 0px; font-size:20px;'>
														If you have any questions please call <br />Jarrod at (402) 505-4226
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	public function sendIncompleteEmailMaster($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr>
										<td style='padding:15px; background:black; color:white; font-family: arial; text-align:center; font-size:30px'>
											Sid Dillon Intranet
										</td>
									</tr>
									<tr>
										<td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial;'>Store Completion Updates</td>
									</tr>"
								. $content. 	
								"</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	
	public function sendIncompleteEmailSingle($content, $percentage) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr>
										<td style='padding:15px; background:black; color:white; font-family: arial; text-align:center; font-size:30px'>
											Sid Dillon Intranet
										</td>
									</tr>
									<tr>
										<td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial;'>" . $percentage . " Complete this week</td>
									</tr>"
								. $content. 	
								"</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	public function resetPassword($link, $newcredentials = false) {
		$emailText = "";
		$headerText = "";
		
		if($newcredentials) {
			$emailText = $newcredentials;
			$headerText = "Create your Credentials";
		} else {
			$emailText = "Click on the link below to reset your Sid Dillon Intranet password.";
			$headerText = "Reset your Password";
		}
		
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding:15px; background:black; color:white; font-family: arial; text-align:center; font-size:30px'>
											Sid Dillon Intranet
										</td>
									</tr>
									<tr>
										<td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial;'>" . $headerText . "</td>
									</tr>
									<tr>
										<td style='padding:30px;'>
											<table cellpadding='0' cellspacing='0'>
												<tr>
													<td style='font-family:arial; text-align:center; padding:0px 0px 20px 0px; font-size:20px;'>". $emailText . "</td>
												</tr>
												<tr>
													<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . $link . "' style='text-decoration:underline; color:white;'>" . $headerText . "</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>

								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
  
  public function NewRequest($content) {
    		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding:15px; background:black; color:white; font-family: arial; text-align:center; font-size:30px'>
											Sid Dillon Intranet
										</td>
									</tr>
									<tr>
										<td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial;'>New User Request</td>
									</tr>
									<tr>
										<td style='padding:30px 30px 5px 30px; font-weight:bold; font-family:arial; font-size:25px;'>
											User Information
										</td>
									</tr>
									<tr>
										<td style='padding:10px 30px 30px 30px; font-weight:bold; font-family:arial; font-size:25px;'>
											<table cellpadding='0' cellspacing='0' width='100%'>
												<tr>
													<td style='font-size:16px; font-weight:bold; padding:0px 10px 0px 0px'>Full Name:</td>
													<td style='font-size:16px; padding:0px'>" . $content['first-name'] . " " . $content['last-name'] . "</td>
												</tr>
												<tr>
													<td style='font-size:16px; font-weight:bold; padding:0px 10px 0px 0px'>Email: </td>
													<td style='font-size:16px; padding:0px'>" . $content['email'] . "</td>
												</tr>
												<tr>
													<td style='font-size:16px; font-weight:bold; padding:0px 10px 0px 0px'>Job Title: </td>
													<td style='font-size:16px; padding:0px'>" . $content['user-type'] . "</td>
												</tr>
												<tr>
													<td style='font-size:16px; font-weight:bold; padding:0px 10px 10px 0px'>Store:</td>
													<td style='font-size:16px; padding:0px 0px 10px 0px'>" . $content['store'] . "</td>
												</tr>
												<tr>
													<td colspan='2' style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . PATH . "#/' style='text-decoration:underline; color:white;'>
															Login
														</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									
								</table>
							</body>
						</html>";
    	$this -> SendEmail();
  	}
	
	public function DeclinedPost($content) {
			
		
		$this -> msg = "<html style='padding:0px; margin:0px;'>
				<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
					<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
						<tr>
							<td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td>
						</tr>
						<tr>
							<td style='padding:15px; background:black; color:white; font-family: arial; text-align:center; font-size:30px'>
								Sid Dillon Intranet
							</td>
						</tr>
						<tr>
							<td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial;'>" . ucfirst($content['type']) . " Post Declined</td>
						</tr>
						<tr>
							<td style='padding:15px; background:white; border-top:1px solid #ececec'>
								<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%'>
									<tr>
										<td style='padding:0px; vertical-align: top'>
											<table>
												<tr>
													<td style='font-weight:bold; font-family:arial; font-size:25px; padding:0px'>
														Your Post		
													</td>
												</tr>
												<tr>
													<td style='padding:15px 20px 0px 20px; font-family:arial;'>". $content['post-type'] . "</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style='padding:15px; background:white; border-top:1px solid #ececec'>
								<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='100%'>
									<tr>
										<td style='padding:0px; vertical-align: top'>
											<table>
												<tr>
													<td style='font-weight:bold; font-family:arial; font-size:25px; padding:0px'>
														Reason		
													</td>
												</tr>
												<tr>
													<td style='padding:15px 20px 0px 20px; font-family:arial;'>" . $content['reason-notes'] . "</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style='font-weight:bold; font-family:arial; font-size:25px; padding:15px; background:white; border-top:1px solid #ececec; text-align:center;'>Please submit a new " . $content['type'] . " post</td>
						</tr>
						<tr>
							<td style='padding:0px 20px 20px 20px;'>
								<table cellpadding='0' cellspacing='0' width='100%'>
									<tr>
										<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
											<a href='" . PATH . "#/' style='text-decoration:underline; color:white;'>
												Login
											</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</body>
			</html>";
			
		$this -> SendEmail();
	}
	
	
	public function NewReferralMember($link, $clubID) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr><td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial; text-align:center'>Create your credentials</td></tr>
									<tr><td style='padding:20px 30px; background:white;'>
											<table cellpadding='0' cellspacing='0'>
												<tr><td style='font-family:arial; font-size:25px'>Welcome to the Sid Dillon Referral program!</td></tr>
												<tr><td style='font-size:16px; padding:15px 0px 0px 0px; font-family:arial'><strong>Your Club ID: </strong>". $clubID . "<br /><br />Please click this link below to create your credentials</td></tr>
											</table>
										</td>
									</tr>
									<tr><td style='padding:0px 30px 20px 30px; background:white;'>
											<a href='". $link . "' style='color:white;'><div style='background: #a81e28; color: white; width: 100%; text-align: center; font-size: 16px; padding: 15px 0px; font-family:arial'>CREATE YOUR CREDENTIALS</div></a>
										</td></tr>
								</table>
							</body>
						</html>";
		$this -> SendEmail();
	}
	
	public function UpdateInventoryUpdate($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='padding: 15px; color:white; font-size: 30px; font-weight:bold; background:black; font-family: arial;'>Inventory Update Script Results</td>
									</tr>
									<tr><td style='padding: 15px; text-align:center; font-family: arial;'>
										<table>
											<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Todays CSV Count:</td><td style='font-family:arial; font-size:16px;'>" . $content['todays-count'] . "</td></tr>
											<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Update Script Finished:</td><td style='font-family:arial; font-size:16px;'>" . $content['finished-time'] . "</td></tr>
											<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>New Record Count:</td><td style='font-family:arial; font-size:16px;'>". $content['new']. "</td></tr>
											<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Deleted Record Count:</td><td style='font-family:arial; font-size:16px;'>". $content['removed'] . "</td></tr>
											<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Updated Record Count:</td><td style='font-family:arial; font-size:16px;'>". $content['update'] . "</td></tr>
											<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Production Inventory Count:</td><td style='font-family:arial; font-size:16px;'>". $content['total-production-count']. "</td></tr>";
											if($content['errors'] !== '') {
												$this -> msg .= "<tr><td style='font-family:arial; font-size:16px; font-weight:bold;'>Errors: </td><td style='font-family:arial; font-size:16px;'>". $scriptErrors. "</td></tr>";
											}
										$this -> msg .= "</table>
									</td></tr></table></body></html>";
			
		$this -> SendEmail();
	}

	public function ReferralLost($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr><td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial; text-align:center'>Referral Lost</td></tr>
									<tr><td style='padding:20px 30px; background:white;'>
										<table cellpadding='0' cellspacing='0'>
											<tr><td style='font-family:arial; font-size:25px'>Reason why your referral submission was lost</td></tr>
											<tr><td style='font-size:16px; padding:15px 0px 0px 0px; font-family:arial'>" .$content . "</td></tr>
										</table>
									</td></tr>
								</table>
							</body>
						</html>";
									
		$this -> SendEmail();
	}
	
	public function ReassignedReferral($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr><td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial; text-align:center'>New Referral</td></tr>
									<tr><td style='padding:20px 30px; background:white;'>
											<table cellpadding='0' cellspacing='0'>
												<tr><td style='font-family:arial; font-size:25px'>You have a new reassigned referral.</td></tr>
												<tr><td style='font-size:16px; padding:15px 0px 0px 0px; font-family:arial'><strong>". $content['name'] . "</strong> has reassgined referral # RF". $content['id'] . " to you</td></tr>
											</table>
										</td>
									</tr>
									<tr><td style='padding:15px; background:white; border-top:1px solid #ececec'>
											<a href='". $content['login'] . "' style='text-decoration:underline; color:white;'>
												<div style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
													Login								
												</div>							
											</a>
										</td></tr>
								</table>
							</body>
						</html>";			
							
		
		$this -> SendEmail();
	}
	
	public function PaidReferral($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr><td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial; text-align:center'>Paid Referral</td></tr>
									<tr>
										<td style='padding:20px 30px; background:white;'>
											<table cellpadding='0' cellspacing='0'>
												<tr><td style='font-size:16px; padding:15px 0px 0px 0px; font-family:arial'>
														<table cellpadding='0' cellspacing='0'>
															<tr><td style='padding:0px 10px 10px 0px'><strong>Todays Date:</strong></td><td style='padding:0px 0px 10px 0px'>". $content['todays-date'] . "</td></tr>
															<tr><td style='padding:0px 10px 0px 0px'><strong>New Paid Referral:</strong></td><td>RF". $content['id'] . "</td></tr>
															<tr><td style='padding:0px 10px 0px 0px'><strong>Successful Count:</strong></td><td>". $content['win-count'] . "</td></tr>
															<tr><td style='padding:0px 10px 0px 0px'><strong>Paid Amount:</strong></td><td>$". $content['win-amount'] . "</td></tr>
														</table>
													</td></tr>
											</table>
										</td></tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	
	public function UrgentPost($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr><td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial; text-align:center'>Urgent Social Media Post</td></tr>
									<tr>
										<td style='padding:20px 30px; background:white;'>
											<table cellpadding='0' cellspacing='0' width='100%'>
												<tr>
													<td style='padding:0px 0px 20px 0px'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='font-size:16px; font-family:arial; padding:0px 0px 10px 0px'><strong>Store Relation</strong></td>
															</tr>
															<tr>
																<td style='color:black; font-size:24px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:0px 0px 5px 0px'>" . $content['storeName'] . "</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='padding:0px 0px 15px 0px'>
														<table cellpadding='0' cellspacing='0' width='100%'>
															<tr>
																<td style='color:#c30000; font-size:16px; font-family:arial; padding:0px 0px 10px 0px'><strong>Needs to be posted</strong></td>
															</tr>
															<tr>
																<td style='color:black; font-size:24px; font-family:arial; border-bottom:1px solid #e0e0e0; padding:0px 0px 5px 0px'>". $content['urgentTime'] . "</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . PATH . "' style='text-decoration:underline; color:white;'>Login</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	
	
	public function IntranetError($content) {
		$this -> msg = "<table cellpadding='0' cellspacing='5'>" . $content ."</table>";
		$this -> SendEmail();
	}
		
	private function SendEmail() {
		mail($this -> to, $this -> subject . EMAIL_SUBJECT, $this -> msg, $this -> header);
	}
	
	public static function verifyEmailAddress($emailInput) {
		return preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$emailInput);
	}
}