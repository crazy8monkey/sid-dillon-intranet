<?php 

class View {
	
	
	public function __construct() {
		$this-> recordedTime = new Time();
		$this -> visible = New Permissions();
		$this -> security = new Security();
	}
	
	
	
	
	
	
	public function render($name, $noInclude = false) {

		if ($noInclude == true) {
			require 'view/' . $name . '.php';
		} else {
			require 'view/header.php';
			require 'view/' . $name . '.php';
			require 'view/footer.php';
		}
		
	}
	
	
}