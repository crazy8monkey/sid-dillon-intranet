var CurrentAppPath = Globals.currentAppPath();

//Stores
SidDillonIntranet.service('StoresList', function($http) {
	this.storeSingle = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/admin/" + id,
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.GetUserToDoLists = function(data) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/SocialMediaPostProgress?IDS=" + data + "&time=" + new Date().getTime(),
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.GetUserReferralStores = function(data) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/referral?IDS=" + data + "&time=" + new Date().getTime(),
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.GetUserReferralAdminStores = function(data) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/referralAdminStores?IDS=" + data + "&time=" + new Date().getTime(),
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.getAllStores = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/admin",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.GetReferralStores = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/referral",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.getAllToDoLists = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/todo",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.SingleToDoList = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/SocialMediaPostProgress/" + id + "&time=" + new Date().getTime(),
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.AddStore = function(data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "stores/POST",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.SaveStore = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "stores/PUT/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.DeleteStore = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/DELETE/" + id,
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.SubmittedFacebookPosts = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/FacebookPostList/" + id + "&time=" + new Date().getTime(),
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.SubmittedTwitterPosts = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/TwitterPostsList/" + id + "&time=" + new Date().getTime(),
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.SubmittedBlogPosts = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "stores/GET/BlogPostsList/" + id + "&time=" + new Date().getTime(),
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	}
	function handleSuccess( response ) {
    	return( response.data );
    }
});

//referrals
SidDillonIntranet.service('Referrals', function($http) {
	this.getTiers = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/tiers",
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.referralsByStore = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/store/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.referralsByStoreMonth = function(month, id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GETMONTH/" + month + "/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.referralsBySalesman = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/salesman/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.GetSalesmanReferralCount = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/count/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.GetReferralsBySalesman = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/salesman/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.Single = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/single/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.MemberSingle = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/membersingle/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.MemberPaidReferrals = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/membersinglepaid/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},	
	this.MemberActiveReferrals = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/membersingleactive/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},	
	this.MemberInactiveReferrals = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/membersingleinactive/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},	
	this.DeactivateUser = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/deactivate/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	}	
	this.getAllMembers = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "referral/GET/allmembers",
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.MarkAsSold = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/sold/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.SalesmanSoldData = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/salemansold/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.SalesmanLostData = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/salesmanlost/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},		
	this.AdminSaveMember = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/referralmember/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},	
	this.MarkAsLost = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/lost/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.MarkAsPaid = function(id, MemberID, data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/paid/" + id + "?memberID=" + MemberID,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.SalesComments = function(id, data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/salecomments/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.SaveSalesman = function(userID, referralID) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "referral/PUT/salesmanchange/" + referralID + "?UserID=" + userID,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.SaveTier = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/PUT/tier/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.NewMember = function(data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "referral/POST/newmember",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.ResetReferralStatus = function(id) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "referral/PUT/resetstatus/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	}	
	function handleSuccess( response ) {
    	return( response.data );
    }
});

//Users
SidDillonIntranet.service('Users', function($http) {
	this.UserRequest = function(data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/POST/request",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.StoreAdminUsers = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "users/GET/storeadmins/"  + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.EditUser = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/PUT/edituser/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.SaveProfile = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/PUT/profile/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.NewUser = function(data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/POST/newuser",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.NewReferralUser = function(data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/POST/newreferraluser",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.EditReferralUser = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/PUT/referraluser/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	
	this.UserLogin = function(data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/POST/login",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.NewPassword = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/PUT/newpassword/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.singleUser = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "users/GET/user/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.getAllUsers = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "users/GET/all",
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.getReferralUsersByStore = function(ids) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "users/GET/referrals?storeID=" + ids,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	}
	this.getUsersByStore = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "users/GET/store/" + id,
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		});
	},
	this.forgotPassword = function(data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/POST/passwordReset",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.DeleteUser = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "users/DELETE/" + id,
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.newCredentials = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/PUT/credientials/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.ApproveUser = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/PUT/approve/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	},
	this.DeactivateUser = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "users/PUT/deactivate/" + id,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		});
		return (request.then(handleSuccess));
	}
	function handleSuccess( response ) {
    	return( response.data );
    }
});

//System
SidDillonIntranet.service('System', function($http) {
	this.CurrentSessionVariables = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "system/GET/session&time=" + new Date().getTime(),
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.getCurrentWeek= function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "system/GET/week",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.GetCurrentSettings = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "system/GET/settings",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.SaveMainSettings = function(data) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "system/PUT/main",
			data 	: data,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});	
		return (request.then(handleSuccess));
	},
	this.SaveToDoListSettings = function(data) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "system/PUT/todo",
			data 	: data,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});
		return (request.then(handleSuccess));
	},
	this.SystemLogs = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "system/GET/logs",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.PHPInfo = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "system/GET/php",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.CurrentSessionInformation = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "system/GET/currentsession",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.ReferralReportingHistory = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "system/GET/referralreporting",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.Logout = function() {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "system/POST/logout",
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});	
		return (request.then(handleSuccess));		
	},
	this.ReferralReporting = function(data) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "system/POST/reporting?type=referrals",
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});	
		return (request.then(handleSuccess));		
	},
	this.CurrentAppPath = function() {
		return CurrentAppPath;
	}
	function handleSuccess( response ) {
    	return( response.data );
    }
});

//Websites
SidDillonIntranet.service('Websites', function($http) {
	this.getWebsitesByStore = function(id) {
		return $http({
		    method: 'GET',
		    url: CurrentAppPath + "website/GET/" + id,
		    // pass in data as strings
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.SaveWebsites = function(data, id) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "website/POST/" + id,
			data 	: data,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});	
		return (request.then(handleSuccess));
	},
	this.DeleteStore = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "website/DELETE/" + id,
		    // pass in data as strings
		    headers: {'Content-Type': 'application/json'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	}
	function handleSuccess( response ) {
    	return( response.data );
    }
});

//social media list
SidDillonIntranet.service('SocialMediaList', function($http) {
	this.NewPosts = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "socialmedia/GET/NewPosts",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.ApprovedPosts = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "socialmedia/GET/ApprovedPosts",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.DeclinedPosts = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "socialmedia/GET/DeclinedPosts",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.PostedPosts = function() {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "socialmedia/GET/PostedPosts",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.oldSocialMediaList = function(type) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "socialmedia/GET/old/" + type,
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.declinedSocialMediaList = function(type) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "socialmedia/GET/declined/" + type,
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.SinglePost = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "socialmedia/GET/Single/" + id,
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})		
	},
	this.AddPost = function(data, storeID) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "socialmedia/POST/" + storeID,
			data 	: data,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});	
		return (request.then(handleSuccess));
	},
	this.SaveDraft = function(data, id) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "socialmedia/PUT/draft/" + id,
			data 	: data,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});	
		return (request.then(handleSuccess));
	},
	this.MarkAsPosted = function(id) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "socialmedia/PUT/makepostlive/" + id,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});
		return (request.then(handleSuccess));
	},
	this.ApprovePost = function(id) {
		var request = $http({
			method  : 'POST',
			url     : CurrentAppPath + "socialmedia/PUT/approve/" + id,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});
		return (request.then(handleSuccess));
	}
	this.DeclinePost = function(data, id) {
		var request = $http({
			method  : 'POST',
			data 	: data,
			url     : CurrentAppPath + "socialmedia/PUT/decline/" + id,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
		});
		return (request.then(handleSuccess));
	}
	function handleSuccess( response ) {
		return( response.data );
	}
		    		    
});

//Permissions
SidDillonIntranet.service('Permissions', function($http) {
	this.getUserPermissions = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "permissions/GET/all/" + id,
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.getUserMarketingPermissions = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "permissions/GET/user/" + id + "/Marketing",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.getUserReferralAdminPermissions = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "permissions/GET/user/" + id + "/ReferralAdmin",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	},
	this.getUserReferralPermissions = function(id) {
		return $http({
			method: 'GET',
		    url: CurrentAppPath + "permissions/GET/user/" + id + "/Referral",
		    // pass in data as strings
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		}).then(function(data) {
			return data;
		})
	}				    
});


SidDillonIntranet.factory('Authentication', function() {
	var CurrentSessionIsLoggedIn;
	var CurrentSessionUserType;
	var CurrentSessionUserID;
	var CurrentSessionUserStore;
	
	return {
		setCurrentSession: function(session) {
			CurrentSessionIsLoggedIn = session;
		},
		setCurrentSessionUserType: function(sesion) {
			CurrentSessionUserType = sesion;
		},
		setCurrentSessionUserID: function(session) {
			CurrentSessionUserID = session;
		},
		setCurrentSessionUserStore: function(session) {
			CurrentSessionUserStore = session;
		},
		getCurrentSession: function() {
			return CurrentSessionIsLoggedIn
		},
		getCurrentSessionUserType: function() {
			return CurrentSessionUserType	
		},
		getCurrentSessionUserID: function() {
			return CurrentSessionUserID;	
		},
		getCurrentSessionUserStore: function() {
			return CurrentSessionUserStore;
		}
	}
	
});

SidDillonIntranet.service('SessionVariables', function() {
	var CurrentSessionUserType;
	var userID;
	
	return {
		setUserID: function(id) {
			userID = id;
		},
		setCurrentSessionUserType: function(userType) {
			CurrentSessionUserType = userType;
		},
		getCurrentSessionUserType: function() {
			return CurrentSessionUserType	
		},
		getUserID: function() {
			return userID;
		}
	}
});




