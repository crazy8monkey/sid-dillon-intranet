var AdminController = function() {
	
	var SidDillonIntranet = Globals.AngularVariable();
	var currentAppPath = Globals.currentAppPath();
	var pageTitle = " | Sid Dillon Intranet";
	
			
	var storeSingleContentPage = currentAppPath + "admin/singleStorePage";
	
	var addPostPage = currentAppPath + "admin/addPost";
	
	var logsPage = currentAppPath + "admin/logs";
		
	function Initialize() {
		
		SidDillonIntranet.run(['$location', '$rootScope', '$timeout', 'Users', function($location, $rootScope, $timeout, Users) {
			$rootScope.loadingDataText = "Loading Data";
			$rootScope.$on('$routeChangeStart', function(event, next, current) {
				
				Users.getCurrentSessionID().then(function(data) { 
					Users.getSingleUser(data.data.currentSessionID).then(function(data) { 
						$rootScope.UserLoggedInName = data.data.userSingleLoad.firstName + " " +  data.data.userSingleLoad.lastName;
						
						var CanSeeThisPage;
						
						if(next.Permission) {
							CanSeeThisPage = (next.Permission == data.data.userSingleLoad.UserType);
							//alert(CanSeeThisPage);	
							if(!CanSeeThisPage) {
								$rootScope.loadingDataText = "You do not have access to this page";
								$location.path('/to-do-list');
								event.preventDefault();
							} else {
								$rootScope.loadingDataText = "Loading Data";
							}
						}
							
					});
				});
			
				
			
			
				$rootScope.showLoading = true;
				$rootScope.showLoadingText = true;	
				
				
			});
			$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
				
				$rootScope.title = current.$$route.title  + pageTitle;
				$rootScope.finishLoading = Globals.LoadingSpinner();
				$rootScope.settingsMain = currentAppPath + "admin#/settings";
				$rootScope.settingsToDoList = currentAppPath + "admin#/settings/to-do-list"; 
				$rootScope.settingsStores = currentAppPath + "admin#/settings/stores"; 
				$rootScope.settingsUsers = currentAppPath + "admin#/settings/users";
				$rootScope.settingsLogs = currentAppPath + "admin#/settings/logs";
				$rootScope.settingsSystem = currentAppPath + "admin#/settings/system";
				$rootScope.settingsCron = currentAppPath + "admin#/settings/cronjob";
				
				
				
				$rootScope.subContent = current.$$route.subContent;
				
				Users.getCurrentSession().then(function(data) { 
					if(!data.data.currentSession) {
						$rootScope.showLoading = true;
						$rootScope.showLoadingText = false;
						$rootScope.showExpiredMessage = true;
					} else {
						$timeout(function(){ 
							$rootScope.showLoading = false;
						},400);		
					}
				});
				
			}); 
		}]);
		



		SidDillonIntranet.service('StoresList', function($http) {
		    this.getAllStores = function() {
		        return $http({
		            method: 'GET',
		            url: currentAppPath + "admin/getStores",
		            // pass in data as strings
		            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		        }).then(function(data) {
					return data;
		        })
		    },
		    this.getSingleStore = function(id) {
		    	return $http({
		            method: 'GET',
		            url: currentAppPath + "admin/getSingleStore/" + id,
		            // pass in data as strings
		            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		        }).then(function(data) {
					return data;
		        })
		    },
		    this.GetStoreWeeklyProgress = function(id) {
		    	return $http({
		            method: 'GET',
		            url: currentAppPath + "admin/getStoreWeeklyProgress/" + id,
		            // pass in data as strings
		            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		        }).then(function(data) {
					return data;
		        })
		    },
		    this.GetStoreWebsites = function(id) {
		    	return $http({
		            method: 'GET',
		            url: currentAppPath + "admin/getStoreWebsites/" + id,
		            // pass in data as strings
		            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		        }).then(function(data) {
					return data;
		        })
		    },
		    this.getRelatedUsers = function(id) {
		    	return $http({
		            method: 'GET',
		            url: currentAppPath + "admin/getStoreUsers/" + id,
		            // pass in data as strings
		            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		        }).then(function(data) {
					return data;
		        })
		    },
		    this.getAllToDoList = function() {
		    	return $http({
		            method: 'GET',
		            url: currentAppPath + "admin/getStoreListWeeklyToDo",
		            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		        }).then(function(data) {
					return data;
		        })
		    	
		    },
		    this.SaveStore = function(data, id = false) {
		    	var url;
		    	
		    	if(id) {
		    		url = currentAppPath  + "admin/save/store/" + id
		    	} else {
		    		url = currentAppPath  + "admin/save/store"
		    	}
		    	var request = $http({
					method  : 'POST',
					url     : url,
					data 	: data,
					headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
				});
				return (request.then(handleSuccess));
		    }
		    
		    function handleSuccess( response ) {
            	return( response.data );
            }
		});
		
		
		

		
		
		
		
		


		SidDillonIntranet.service('AdminSettings', function($http) {
		    this.getSettings = function() {
		        return $http({
		            method: 'GET',
		            url: currentAppPath + "admin/getSettings",
		            // pass in data as strings
		            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
		        }).then(function(data) {
					return data;
		        })
		    },
		    this.SaveMainSettings = function(data) {
		    	var request = $http({
					method  : 'POST',
					url     : currentAppPath + "admin/save/settingsmain",
					data 	: data,
					headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
				});
				
				return (request.then(handleSuccess));
		    },
		    this.SaveToDoListSettings = function(data) {
		    	var request = $http({
					method  : 'POST',
					url     : currentAppPath + "admin/save/settingstodolist",
					data 	: data,
					headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
				});
				return (request.then(handleSuccess));
		    }
		    
		    function handleSuccess( response ) {
            	return( response.data );
            }
		});


		
		

		
	
				
		SidDillonIntranet.config(function($routeProvider) {
			$routeProvider
			.when('/to-do-list', 
            	{
					controller : "storeToDoListCtrl",
	                templateUrl : 'view/admin/singleStoreToDoList.php',
	                title: "Your To Do List",
	                resolve: {
	                	CurrentWeekText: function(CurrentWeek) {
		                	return CurrentWeek.getCurrentWeek().then(function(data) { 
								return data.data.CurrentWeek;
							});	
	                	},
	                	StoreToDoList: function(Users, StoresList) {
	                		return Users.getCurrentSessionStoreID().then(function(data) { 
								return StoresList.GetStoreWeeklyProgress(data.data.currentSessionStoreID).then(function(data) { 
									return data.data.storeWeeklyProgress;
								});
							});
	                	}
	                }
            	}
            ).when('/post/:type/add', 
            	{
					controller : "addSocialMediaPostCtrl",
	                templateUrl : 'view/admin/postSingle.php',
	                title: "Add Social Media Post",
	                resolve: {
	                	StoreToDoList: function(Users, StoresList) {
	                		return Users.getCurrentSessionStoreID().then(function(data) { 
								return StoresList.GetStoreWeeklyProgress(data.data.currentSessionStoreID).then(function(data) { 
									return data.data.storeWeeklyProgress;
								});
							});
	                	}
	                }
            	}
            ).when('/your-post/:id/:type/view', 
            	{
					controller : "storeViewSubmittedPostCtrl",
	                templateUrl : 'view/admin/postSingle.php',
	                title: "View Social Media Post",
	                resolve:  {
	                	SocialMediaPostSingle: function(SocialMediaList, $route) {
	                		return SocialMediaList.SingleWebsiteBlogPost($route.current.params.id).then(function(data) { 
								return data.data.singlePost;
							});	  
	                	}
	                }
            	}
            ).otherwise(
            	{
            		redirectTo: '/'
            	}
            );
		});
		

		
		

		

		
		
		SidDillonIntranet.controller('adminSingleStoreToDoListCtrl',['$scope', '$sce', 'Settings', 'CurrentWeekText', 'WeeklyProgress', 'SingleStore', 'Websites', 'RelatedUsers', function($scope, $sce, Settings, CurrentWeekText, WeeklyProgress, SingleStore, Websites, RelatedUsers) {
			
			
			//facebook posts
			var facebookPostValue = [];
			for(var i=0; i< Settings.facebookPosts; i++) {
				facebookPostValue.push(i);
			}
			$scope.facebookPosts = facebookPostValue;
			
			//twitter
			var twitterPostValue = [];
			for(var i=0; i < Settings.TwitterPosts; i++) {
				twitterPostValue .push(i);
			}
			$scope.twitterPosts = twitterPostValue;
			
			
			//blog posts
			var blogPostsValue = [];
			for(var i=0; i< Settings.blogPosts; i++) {
				blogPostsValue.push(i);
			}
			$scope.blogPosts = blogPostsValue;
			
			//current week
			$scope.weeklyText = CurrentWeekText;
			
			
			//facebook
			$scope.completedFacebook = WeeklyProgress.WeeklyFacebookPosts;
			$scope.totalFacebookPosts = WeeklyProgress.totalRequiredFacebook;
			
			//twitter
			$scope.completedTwitter = WeeklyProgress.WeeklyTwitterPosts
			$scope.totalTwitterPosts = WeeklyProgress.totalRequiredTwitter
			
			//blog posts
			$scope.completedBlogPosts = WeeklyProgress.WeeklyBlogPosts;
			$scope.totalBlogPosts = WeeklyProgress.totalRequiredBlogPosts; 
			
			$scope.currentProgress = WeeklyProgress.progressBarPercentage;
			
			//single store information
			$scope.storeName = SingleStore.name;
			$scope.facebookURL = SingleStore.facebook;
			$scope.twitterURL = SingleStore.twitter;
			$scope.youtubeURL = SingleStore.youtube;
			$scope.instagramURL = SingleStore.instagram;
			$scope.googlePlusURL = SingleStore.googlePlus;
			
			$scope.storeAddress = $sce.trustAsHtml(SingleStore.street + '<br />' + SingleStore.city + ", " + SingleStore.state + " " + SingleStore.zip);
				
			var phoneAreaCode = SingleStore.phone.substring(0, 3);
			var phone1 = SingleStore.phone.substring(3, 6);
			var phone2 = SingleStore.phone.substring(6, 10);
				
			var storePhoneFinal = "(" + phoneAreaCode + ") " + phone1 + " - " + phone2;
				
				
			$scope.storePhone = $sce.trustAsHtml(storePhoneFinal);
			
			$scope.storeWebsites = JSON.parse([Websites]);
			
			$scope.RelatedUsers = JSON.parse([RelatedUsers]);
			
		}]);
		

		
		
		SidDillonIntranet.controller('storeViewSubmittedPostCtrl', ['$scope', '$sce', '$routeParams', 'SocialMediaPostSingle', function($scope, $sce, $routeParams, SocialMediaPostSingle) {
			$scope.ShowSocialMediaStatus = true;
			
			$scope.PostSingleHeader = "View Post Submission";
			$scope.showViewOnlyPost = true;
			$scope.socialMediaType = $routeParams.type + " Post";
			$scope.postState = "View";
			$scope.PostContent = $sce.trustAsHtml(SocialMediaPostSingle.content);
			$scope.KeyWords = $sce.trustAsHtml(SocialMediaPostSingle.keywords);
			$scope.socialMediaListText = "Your To Do List";
			$scope.breakCrumbSocialMediaLink = "#/to-do-list";	
			$scope.socialMediaIcon = currentAppPath + "public/images/" + $routeParams.type + "Icon.png";
			
			$scope.PostedApprovedByName = SocialMediaPostSingle.isApprovedBy;
			$scope.PostEditedDetailsDate = SocialMediaPostSingle.isApprovedByDate;
			
			if(SocialMediaPostSingle.isApproved == 1) {
				$scope.showApprovalSection = true;
			} else {
				$scope.showApprovalSection = false;
			}
			
			
		}]);	
		
		SidDillonIntranet.controller('addSocialMediaPostCtrl', ['$scope', '$sce', '$http', '$routeParams', '$location', 'StoreToDoList', function($scope, $sce, $http, $routeParams, $location,  StoreToDoList) {
			$scope.ShowSocialMediaStatus = true;
			
			$scope.showStoreDetailsMediaType = true;
			$scope.PostSingleHeader = "Submit " + $routeParams.type + " Post";
			$scope.postState = "Add";
			$scope.socialMediaListText = "Your To Do List";
			$scope.breakCrumbSocialMediaLink = "#/to-do-list";
			$scope.storeEditView = true;
			$scope.socialMediaIcon = currentAppPath + "public/images/" + $routeParams.type + "Icon.png";
			
			//prevent excess required posts
			if($routeParams.type == "facebook") {
				if(StoreToDoList.WeeklyFacebookPosts.length == StoreToDoList.totalRequiredFacebook) {
					$scope.showLoading = true;
					$location.path("/to-do-list")
				}				
			}

			if($routeParams.type == "twitter") {
				if(StoreToDoList.WeeklyTwitterPosts.length == StoreToDoList.totalRequiredTwitter) {
					$scope.showLoading = true;
					$location.path("/to-do-list");
				}
			}
			
			if($routeParams.type == "blog") {
				if(StoreToDoList.WeeklyBlogPosts.length == StoreToDoList.totalRequiredBlogPosts) {
					$scope.showLoading = true;
					$location.path("/to-do-list");
				}
			}
			
			
			
			$scope.options = {
				filebrowserUploadUrl: currentAppPath + "admin/uploadImages",
				filebrowserImageUploadUrl: currentAppPath + "admin/uploadImages",
	 			filebrowserImageWindowWidth: '640',
	    		filebrowserImageWindowHeight: '480',
	    		baseUrl: '/view/'
			};
			
			$scope.submitPost = "Save Post";
			
			$scope.showAddPostForm = true;
			
			$scope.SinglePost = {};
			
			
			
			$scope.postFormSubmit = function() {
				$scope.loadingFinal = true;
				$scope.showSuccess = false;
					//not the most effienct way to send the data via ajax
					//this is temporary, just to make it work, clean this up later
				$http({
					method  : 'POST',
					url     : currentAppPath + "admin/save/socialmedia?type=" + $routeParams.type,
					data 	: "hiddenSocialMediaContent=" + Globals.VariableCheck(btoa($sce.trustAsHtml($scope.SinglePost.socialMediaContent))) + 
							  "&socialMediaKeyWords=" + Globals.VariableCheck($scope.SinglePost.socialMediaKeyWords),
					headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
				}).success(function(data) {		
					if(data.errorMessage) {$scope.errorMessage = data.errorMessage; $scope.showMessage = true;}
					if(data.MySqlError) {$scope.errorMessage = data.MySqlError; $scope.showMessage = true;}
						
					if(data.redirect) {
						Globals.redirect(data.redirect);
						$scope.showMessage = true;
						$scope.loadingFinal = true;
					}
				}).finally(function() {
					// "complete" code here
    				$scope.loadingFinal = false;
				});		
			}
			
			
			

		}]);
		
		
		SidDillonIntranet.controller('storeToDoListCtrl', ['$scope', '$sce', 'AdminSettings', 'CurrentWeekText', 'StoreToDoList', function($scope, $sce, AdminSettings, CurrentWeekText, StoreToDoList) {
			
			$scope.hideSocialMediaStatus = true;
			
			//facebook
			var facebookPostValue = [];
			for(var i=0; i< StoreToDoList.totalRequiredFacebook; i++) {
				facebookPostValue.push(i);
			}
			$scope.facebookPosts = facebookPostValue;
			$scope.totalFacebookPosts = StoreToDoList.totalRequiredFacebook;
				
			//twitter
			var twitterPostValue = [];
			for(var i=0; i< StoreToDoList.totalRequiredTwitter; i++) {
				twitterPostValue .push(i);
			}    
		    $scope.twitterPosts = twitterPostValue;
		    $scope.totalTwitterPosts = StoreToDoList.totalRequiredTwitter;
		    	
		   	//blog posts
		    var blogPostsValue = [];
		    for(var i=0; i< StoreToDoList.totalRequiredBlogPosts; i++) {
				blogPostsValue.push(i);
			}
			$scope.blogPosts = blogPostsValue;
			$scope.totalBlogPosts = StoreToDoList.totalRequiredBlogPosts;
			
			$scope.weeklyText = CurrentWeekText;
			$scope.currentProgress = StoreToDoList.progressBarPercentage;
			
			//facebook list
			$scope.completedFacebook = StoreToDoList.WeeklyFacebookPosts;
			
			//twitter list
			$scope.completedTwitter = StoreToDoList.WeeklyTwitterPosts;
			
		    //blog posts
		    $scope.completedBlogPosts = StoreToDoList.WeeklyBlogPosts; 
		    
		}]);
		
		
		
		//settings

		



		
		SidDillonIntranet.controller('settingsNewUserController', function($scope, $sce, $http, StoresList) {
			$scope.settingsUsersSelected = true;
			$scope.showSaveButton = true;
			$scope.subContent = 'view/admin/settings/userSingle.php'
			$scope.userSingleTitle = "Add New User";
			
			
			//this is needed if you are using template within a template
			$scope.UserSingle = {};
				
			//load all store list
			StoresList.getAllStores().then(function(data) { 
				$scope.storesList = JSON.parse([data.data.storesList]);
				$scope.UserSingle.userStore = $scope.storesList[0].id;
			});
				
			
			$scope.userFormSubmit = function(data) {
				$scope.loadingFinal = true;
					
				$http({
					method  : 'POST',
					url     : currentAppPath  + "admin/save/user",
					data 	: data,
					headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
				}).success(function(data) {
					if(data.errorMessage) {
						$scope.errorMessage = data.errorMessage;
						$scope.showMessage = true;
					}
						
					if(data.redirect) {
						Globals.redirect(data.redirect);
						$scope.showMessage = true;
					}
					
					if(data.MySqlError) {
						$scope.errorMessage = data.MySqlError;
						$scope.showMessage = true;
					}
						
				}).finally(function() {
					// "complete" code here
    				$scope.loadingFinal = false;
				});
					
					
			};
				
			$scope.changedValue = function(item) {
				if(item == 2 || item == 3) {
					$scope.StoreSelectDropDown = true
				} else {
				    $scope.StoreSelectDropDown = false
				}
			}
			
		});
		
		SidDillonIntranet.controller('settingsViewUserCtrl', ['$scope', '$sce', '$http', '$routeParams', '$timeout', 'StoresListLoad', 'Users', 'UserSingle', function($scope, $sce, $http, $routeParams, $timeout, StoresListLoad, Users, UserSingle) {
			$scope.settingsUsersSelected = true;
			$scope.showBreadCrumbs = true;
			$scope.subContent = 'view/admin/settings/userSingle.php';
			
			
			$scope.UserSingle = {};
			
			//load all store list
			$scope.storesList = JSON.parse([StoresListLoad])
			
			//load single user data
			if(UserSingle.isActive == 0) {
				$scope.userSingleTitle = "Approve/Decline User";	
			} else {
				$scope.userSingleTitle = "Edit User";
				$scope.showSaveButton = true;
			}
			
			$scope.UserSingle.userFirstName = UserSingle.firstName;
			$scope.UserSingle.userLastName = UserSingle.lastName;
			$scope.UserSingle.userEmail = UserSingle.email;
			
			$scope.userFullName = UserSingle.firstName + " " + UserSingle.lastName;
			
			if(UserSingle.UserType == 1) {
				$scope.AdminSelect = true;
			} else if(UserSingle.UserType == 2) {
				$scope.StoreSelectDropDown = true
				$scope.GeneralManager = true
			} else if(UserSingle.UserType == 3) {
				$scope.SalesManager = true;
				$scope.StoreSelectDropDown = true
			}
			
			$scope.UserSingle.userType = UserSingle.UserType;
			$scope.currentStore = UserSingle.Store;
			
			$scope.UserSingle.userStore = UserSingle.Store;
			$scope.UserSingle.userNameLogin = UserSingle.userName;
				
			if(UserSingle.IsReceivingNotification == 1) {
				$scope.receivingNotificaitons = true;	
			}
			
			
			
			$scope.changedValue = function(item) {
				if(item == 2 || item == 3) {
					$scope.StoreSelectDropDown = true
				} else {
					$scope.StoreSelectDropDown = false
				}
			}   
			
			
			$scope.userFormSubmit = function(data) {
				$scope.loadingFinal = true;
				$scope.showSuccess = false;
				
				Users.SaveUser(data, $routeParams.id).then(function(response) { 
					if(response.errorMessage) {
						$scope.errorMessage = response.errorMessage;
						$scope.showMessage = true;
					}
					
					if(response.redirect) {
						Globals.redirect(response.redirect);
						$scope.showMessage = true;
					}
					
					if(response.MySqlError) {
						$scope.errorMessage = response.MySqlError;
						$scope.showMessage = true;
					}
					
					if(response.userUpdated) {
						$scope.errorMessage = response.userUpdated;
						$scope.showMessage = true;
						$scope.showSuccess = true;		
					}
					$scope.loadingFinal = false;
				});
				

			}
			
			$scope.approveUser = function() {
				$scope.loadingFinal = true;
				$http.get(currentAppPath + "admin/save/approveuser/" + $routeParams.id).success(function(response) {
					$scope.userSingleTitle = "Edit User";
					$scope.showSaveButton = true;
					$scope.showMessage = true;
					$scope.loadingFinal = false;
					$scope.showSuccess = true;
					$scope.errorMessage = "User is Approved. Email will be sent to this particular user to setup their login credentials"
					$scope.$apply();
					//Globals.redirect("#/settings/users/" + $routeParams.id + "/edit")
				});					
			}
			
			$scope.declineUser = function() {
				$http.get(currentAppPath + "admin/delete/user/" + $routeParams.id).success(function(response) {
					Globals.redirect("#/settings/users")
				});
					//alert("declineUser");
			}
			
		}]);
		

	}
	
	return {
		Initialize: Initialize 
	}
}()
