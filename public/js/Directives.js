SidDillonIntranet.directive("serializer", function(){
	return {
		restrict: "A",
		scope: {
			onSubmit: "&serializer"
		},
		link: function(scope, element){
		    // assuming for brevity that directive is defined on <form>
		    var form = element;
		
		    form.submit(function(event){
		        event.preventDefault();
		        var serializedData = form.serialize();
		
		    	scope.onSubmit({data: serializedData});
			});
		}
	};
});

SidDillonIntranet.directive('phoneInput', function($filter, $browser) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});


SidDillonIntranet.directive('angularMask', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        isModelValueEqualViewValues: '='
      },
      link: function ($scope, el, attrs, model) {
        $scope.$watch(function(){return attrs.angularMask;}, function(value) {
          if (model.$viewValue != null){
            model.$viewValue = mask(String(model.$viewValue).replace(/\D/g, ''));
            el.val(model.$viewValue);
          }
        });

        model.$formatters.push(function (value) {
          return value === null ? '' : mask(String(value).replace(/\D/g, ''));
        });

        model.$parsers.push(function (value) {
          model.$viewValue = mask(value);
          var modelValue = $scope.isModelValueEqualViewValues ? model.$viewValue : String(value).replace(/\D/g, '');
          el.val(model.$viewValue);
          return modelValue;
        });

        function mask(val) {
          var format = attrs.angularMask,
          arrFormat = format.split('|');

          if (arrFormat.length > 1) {
            arrFormat.sort(function (a, b) {
              return a.length - b.length;
            });
          }

          if (val === null || val == '') {
            return '';
          }
          var value = String(val).replace(/\D/g, '');
          if (arrFormat.length > 1) {
            for (var a in arrFormat) {
              if (value.replace(/\D/g, '').length <= arrFormat[a].replace(/\D/g, '').length) {
                format = arrFormat[a];
                break;
              }
            }
          }
          var newValue = '';
          for (var nmI = 0, mI = 0; mI < format.length;) {
            if (!value[nmI]) {
              break;
            }
            if (format[mI].match(/\D/)) {
              newValue += format[mI];
            } else {
              newValue += value[nmI];
              nmI++;
            }
            mI++;
          }
          return newValue;
        }
      }
    };
 });
 
SidDillonIntranet.directive('routeLoadingIndicator', function ($rootScope) {
	return {
    	restrict:'E',
    	template:"<div class='loadingViewIndicator' ng-if='isRouteLoading'><img src='public/images/ajax-loader.gif' /></div>",
    	link:function($scope, elem, attrs){
	      $scope.isRouteLoading = false;
	
	      $rootScope.$on('$routeChangeStart', function(){
	        $scope.isRouteLoading = true;
	      });
	
	      $rootScope.$on('$routeChangeSuccess', function(){
	        $scope.isRouteLoading = false;
	      });
	    }
  	};
});


SidDillonIntranet.directive('ngConfirmClick', [
    function(){
    	return {
        	link: function (scope, element, attr) {
            	var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                	if ( window.confirm(msg) ) {
                    	scope.$eval(clickAction)
                	}
        	});
        }
	};
}])


