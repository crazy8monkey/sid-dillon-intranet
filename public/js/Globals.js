var Globals = function() {
	
	
	
	var currentAppPath = 'http://localhost/intranet/';
	//var currentAppPath = "http://dev.siddillon.com/adam/intranet/";
	//var currentAppPath = "http://internal.siddillon.com/";
		
	
	function closeMessaging() {
		$('.message').fadeOut('fast');
	}
	function hideFormOverlay() {
		$(".form-overlay").fadeOut("fast");
	}
	
	function ToggleMobileMenu() {
		$(document).click(function (e) {
            if ($(e.target).closest('#hrefMobileMenu').length === 0) {
            	$("header .mobileMenu .mobileMenuDropdown").hide();
        	}
        });
			
		$("header .mobileMenu .mobileMenuDropdown").toggle();
	}
	
	return {
		currentAppPath: function() {
			return currentAppPath;
		},
		redirect: function(string) {
			return window.location.assign(string)
		},
		LoadingSpinner: function() {
			return Globals.currentAppPath() + 'public/images/ajax-loader.gif';
		},
		VariableCheck: function(element) {
			return ((element !== undefined) ? element : '')
		},
		ToggleMobileMenu: ToggleMobileMenu 
	}	
	
}();