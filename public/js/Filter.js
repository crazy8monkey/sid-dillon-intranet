SidDillonIntranet.filter("GetUserTypeText", function() {
	return function(id) {
		switch(id) {
			case '1':
				return "Admin";
				break;
			case '2':
				return "Store Admin";
				break;					        
			case '3':
				return "Normal User";
				break;					        		    
		}				
	}
});
	
	
SidDillonIntranet.filter("adminSocialMediaViewStates", function() {
	return function(id) {
		switch(id) {
			case '0':
				return "edit";
				break;
			case '1':
				return "view";
				break;					        
		}				
	}
});	

SidDillonIntranet.filter("SocialMediaFilter", function() {
	return function(id) {
		switch(id) {
			case '1':
				return "facebook";
				break;
			case '2':
				return "twitter";
				break;
			case '3':
				return "blog";
				break;					        
		}				
	}
});

SidDillonIntranet.filter("YesNo", function() {
	return function(id) {
		switch(id) {
			case '0':
				return "No";
				break;
			case '1':
				return "Yes";
				break;					        
					        		    
		}				
	}
});


SidDillonIntranet.filter("JobTitles", function() {
	return function(id) {
		switch(id) {
			case '1':
				return "General Manager";
				break;
			case '2':
				return "Sales Manager";
				break;
			case '3':
				return "Salesman";
				break;
			case '4':
				return "Internet Salesman";
				break;								        
			case '5':
				return "Service Manager";
				break;
			case '6':
				return "Service Advisor";
				break;
			case '7':
				return "Service Tech";
				break;
			case '8':
				return "Parts Manager";
				break;					        		    
			case '9':
				return "Parts Counter";
				break;
			case '10':
				return "Office Manager";
				break;
			case '11':
				return "Office Personal";
				break;
			case '12':
				return "Receptionist";
				break;
			case '13':
				return "Marketing/Advertising";
				break;
			case '14':
				return "Finance Manager";
				break;
			case '15':
				return "Finance";
				break;
			case '16':
				return "Bodyshop Manager";
				break;
			case '17':
				return "Bodyshop Consultant";
				break;
			case '18':
				return "Bodyshop Tech";
				break;
			case '19':
				return "Other";
				break;
		}		
	}
});


SidDillonIntranet.filter("ReferralStatus", function() {
	return function(id) {
		switch(id) {
			case '0':
				return "ActiveStatus";
				break;
			case '1':
				return "SoldStatus";
				break;
			case '2':
				return "PaidStatus";
				break;
			case '3':
				return "InactiveStatus";
				break;
			case '4':
				return "WarningStatus";
				break;
		}		
	}
});

SidDillonIntranet.filter("ReferralStatusTextColor", function() {
	return function(id) {
		switch(id) {
			case '0':
				return "ActiveStatusText";
				break;
			case '1':
				return "SoldStatusText";
				break;
			case '2':
				return "PaidStatusText";
				break;
			case '3':
				return "InactiveStatusText";
				break;
		}		
	}
});


SidDillonIntranet.filter("ReferralStatusText", function() {
	return function(id) {
		switch(id) {
			case '0':
				return "Active";
				break;
			case '1':
				return "Sold";
				break;
			case '2':
				return "Paid";
				break;
			case '3':
				return "Inactive";
				break;
		}		
	}
});



SidDillonIntranet.filter('capitalize', function() {
	return function(input) {
		return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
	}
});

SidDillonIntranet.filter('ConvertTimeAMPM', function() {
	return function(str){
		    	
		var time = str.split(':');
				
		var hours = time[0] > 12 ? time[0] - 12 : time[0];
		var minutes = time[1];
		var am_pm = time[0] >= 12 ? "PM" : "AM";
				
		return hours + ":" + minutes + " " + am_pm;
	};
});

SidDillonIntranet.filter('ProgressBarColor', function() {
	return function(Percentage){
		var colorClass;
		if(20 >= Percentage) {
		    colorClass = "progressRed";
		} else if(Percentage >= 21 && Percentage <= 40) {
		    colorClass = "progressOrange";
		} else if(Percentage >= 41 && Percentage <= 60) {
		    colorClass = "progressYellow";
		} else if(Percentage >= 61 && Percentage <= 80) {
		    colorClass = "progressLightGreen";
		} else if(Percentage >= 81 && Percentage <= 100) {
			colorClass = "progressGreen";
		}
		return colorClass;
	};
});

SidDillonIntranet.filter('tel', function () {
    return function (tel) {
        console.log(tel);
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + '-' + number.slice(3,7);
            }
            else{
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else{
            return "(" + city;
        }

    };
});

SidDillonIntranet.filter('pagination', function() {
	return function(input, start) {
		start = +start;
		return input.slice(start);
	};
});

SidDillonIntranet.filter('SettingsLink', function() {
	return function(id) {
		switch(id) {
			case '1':
				return true;
				break;
			case '2':
				return true;
				break;
			case '3':
				return false;
				break;
		}
	};
});

SidDillonIntranet.filter('MarketingLink', function() {
	return function(id) {
		switch(id) {
			case '1':
				return true;
				break;
			case '2':
				return false;
				break;
			case '3':
				return false;
				break;
		}
	};
});

SidDillonIntranet.filter('ReferralLink', function() {
	return function(id) {
		switch(id) {
			case '1':
				return true;
				break;
			case '2':
				return true;
				break;
			case '3':
				return false;
				break;
		}
	};
});

SidDillonIntranet.filter('SettingsTabTypes', function() {
	return function(id) {
		switch(id) {
			case '1':
				return true;
				break;
			case '2':
				return false;
				break;
		}
	};
});

SidDillonIntranet.filter('Percentage', function () {
        return function (value) {
            return Math.ceil(value * 100);
        };
    })


