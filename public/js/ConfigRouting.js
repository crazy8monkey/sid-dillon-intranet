var pageTitle = " | Sid Dillon Intranet"
var CurrentAppPath = Globals.currentAppPath();

var SidDillonIntranet = angular.module('SidDillonIntranet', ['ngRoute', 'ngAnimate', 'ckeditor', 'ui.bootstrap', 'ui.mask']);

var CurrentSessionUserID;


//run
SidDillonIntranet.run(['$location', '$rootScope', '$timeout', 'SessionVariables', 'System', function($location, $rootScope, $timeout, SessionVariables, System) {
	//System.CurrentSessionVariables().then(function(response) { 
	//	CurrentSessionUserID = response.data.CurrentSessionUser;
	//	SessionVariables.setUserID(response.data.CurrentSessionUser);
	//	SessionVariables.setCurrentSessionUserType(response.data.CurrentSessionUserType);
	//});	
	
	$rootScope.$on('$routeChangeStart', function(event, next, current) {
		System.CurrentSessionVariables().then(function(response) { 
			CurrentSessionUserID = response.data.CurrentSessionUser;
			SessionVariables.setUserID(response.data.CurrentSessionUser);
			SessionVariables.setCurrentSessionUserType(response.data.CurrentSessionUserType);
		});	
	});
	
	$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {		
		$rootScope.title = current.$$route.title  + pageTitle;
		$rootScope.controllerCssFile = current.$$route.controllerCssFile;
		$rootScope.headerContent = current.$$route.headerContent;
		$rootScope.settingsMain = "#/admin/settings/main";
		$rootScope.referralUsers = "#/admin/settings/referral-users";
		$rootScope.settingsToDoList = "#/admin/settings/to-do-list"; 
		$rootScope.settingsStores = "#/admin/settings/stores"; 
		$rootScope.settingsUsers = "#/admin/settings/users";
		$rootScope.settingsReferralTier = "#/admin/settings/referral-tiers";
		$rootScope.settingsReferralMembers = "#/admin/settings/referral-members";
		$rootScope.settingsLogs = "#/admin/settings/logs";
		$rootScope.settingsSystem = "#/admin/settings/system";
		$rootScope.settingsCron = "#/admin/settings/cronjob";
		$rootScope.referralReporting = "#/admin/reporting/referrals";
		$rootScope.subContent = current.$$route.subContent;
		$rootScope.IntranetLoginContent = current.$$route.IntranetLoginContent;
		
		System.CurrentSessionVariables().then(function(response) { 
			SessionVariables.setUserID(response.data.CurrentSessionUser);
			SessionVariables.setCurrentSessionUserType(response.data.CurrentSessionUserType);
		});
		
	}); 
}]);




//routing
SidDillonIntranet.config(function($routeProvider, $provide) {
	$provide.constant('$MD_THEME_CSS', '/**/');
	$routeProvider
		.when('/', 
			{
				controller : "loginController",
	            templateUrl : 'view/login/login.html',
	            title: "Login",
	            controllerCssFile: "LoginController"
	        }
		).when('/forgot-password', 
            {
				controller : "forgotPasswordCtrl",
	            templateUrl : 'view/login/forgotPassword.html',
	            title: "Forgot Password",
	            controllerCssFile: "LoginController"    
        	}
        ).when('/request-access', 
            {
				controller : "requestAccessCtrl",
	            templateUrl : 'view/login/requestAccess.html',
	            title: "Request Access",
	            controllerCssFile: "LoginController",
	            resolve : {
	            	StoresList: function(StoresList) {
	                	return StoresList.getAllStores().then(function(data) { 	
							return data.data.stores
						});
	                }
	            }    
        	}
        ).when('/passwordReset', 
            {
				controller : "passwordResetCtrl",
	            templateUrl : 'view/login/passwordReset.html',
	            title: "Reset Your Password",
	            controllerCssFile: "LoginController",
	            resolve: {
	            	UserSingle: function(Users) {
	            		return Users.singleUser(getParameterByName('u')).then(function(data) { 	
							return data.data.userSingle[0]
						});
	            	}
	            }
        	}
        ).when('/new-credentials', 
            {
				controller : "createCredentialsCtrl",
	            templateUrl : 'view/login/createCredentials.html',
	            title: "Create your Credentials",
	            controllerCssFile: "LoginController",
	            resolve: {
	            	UserSingle: function(Users) {
	            		return Users.singleUser(getParameterByName('u')).then(function(data) { 	
							return data.data.userSingle[0]
						});
	            	}
	            }       
        	}
        ).when('/admin/', 
				{
					controller : "DashboardController",
	                templateUrl : 'view/admin/dashboard.html',
	                title: "Dashboard",
	                controllerCssFile: "AdminController",
	                headerContent: "shared/header.html",
	                IntranetLoginContent: true,
	                NeedLoggedInStatus: true
            	}
		).when('/admin/:id/store-weekly-to-do-list', 
        	{
				controller : "singleStoreToDoListCtrl",
	            templateUrl : 'view/admin/socialmedia/AdminSingleToDoList.html',
	            title: "Store Weekly Progress",
	            controllerCssFile: "AdminController",
	            headerContent: "shared/header.html",
	            IntranetLoginContent: true,
	            NeedLoggedInStatus: true,
	            resolve: {
	            	SubmittedFacebookPosts: function(StoresList, $route) {
						return StoresList.SubmittedFacebookPosts($route.current.params.id).then(function(data) { 
							return data.data.StoreFacebookList;
						});
					},
					SubmittedTwitterPosts: function(StoresList, $route) {
						return StoresList.SubmittedTwitterPosts($route.current.params.id).then(function(data) { 
							return data.data.StoreTwitterList;
						});
					},
					SubmittedBlogPosts: function(StoresList, $route) {
						return StoresList.SubmittedBlogPosts($route.current.params.id).then(function(data) { 
							return data.data.StoreBlogList;
						});
					},
	                StoreInfo: function(StoresList, $route) {
			        	return StoresList.storeSingle($route.current.params.id).then(function(data) { 
							return data.data.StoreSingle[0];
						});
					},
					SingleToDoList: function(StoresList, $route) {
			        	return StoresList.SingleToDoList($route.current.params.id).then(function(data) { 
							return data.data.StoreProgress;
						});
					},
					CurrentWeekText: function(System) {
			            return System.getCurrentWeek().then(function(data) { 
							return data.data.CurrentWeek;
						});	                		
			        },
			        Settings: function(System) {
			            return System.GetCurrentSettings().then(function(data) { 
							return data.data.settings[0];
						});	                		
			        },
			        Websites: function(Websites, $route) {
			        	return Websites.getWebsitesByStore($route.current.params.id).then(function(data) { 
							return data.data.StoreWebsites;
						});
			        },
			        StoreUsers: function(Users, $route) {
			        	return Users.getUsersByStore($route.current.params.id).then(function(data) { 
							return data.data.storeUsers;
						});
			        }

	        	}
            }
        ).when('/admin/profile', 
        	{
				controller : "UserProfileController",
	            templateUrl : 'view/admin/settings/userProfile.html',
	            title: "Your Profile",
	            controllerCssFile: "AdminController",
	            headerContent: "shared/header.html",
	            IntranetLoginContent: true,
	            NeedLoggedInStatus: true,
	            resolve: {
	            	UserSingle: function(Users, System) {
	            		return System.CurrentSessionVariables().then(function(response) { 
							return Users.singleUser(response.data.CurrentSessionUser).then(function(data) { 
								return data.data.userSingle[0];
							});
						});	
	            		
	            		
						
	               },
	               StoreList: function(StoresList) {
	               	    return StoresList.getAllStores().then(function(data) { 
							return data.data.stores;
						});
	               },
	               Permissions: function(Permissions, System) {
	               		return System.CurrentSessionVariables().then(function(data) { 
							return Permissions.getUserPermissions(data.data.CurrentSessionUser).then(function(data) { 
								return data.data.UserPermissions;
							});
						});
					}
	            }
            }
        ).when('/admin/marketing', 
            	{
					controller : "PostListCtrl",
	                templateUrl : 'view/admin/socialmedia/socialMediaList.html',
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            title: "Social Media Posts",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	NewPosts: function(SocialMediaList) {
	                		return SocialMediaList.NewPosts().then(function(data) { 
								return data.data.NewPosts;
							});
	                	},
	                	ApprovedPosts: function(SocialMediaList) {
	                		return SocialMediaList.ApprovedPosts().then(function(data) { 
								return data.data.ApprovedPosts;
							});
	                	},
	                	DeclinedPosts: function(SocialMediaList) {
	                		return SocialMediaList.DeclinedPosts().then(function(data) { 
								return data.data.DeclinedPosts;
							});
	                	},
	                	PostedPosts: function(SocialMediaList) {
	                		return SocialMediaList.PostedPosts().then(function(data) { 
								return data.data.PostedPosts;
							});
	                	}
	                }
            	}
		).when('/admin/marketing/:type-posts/declined', 
            	{
					controller : "PostListCtrl",
	                templateUrl : 'view/admin/socialMediaList.html',
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            title: "Declined Social Media Posts",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	SocialMediaList: function(SocialMediaList, $route) {
	                		return SocialMediaList.declinedSocialMediaList($route.current.params.type).then(function(data) { 
								return data.data.SocialMediaListDeclined;
							});	                		
	                	},
	                	SelectedTab: function() {
	                		return "declined";
	                	}
	                }
            	}
		).when('/admin/marketing/postSingle/:id', 
            	{
					controller : "AdminSinglePostCtrl",
	                templateUrl : 'view/admin/socialmedia/adminPostSingle.html',
	                title: "View Social Media Post",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	SocialMediaPostSingle: function(SocialMediaList, $route) {	                		
	                		return SocialMediaList.SinglePost($route.current.params.id).then(function(data) { 
								return data.data.PostSingle;
							});
	                	}
	                }
            	}
		).when('/admin/settings/main', 
            	{
					controller : "settingsMainCtrl",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "Settings" + pageTitle,
	                controllerCssFile: "AdminController",
	                subContent: 'view/admin/settings/main.html',
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
	                NeedLoggedInStatus: true,
	                resolve : {
	                	Settings: function(System) {
	                		return System.GetCurrentSettings().then(function(data) { 
								return data.data.settings[0];
							});
	                	}
	                }
            	}
		).when('/admin/settings/referral-users', 
            	{
					controller : "SettingsReferralUsersController",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "Referral Users" + pageTitle,
	                controllerCssFile: "AdminController",
	                headerContent: "shared/header.html",
	                subContent: 'view/admin/settings/referralUsersList.html',
		            IntranetLoginContent: true,
	                NeedLoggedInStatus: true,
	                resolve : {
	                	ReferralUsersList: function(System, Users, Permissions, SessionVariables) {
	                		return System.CurrentSessionVariables().then(function(response) { 
	                			return Permissions.getUserReferralAdminPermissions(response.data.CurrentSessionUser).then(function(response) {
									var StoreIDS = [];
									angular.forEach(response.data.Marketing, function(value, key) {
										StoreIDS.push(value.storeID)	  		
									});
									return Users.getReferralUsersByStore(StoreIDS).then(function(data) {
										return data.data.StoreReferralUsers
									});
									
								});
	                		});
	                		
	                	}
	                }
            	}
		).when('/admin/settings/to-do-list', 
            	{
					controller : "settingsToDoListCtrl",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "To Do List Settings",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
	                NeedLoggedInStatus: true,
	                resolve : {
	                	SettingsToDoList: function(System) {
	                		return System.GetCurrentSettings().then(function(data) { 	
								return data.data.settings[0];
							});
	                	}
	                }
            	}
		).when('/admin/settings/stores', 
        	{
					controller : "settingsStoreListCtrl",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "Store List Settings",
	                subContent: 'view/admin/settings/storesList.html',
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
	                NeedLoggedInStatus: true,
	                resolve: {
	                	Stores: function(StoresList) {
	                		return StoresList.getAllStores().then(function(data) { 
								return data.data.stores;
							});
	                	}
	                }
	                
            }
		).when('/admin/settings/stores/new', 
            	{
					controller : "settingsSingleStoreCtrl",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                subContent: 'view/admin/settings/storeSingle.html',
	                title: "Add New Store",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
	                NeedLoggedInStatus: true,
	                resolve: {
	                	StoreInfo: function(StoresList, $route) {
				        	return '';
						},
						StoreWebsites: function(Websites, $route) {
							return '';
						},
	                	State: function() {
		                	return "New"
		                }
	                }
            	}
		).when('/admin/settings/stores/:id/edit', 
            {
				controller : "settingsSingleStoreCtrl",
	            templateUrl : 'view/admin/settings/settingsTemplate.html',
	            subContent: 'view/admin/settings/storeSingle.html',
	            title: "Edit Store",
	            controllerCssFile: "AdminController",
		        headerContent: "shared/header.html",
		        IntranetLoginContent: true,
	            NeedLoggedInStatus: true,
	            resolve: {
	            	StoreInfo: function(StoresList, $route) {
			        	return StoresList.storeSingle($route.current.params.id).then(function(data) { 
							return data.data.StoreSingle[0];
						});
					},
					StoreWebsites: function(Websites, $route) {
						return Websites.getWebsitesByStore($route.current.params.id).then(function(data) { 
							return data.data.StoreWebsites;
						});
					},
	                State: function() {
	                	return "Edit"
	                },
	                StoreAdminUsers: function(Users, $route) {
	                	return Users.StoreAdminUsers($route.current.params.id).then(function(data) { 
							return data.data.StoreAdmins;
						});
	                	
	                }
	            }
        	}
        ).when('/admin/settings/users', 
            	{
					controller : "settingsUsersListCtrl",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "Users List Settings",
	                subContent: 'view/admin/settings/usersList.html',
	                controllerCssFile: "AdminController",
			        headerContent: "shared/header.html",
			        IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve: {
	                	UsersList: function(Users) {
	                		return Users.getAllUsers().then(function(data) { 
								return data.data.users;
							});
	                	}
	                }
            	}
		).when('/admin/settings/referral-tiers', 
            	{
					controller : "settingsReferralTierController",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "Referral Tier Settings",
	                subContent: 'view/admin/settings/referrals/referralTierList.html',
	                controllerCssFile: "AdminController",
			        headerContent: "shared/header.html",
			        IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve: {
	                	Tiers: function(Referrals) {
	                		return Referrals.getTiers().then(function(data) { 
								return data.data.tiers;
							});
	                	}
	                }
            	}
		).when('/admin/settings/users/new', 
            {
				controller : "AddUserController",
	            templateUrl : 'view/admin/settings/settingsTemplate.html',
	            title: "New User",
	            subContent: 'view/admin/settings/AddUser.html',
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
	            resolve: {
	               	StoreList: function(StoresList) {
	               	    return StoresList.getAllStores().then(function(data) { 
							return data.data.stores;
						});
	               	},
	            }
        	}
		).when('/admin/settings/users/:id/edit', 
            	{
					controller : "EditProfileController",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "Edit User",
	                subContent: 'view/admin/settings/userSingle.html',
		            controllerCssFile: "AdminController",
				    headerContent: "shared/header.html",
				    IntranetLoginContent: true,
			        NeedLoggedInStatus: true,
	                resolve: {
	                	UserSingle: function(Users, $route) {
		            		return Users.singleUser($route.current.params.id).then(function(data) { 
								return data.data.userSingle[0];
							});
		               	},
	                	StoreList: function(StoresList) {
		               	    return StoresList.getAllStores().then(function(data) { 
								return data.data.stores;
							});
		               	},
		               	Permissions: function(Permissions, $route) {
		               		return Permissions.getUserPermissions($route.current.params.id).then(function(data) { 
								return data.data.UserPermissions;
							});
		               	}
		            }
            	}
		).when('/admin/settings/users/:id/requested', 
            	{
					controller : "ApproveDeclineUserController",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "New Requested User",
	                subContent: 'view/admin/settings/requestedUser.html',
		            controllerCssFile: "AdminController",
				    headerContent: "shared/header.html",
				    IntranetLoginContent: true,
			        NeedLoggedInStatus: true,
	                resolve: {
	                	UserSingle: function(Users, $route) {
		            		return Users.singleUser($route.current.params.id).then(function(data) { 
								return data.data.userSingle[0];
							});
		               	},
	                	StoreList: function(StoresList) {
		               	    return StoresList.getAllStores().then(function(data) { 
								return data.data.stores;
							});
		               	},
		               	Permissions: function(Permissions, $route) {
		               		return Permissions.getUserPermissions($route.current.params.id).then(function(data) { 
								return data.data.UserPermissions;
							});
		               	}
		            }
            	}
		).when('/admin/settings/logs', 
            	{
					controller : "settingsLogsCtrl",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "Logs" + pageTitle,
	                subContent: 'view/admin/settings/logs.html',
	                controllerCssFile: "AdminController",
				    headerContent: "shared/header.html",
				    IntranetLoginContent: true,
			        NeedLoggedInStatus: true,
	                resolve: {
	                	errorLogsPage: function(System) {
	                		return System.SystemLogs().then(function(data) { 
								return data.data.errorLogs;
							});
	                	}
	                }
            	}
		).when('/admin/settings/system', 
            	{
					controller : "SystemController",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                title: "System" + pageTitle,
	                subContent: 'view/admin/settings/system.html',
	                controllerCssFile: "AdminController",
				    headerContent: "shared/header.html",
				    IntranetLoginContent: true,
			        NeedLoggedInStatus: true,
	                resolve : {
	                	CurrentSession: function(System) {
	                		return System.CurrentSessionInformation().then(function(data) { 
								return data.data.currentSessionInfo;
							});
	                	},
	                	PHPInfo: function(System) {
	                		return System.PHPInfo().then(function(data) { 
								return data.data.systemInfo;
							});
	                	}
	                }
            	}
		).when('/admin/settings/cronjob', 
            	{
					controller : "cronJobListCtrl",
	                templateUrl : 'view/admin/settings/settingsTemplate.html',
	                subContent: 'view/admin/settings/cronjob.html',
	                title: "Cron Job List" + pageTitle,
	                controllerCssFile: "AdminController",
				    headerContent: "shared/header.html",
				    IntranetLoginContent: true,
			        NeedLoggedInStatus: true,
	                resolve : {
	                	AppPath: function(System) {
	                		return System.CurrentAppPath()
	                	}
	                }
            	}
		).when('/admin/post/:type/add/:storeID', 
            	{
					controller : "AddSocialMediaPostController",
	                templateUrl : 'view/admin/socialmedia/storeAddPost.html',
	                title: "Add Social Media Post",
	                controllerCssFile: "AdminController",
				    headerContent: "shared/header.html",
				    IntranetLoginContent: true,
			        NeedLoggedInStatus: true,
	                resolve: {
	                	StoreToDoList: function(System, StoresList) {
	                		return System.CurrentSessionVariables().then(function(data) { 
								return StoresList.SingleToDoList(data.data.CurrentSessionUserStore).then(function(data) { 
									return data.data.StoreSingleToDo;
								})
							});	                		
	                	},
	                	StoreID: function($route) {
	                		return $route.current.params.storeID;
	                	}
	                }
            	}
		).when('/admin/:id/your-to-do-list', 
        	{
				controller : "UserStoreToDoListController",
	            templateUrl : 'view/admin/socialmedia/singleStoreToDoList.html',
	            title: "Store Weekly Progress",
	            controllerCssFile: "AdminController",
	            headerContent: "shared/header.html",
	            IntranetLoginContent: true,
	            NeedLoggedInStatus: true,
	            resolve: {
					SubmittedFacebookPosts: function(StoresList, $route) {
						return StoresList.SubmittedFacebookPosts($route.current.params.id).then(function(data) { 
							return data.data.StoreFacebookList;
						});
					},
					SubmittedTwitterPosts: function(StoresList, $route) {
						return StoresList.SubmittedTwitterPosts($route.current.params.id).then(function(data) { 
							return data.data.StoreTwitterList;
						});
					},
					SubmittedBlogPosts: function(StoresList, $route) {
						return StoresList.SubmittedBlogPosts($route.current.params.id).then(function(data) { 
							return data.data.StoreBlogList;
						});
					},
					SingleToDoList: function(StoresList, $route) {
			        	return StoresList.SingleToDoList($route.current.params.id).then(function(data) { 
							return data.data.StoreProgress;
						});
					},
					Settings: function(System) {
			            return System.GetCurrentSettings().then(function(data) { 
							return data.data.settings[0];
						});	                		
			       },
			       CurrentWeekText: function(System) {
			            return System.getCurrentWeek().then(function(data) { 
							return data.data.CurrentWeek;
						});	                		
					}
	        	}
            }
        ).when('/admin/your-post/:id/', 
            	{
					controller : "SocialMediaPostController",
	                templateUrl : 'view/admin/socialmedia/storePostSingle.html',
	                title: "View Social Media Post",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	PostSingle: function(SocialMediaList, $route) {	                		
	                		return SocialMediaList.SinglePost($route.current.params.id).then(function(data) { 
								return data.data.PostSingle;
							});
	                	}
	                }
            	}
		).when('/admin/:storeID/referral', 
            	{
					controller : "AdminReferralStoreInfoController",
	                templateUrl : 'view/admin/AdminReferralSingle.html',
	                title: "Store Referral Info",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	ReferralsList: function(Referrals, $route) {
	                		return Referrals.referralsByStore($route.current.params.storeID).then(function(data) { 
								return data.data.storeReferrals;
							});
	                	}
	                }
            	}
		).when('/admin/:storeID/referrals/:month', 
            	{
					controller : "AdminReferralMonthInfoController",
	                templateUrl : 'view/admin/AdminReferralSingleMonth.html',
	                title: "Store Monthly Referrals",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	ReferralsByMonth: function(Referrals, $route) {
	                		return Referrals.referralsByStoreMonth($route.current.params.month, $route.current.params.storeID).then(function(data) { 
								return data.data.storeMonthReferrals;
							});
	                	}
	                }
            	}
		).when('/admin/:storeID/referrals/:month/:referralID/view/admin', 
            	{
					controller : "AdminReferralSingleController",
	                templateUrl : 'view/admin/referrals/AdminReferralSingleView.html',
	                title: "View Referral",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	ReferralSingle: function(Referrals, $route) {
	                		return Referrals.Single($route.current.params.referralID).then(function(data) { 
								return data.data.ReferralSingle[0];
							});
	                	},
	                	SalesmanByStore: function(Users, $route) {
	                		return Users.getUsersByStore($route.current.params.storeID).then(function(data) { 
								return data.data.storeUsers;
							});
	                	},
	                	PaidReferralsByMember: function(Referrals, $route) {
	                		return Referrals.Single($route.current.params.referralID).then(function(data) { 
								return data.data.ReferralSingle[0];
							});
	                	}
	                }
            	}
		).when('/admin/:storeID/your-referrals', 
            	{
					controller : "NonAdminStoreReferralsController",
	                templateUrl : 'view/admin/ReferralSingle.html',
	                title: "Your Referrals",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	ReferralsList: function(Referrals, $route) {
	                		return Referrals.referralsByStore($route.current.params.storeID).then(function(data) { 
								return data.data.storeReferrals;
							});
	                	}
	                }
            	}
		).when('/admin/:storeID/your-referrals/:month', 
            	{
					controller : "NonAdminReferralMonthInfoController",
	                templateUrl : 'view/admin/ReferralSingleMonth.html',
	                title: "Your Store Monthly Referrals",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	ReferralsByMonth: function(Referrals, $route) {
	                		return Referrals.referralsByStoreMonth($route.current.params.month, $route.current.params.storeID).then(function(data) { 
								return data.data.storeMonthReferrals;
							});
	                	}
	                }
            	}
		).when('/admin/:storeID/your-referrals/:month/:referralID/view', 
            	{
					controller : "NonAdminReferralSingleController",
	                templateUrl : 'view/admin/referrals/ReferralSingleView.html',
	                title: "View Referral",
	                controllerCssFile: "AdminController",
		            headerContent: "shared/header.html",
		            IntranetLoginContent: true,
		            NeedLoggedInStatus: true,
	                resolve:  {
	                	ReferralSingle: function(Referrals, $route) {
	                		return Referrals.Single($route.current.params.referralID).then(function(data) { 
								return data.data.ReferralSingle[0];
							});
	                	},
	                	SalesmanByStore: function(Users, $route) {
	                		return Users.getUsersByStore($route.current.params.storeID).then(function(data) { 
								return data.data.storeUsers;
							});
	                	}
	                }
            	}
		).when('/admin/settings/referral-users/new', 
            {
				controller : "AddReferralUserController",
	            templateUrl : 'view/admin/settings/settingsTemplate.html',
	            title: "New User",
	            subContent: 'view/admin/settings/referrals/AddReferralUser.html',
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
	            resolve: {
	               	Stores: function(System, StoresList, Permissions) {
	               		return System.CurrentSessionVariables().then(function(response) { 
	               			return Permissions.getUserReferralAdminPermissions(response.data.CurrentSessionUser).then(function(response) {
								var StoreIDS = [];
								angular.forEach(response.data.Marketing, function(value, key) {
									StoreIDS.push(value.storeID)	  		
								});
								return StoresList.GetUserReferralAdminStores(StoreIDS).then(function(data) { 
									return data.data.referralStores;
								});		
							});
	               		});	
	               	}	               	
	            }
        	}
		).when('/admin/settings/referral-members', 
            {
				controller : "AllReferralMembersController",
	            templateUrl : 'view/admin/settings/settingsTemplate.html',
	            title: "All Referral Members List",
	            subContent: 'view/admin/settings/AllReferralMembersList.html',
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
	            resolve: {
	            	AllReferralMembers: function(Referrals) {
	            		return Referrals.getAllMembers().then(function(data) { 
							return data.data.members;
						});
	            	}   	               	
	            }
        	}
		).when('/admin/settings/referral-users/:userID/edit', 
            {
				controller : "EditReferralUserController",
	            templateUrl : 'view/admin/settings/settingsTemplate.html',
	            title: "Edit User",
	            subContent: 'view/admin/settings/referrals/EditReferralUser.html',
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
	            resolve: {
	               	Stores: function(System, StoresList, Permissions) {
	               		return System.CurrentSessionVariables().then(function(response) { 
	               			return Permissions.getUserReferralAdminPermissions(response.data.CurrentSessionUser).then(function(response) {
								var StoreIDS = [];
								angular.forEach(response.data.Marketing, function(value, key) {
									StoreIDS.push(value.storeID)	  		
								});
								return StoresList.GetUserReferralAdminStores(StoreIDS).then(function(data) { 
									return data.data.referralStores;
								});		
							});
	               		});	
	               	},
	               	UserSingle: function(Users, $route) {
	            		return Users.singleUser($route.current.params.userID).then(function(data) { 	
							return data.data.userSingle[0]
						});
	            	}	               	
	            }
        	}
		).when('/admin/your-individual-referrals', 
            {
				controller : "IndividualSalesmanReferralsController",
	            templateUrl : 'view/admin/SalesmanReferralsSummary.html',
	            title: "Your Referrals",
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
	            resolve: {
	            	SalesmanReferrals: function(System, Referrals) {
	            		return System.CurrentSessionVariables().then(function(response) { 
	            			return Referrals.GetReferralsBySalesman(response.data.CurrentSessionUser).then(function(response) {
	            				return response.data.SalesmanReferrals;
	            			});		
	               		});	
	            	}  	            	
	            }
        	}
		).when('/admin/your-individual-referrals/:referralID', 
            {
				controller : "IndividualSalesmanReferralSingleController",
	            templateUrl : 'view/admin/referrals/SalesmanReferralsSingleView.html',
	            title: "Your Referrals",
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
	            resolve: {
	            	ReferralSingle: function(Referrals, $route) {
	            		return Referrals.Single($route.current.params.referralID).then(function(response) { 
	            			return response.data.ReferralSingle[0];	
	               		});	
	            	}  	            	
	            }
        	}
		).when('/admin/settings/referral-members/SD:referralMemberID', 
            {
				controller : "ReferralMemberController",
	            templateUrl : 'view/admin/settings/settingsTemplate.html',
	            title: "View Referral Member",
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    subContent: 'view/admin/settings/ReferralMemberSingle.html',
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
	            resolve: {
	            	MemberSingle: function(Referrals, $route) {
	            		return Referrals.MemberSingle($route.current.params.referralMemberID).then(function(response) { 
	            			return response.data.memberSingle[0];	
	               		});	
	            	},
	            	PaidReferrals: function(Referrals, $route) {
	            		return Referrals.MemberPaidReferrals($route.current.params.referralMemberID).then(function(response) { 
	            			return response.data.memberSinglePaid;	
	               		});	
	            	},
	            	ActiveReferrals: function(Referrals, $route) {
	            		return Referrals.MemberActiveReferrals($route.current.params.referralMemberID).then(function(response) { 
	            			return response.data.memberSingleActive;	
	               		});	
	            	},
	            	InactiveReferrals: function(Referrals, $route) {
	            		return Referrals.MemberInactiveReferrals($route.current.params.referralMemberID).then(function(response) { 
	            			return response.data.memberSingleInactive;	
	               		});	
	            	}   	            	
	            }
        	}
		).when('/admin/settings/referral-members/new', 
            {
				controller : "AddReferralMemberController",
	            templateUrl : 'view/admin/settings/settingsTemplate.html',
	            title: "View Referral Member",
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    subContent: 'view/admin/settings/NewReferralMemberSingle.html',
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
	            resolve: {            	
	            }
        	}
		).when('/admin/reporting/referrals', 
            {
				controller : "ReferralsReportingController",
	            templateUrl : 'view/admin/reportingTemplate.html',
	            title: "Referral Reporting",
	            controllerCssFile: "AdminController",
			    headerContent: "shared/header.html",
			    subContent: 'view/admin/referralReporting.html',
			    IntranetLoginContent: true,
		        NeedLoggedInStatus: true,
		        resolve: {
		        	ReferralStores: function(StoresList) {
	            		return StoresList.GetReferralStores().then(function(response) { 
	            			return response.data.referralStores;	
	               		});	
	            	},
		        }
        	}
		).otherwise(
        	{
        		redirectTo: '/'
        	}
        );
});




function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}
