"use strict"
var CurrentAppPath = Globals.currentAppPath();

function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

//controllers
SidDillonIntranet.controller('IntranetHeaderController', function($scope, $location, $http, Users, Authentication, SessionVariables, System) {
	
	
	$scope.CurrentUserType = SessionVariables.getCurrentSessionUserType();
	
	
	System.CurrentSessionVariables().then(function(response) { 
		if(response.data.CurrentSessionLoggedIn == true) {
			Users.singleUser(response.data.CurrentSessionUser).then(function(data) { 
				$scope.UserLoggedInName = data.data.userSingle[0].firstName + " " +  data.data.userSingle[0].lastName;
			});
		}
		
		if(response.data.CurrentSessionUserType == 1) {
			$scope.settingsLink = "#/admin/settings/main";	
		} else {
			$scope.settingsLink = "#/admin/settings/referral-users";	
		}
		
		if(response.data.CurrentSessionLoggedIn == false) {
			$location.path('/');
		}
	});		
		
	
	
	$scope.logout = function() {
		System.Logout().then(function(response) { 
			window.localStorage.clear();
			SessionVariables.setCurrentSessionUserType(null);
			Authentication.setCurrentSession(false);
			Authentication.setCurrentSessionUserType(null);
			Authentication.setCurrentSessionUserID(null);
			Authentication.setCurrentSessionUserStore(null);
			$location.path('/');
		});		
	}
	
});

SidDillonIntranet.controller('SettingsReferralUsersController', ['$scope', 'ReferralUsersList', function($scope, ReferralUsersList) {
	$scope.referralUsersSelected = true;
	$scope.usersList = ReferralUsersList;
	
	// pagination
	$scope.curPage = 0;
	$scope.pageSize = 10;



	$scope.numberOfPages = function() {
 		return Math.ceil($scope.usersList.length / $scope.pageSize);
 	};
	
}]);


SidDillonIntranet.controller('mainAppCtrl', function($scope, $location, $http, System, Users, Authentication, SessionVariables) {

	System.CurrentSessionVariables().then(function(response) { 
		//these are needed to show differences based on user type
		SessionVariables.setCurrentSessionUserType(response.data.CurrentSessionUserType);
		SessionVariables.setUserID(response.data.CurrentSessionUser);
		Authentication.setCurrentSession(response.data.CurrentSessionLoggedIn);
		//Authentication.setCurrentSessionUserType(response.data.CurrentSessionUserType);
		Authentication.setCurrentSessionUserID(response.data.CurrentSessionUser);
		Authentication.setCurrentSessionUserStore(response.data.CurrentSessionUserStore);
		
		
		
		//this is not the best way that I want to do this, 
		//but currently having separarte routeStates makes the authentication work
		
		$scope.$on('$routeChangeStart', function(scope, next, current) {
			var NeedLoggedInStatus = next.$$route.NeedLoggedInStatus;
			if(NeedLoggedInStatus) {
				//alert(Authentication.getCurrentSession());		
				if(Authentication.getCurrentSession() == false) {
					//alert('no session started test');
					$location.path('/');
				}
			} else if(Authentication.getCurrentSession() == false) {
				$location.path('/');
			}
		});
		
		$scope.$on('$routeChangeSuccess', function(scope, next, current) {
			var NeedLoggedInStatus = next.$$route.NeedLoggedInStatus;
			if(NeedLoggedInStatus) {
				//alert(Authentication.getCurrentSession());		
				if(Authentication.getCurrentSession() == false) {
					//alert('no session started test');
					$location.path('/');
				}
			} else if(Authentication.getCurrentSession() == false) {
				$location.path('/');
			}
			
			//alert(Authentication.getCurrentSessionUserType());
		});
	});	
	
	
	
	
});


SidDillonIntranet.controller('loginController', function($scope, $sce, $location, Users, Authentication, SessionVariables) {
	$scope.userLoginSubmit = function(data) {
		$scope.loadingFinal = true;
		Users.UserLogin(data).then(function(response) { 
			
			if(response.errorMessage) {
				$scope.errorMessage = response.errorMessage;
				$scope.showMessage = true;
				$scope.showMessageText = true;
			}
						
			if(response.finished) {
				SessionVariables.setUserID(response.finished.UserID);
				SessionVariables.setCurrentSessionUserType(response.finished.UserType);
				Authentication.setCurrentSession(response.finished.loggedIn);
				
				Authentication.setCurrentSessionUserStore(response.finished.UserStore);
				Globals.redirect(response.finished.redirect);
				
				
				$scope.showMessage = true;
				$scope.showMessageText = false;
				$scope.loadingFinal = true;
			}
			
			if(response.MySqlError) {
				$scope.errorMessage = response.MySqlError;
				$scope.showMessage = true;
				$scope.showMessageText = true;
			}			
			
			$scope.loadingFinal = false;
		});
	}
});
	
SidDillonIntranet.controller('forgotPasswordCtrl', ['$scope', '$sce', '$http', "$timeout", "Users",  function($scope, $sce, $http, $timeout, Users) {
	$scope.userSubmitEmail = function(data) {
		$scope.loadingFinal = true;
		Users.forgotPassword(data).then(function(response) { 
			if(response.errorMessage) {
				$scope.errorMessage = $sce.trustAsHtml(response.errorMessage);
				$scope.showMessage = true;
				$scope.showMessageText = true;
			}
			
			if(response.MySqlError) {
				$scope.errorMessage = response.MySqlError;
				$scope.showMessage = true;
				$scope.showMessageText = true;
			}
			
			if(response.validEmail) {
				$scope.errorMessage = $sce.trustAsHtml(response.validEmail);
				$scope.showMessage = true;
				$scope.showSuccess = true;
				
				$timeout(function(){ 
					Globals.redirect('#/');
				},5000);
				
				
			}
			$scope.loadingFinal = false;
		});
	}				
}]);	

SidDillonIntranet.controller('requestAccessCtrl', ['$scope', '$sce', 'StoresList', 'Users', function($scope, $sce, StoresList, Users) {
	$scope.RequestAccess = {};
	$scope.storesList = StoresList;
	    
	$scope.requestAccessSubmit = function(data) {
		$scope.loadingFinal = true;
		
		Users.UserRequest(data).then(function(response) { 
			if(response.errorMessage) {
				$scope.errorMessage = $sce.trustAsHtml(response.errorMessage);
				$scope.showMessage = true;
				$scope.showMessageText = true;
			}
			
			if(response.MySqlError) {
				$scope.errorMessage = response.MySqlError;
				$scope.showMessage = true;
				$scope.showMessageText = true;
			}
			
			if(response.success) {
				$scope.errorMessage = $sce.trustAsHtml(response.success);
				$scope.showMessage = true;
				$scope.showSuccess = true;
			}
			$scope.loadingFinal = false;
		});
	}
}]);

SidDillonIntranet.controller('passwordResetCtrl', ['$scope', '$sce', '$timeout', "UserSingle", "Users", function($scope, $sce, $timeout, UserSingle, Users) {
	if (getParameterByName('token') && getParameterByName('u')) { 
		
		if(UserSingle.securityToken == getParameterByName('token') && UserSingle.userID== getParameterByName('u')) {
			$scope.showPasswordResetForm = true;	
		} else {
			$scope.showPasswordResetForm = false;
		}
		
		$scope.resetPassword = function(data) {
			$scope.loadingFinal = true;
			
			Users.NewPassword(data, getParameterByName('u')).then(function(response) { 
				if(response.errorMessage) {
					$scope.errorMessage = response.errorMessage;
					$scope.showMessage = true;
					$scope.showMessageText = true;
				}
				
				if(response.MySqlError) {
					$scope.errorMessage = response.MySqlError;
					$scope.showMessage = true;
					$scope.showMessageText = true;
				}
				
				if(response.success) {
					$scope.errorMessage = response.success;
					$scope.showSuccess = true;
					$scope.showMessage = true;
					$scope.showMessageText = true;
									
					$timeout(function(){ 
						Globals.redirect('#/');
					},5000);					
				}
				$scope.loadingFinal = false;
			});
		}
		
		
	} else {
		$scope.showPasswordResetForm = false;
	}   
}]);

SidDillonIntranet.controller('createCredentialsCtrl', ['$scope', '$sce', '$http', '$timeout', 'UserSingle', 'Users', function($scope, $sce, $http, $timeout, UserSingle, Users) {
	
	if (getParameterByName('token') && getParameterByName('u')) {
		if(UserSingle.securityToken == getParameterByName('token') && UserSingle.userID == getParameterByName('u')) {
			$scope.showCreateCredentialsForm = true;	
		} else {
			$scope.showCreateCredentialsForm = false;
		}   
		
		$scope.newCredentials = function(data) {
			$scope.loadingFinal = true;
			Users.newCredentials(data, UserSingle.userID).then(function(response) { 
				if(response.errorMessage) {
					$scope.errorMessage = response.errorMessage;
					$scope.showMessage = true;
					$scope.showMessageText = true;
				}
				
				if(response.success) {
					$scope.errorMessage = response.success;
					$scope.showMessage = true;
					$scope.showSuccess = true;
								
					$timeout(function(){ 
						Globals.redirect('#/');
					},5000);
								
				}
							
				if(response.MySqlError) {
					$scope.errorMessage = response.MySqlError;
					$scope.showMessage = true;
					$scope.showMessageText = true;
				}
				
				
				$scope.loadingFinal = false;
			});
		}
		
		 		
	}  		
}]);



SidDillonIntranet.controller('DashboardController', ['$scope', 'System', function($scope, System) {	
	System.CurrentSessionVariables().then(function(response) { 
		if(response.data.CurrentSessionUserType == 1) {
			$scope.DashboardContent = "view/admin/adminDashboard.html";	
		} else {
			$scope.DashboardContent = "view/admin/nonAdminDashboard.html";	
		}
	});		
}]);

SidDillonIntranet.controller('SettingTabsController', ['$scope', 'System', 'Permissions', 'SessionVariables', function($scope, System, Permissions, SessionVariables) {	
	var storeReferralAdminStores = [];
	
	System.CurrentSessionVariables().then(function(response) { 
		$scope.CurrentUser = response.data.CurrentSessionUserType;
	});		
	
	
	Permissions.getUserReferralAdminPermissions(SessionVariables.getUserID()).then(function(response) {
		angular.forEach(response.data.Marketing, function(value, key) {
			storeReferralAdminStores.push(value.storeID)	  		
		});
			
		if(storeReferralAdminStores.length > 0) {
			$scope.showReferralMembers = true;
		} 			
	});
	
}]);


SidDillonIntranet.controller('AdminDashboardController', ['$scope', 'StoresList', 'System', function($scope, StoresList, System) {	

	StoresList.getAllToDoLists().then(function(response) { 
		$scope.StoreList = response.data.storeList;
	});
	
	StoresList.GetReferralStores().then(function(response) { 
		$scope.ReferralStores = response.data.referralStores;
	});
		
	System.getCurrentWeek().then(function(response) { 
		$scope.weeklyText = response.data.CurrentWeek;
	});
	
	var d = new Date();
	
	$scope.ReferralToolTipText = "Data based on this current year: Jan, 1 " + d.getFullYear() + " - Dec, 31 " + d.getFullYear();
}]);



SidDillonIntranet.controller('NonAdminDashboardController', ['$scope', 'StoresList', 'System', 'Permissions', 'SessionVariables', 'Referrals', function($scope,  StoresList, System, Permissions, SessionVariables, Referrals) {
	var storeToDoIDs = [];
	var storeReferralAdminStores = [];
	var storeReferrals = [];
	
	Permissions.getUserMarketingPermissions(SessionVariables.getUserID()).then(function(response) {
		angular.forEach(response.data.Marketing, function(value, key) {
			storeToDoIDs.push(value.storeID)	  		
		});
			
		if(storeToDoIDs.length > 0) {
			$scope.ShowToDoLists = true;
			StoresList.GetUserToDoLists(storeToDoIDs).then(function(response) {
				$scope.StoreList = response.data.UserStoreList;					
			});
		} else {
			$scope.ShowToDoLists = false;
		}	
	});
	
	Permissions.getUserReferralAdminPermissions(SessionVariables.getUserID()).then(function(response) {
		angular.forEach(response.data.Marketing, function(value, key) {
			storeReferralAdminStores.push(value.storeID)	  		
		});
			
		if(storeReferralAdminStores.length > 0) {
			$scope.ShowReferralStores = true;
			StoresList.GetUserReferralStores(storeReferralAdminStores).then(function(response) {
				$scope.ReferralStores = response.data.referralStores;					
			});
			$scope.ReferralStores = "";
		} else {
			$scope.ShowReferralStores = false;
		}			
	});
	
	Permissions.getUserReferralPermissions(SessionVariables.getUserID()).then(function(response) {
		angular.forEach(response.data.Marketing, function(value, key) {
			storeReferrals.push(value.storeID)	  		
		});
			
		if(storeReferrals.length > 0) {
			$scope.ShowNonReferralAdmin = true;
			
			Referrals.GetSalesmanReferralCount(SessionVariables.getUserID()).then(function(response) {
				$scope.ReferralCount = response.data[0].ReferralCount;					
			});
			$scope.ReferralStores = "";
		} else {
			$scope.ShowNonReferralAdmin = false;
		}			
	});
		

	System.getCurrentWeek().then(function(response) { 
		$scope.weeklyText = response.data.CurrentWeek;
	});
	
	var d = new Date();
	
	$scope.ReferralToolTipText = "Data based on this current year: Jan, 1 " + d.getFullYear() + " - Dec, 31 " + d.getFullYear();
	$scope.CurrentYear = d.getFullYear();
}]);




SidDillonIntranet.controller('singleStoreToDoListCtrl', ['$scope', '$sce', 'StoreInfo', 'SubmittedFacebookPosts', 'SubmittedTwitterPosts', 'SubmittedBlogPosts', 'CurrentWeekText', 'SingleToDoList', 'Settings', 'Websites', 'StoreUsers', function($scope, $sce, StoreInfo, SubmittedFacebookPosts, SubmittedTwitterPosts, SubmittedBlogPosts, CurrentWeekText, SingleToDoList, Settings, Websites, StoreUsers) {
	$scope.ToDoList = SingleToDoList
	
	
	$scope.SubmittedFacebookPosts = SubmittedFacebookPosts;
	$scope.SubmittedTwitterPosts = SubmittedTwitterPosts;
	$scope.SubmittedBlogPosts = SubmittedBlogPosts;
	
			
	//current week
	$scope.weeklyText = CurrentWeekText;
	
	
	$scope.StoreInfo = StoreInfo;
	
	$scope.storePhone = StoreInfo.Phone;
			
	$scope.storeWebsites = Websites;
			
	$scope.RelatedUsers = StoreUsers;
			
}]);	

SidDillonIntranet.controller('UserStoreToDoListController', ['$scope', '$sce', '$routeParams', 'SingleToDoList', 'CurrentWeekText', 'SubmittedFacebookPosts', 'SubmittedTwitterPosts', 'SubmittedBlogPosts', function($scope, $sce, $routeParams, SingleToDoList, CurrentWeekText, SubmittedFacebookPosts, SubmittedTwitterPosts, SubmittedBlogPosts) {
	
	$scope.storeName = SingleToDoList.name;
	
	$scope.SubmittedFacebookPosts = SubmittedFacebookPosts;
	$scope.SubmittedTwitterPosts = SubmittedTwitterPosts;
	$scope.SubmittedBlogPosts = SubmittedBlogPosts;
			
	//current week
	$scope.weeklyText = CurrentWeekText;
			
			
	//facebook
	$scope.completedFacebook = SingleToDoList.SumittedFacebook;
	$scope.totalFacebookPosts = SingleToDoList.RequiredFacebook;
			
	//twitter
	$scope.completedTwitter = SingleToDoList.SumittedTwitter;
	$scope.totalTwitterPosts = SingleToDoList.RequiredTwitter;
			
	//blog posts
	$scope.completedBlogPosts = SingleToDoList.SumittedBlog;
	$scope.totalBlogPosts = SingleToDoList.RequiredBlog; 
			
	$scope.currentProgress = Math.ceil((SingleToDoList.TotalSubmittedContent * 100));	
	
	$scope.storeID = SingleToDoList.id
}]);	


SidDillonIntranet.controller('ApproveDeclineUserController', ['$scope', '$location', '$sce', 'UserSingle', 'StoreList', 'Users', 'Permissions', function($scope, $location, $sce, UserSingle, StoreList, Users, Permissions) {
	
	$scope.changedValue = function(item) {
		if(item == 2 || item == 3) {
			$scope.StoreSelectDropDown = true;
			$scope.showPermissions = true;
			$scope.IsNotAdminUser = true;
		} else {
			$scope.StoreSelectDropDown = false;
			$scope.showPermissions = false;
			$scope.IsNotAdminUser = false;
		}
	}
	$scope.settingsUsersSelected = true;
	
	$scope.UserSingle = {};
	
	$scope.userFullName = UserSingle.firstName + " " + UserSingle.lastName;
	$scope.CurrentJobTitle = UserSingle.JobTitle;
	
	$scope.UserSingle.userFirstName = UserSingle.firstName;
	$scope.UserSingle.userLastName = UserSingle.lastName;
	$scope.UserSingle.userEmail = UserSingle.email;
	$scope.storeDropDown = UserSingle.store;
				
	$scope.UserSingle.userStore = UserSingle.store;
	$scope.UserSingle.userNameLogin = UserSingle.userName;	
	
	if (UserSingle.userType == 2 || UserSingle.userType == 3) {
		$scope.StoreSelectDropDown = true;	
		$scope.IsNotAdminUser = true;
	}
	
	if (UserSingle.userType == 2) {
		$scope.StoreAdminSelect = true;	
	}
	
	if (UserSingle.userType == 3) {
		$scope.NormalUser = true;	
	}
	
	$scope.storesList = StoreList;
	$scope.UserSingle.userStore = UserSingle.store;
	$scope.currentStore = UserSingle.store;
	
	angular.forEach(StoreList, function(value, key) {
  		if(UserSingle.store === value.id) {
  			if(value.isReferralClubStore == 1) {	;
  				$scope.showReferralPermissions = true;	
  			} else {
	  			$scope.showReferralPermissions = false;
	  		}
  					
  		} 
	});
	
	
	
	
	
	$scope.referralStatus = function(data) {
		angular.forEach(StoreList, function(value, key) {
  			if(value.id === data) {
  				if(value.isReferralClubStore == 1) {	
  					$scope.showReferralPermissions = true;	
  				} else {
	  				$scope.showReferralPermissions = false;
	  			}  				
  			} 
		});
	}
	
	$scope.approveUser = function(data) {
		$scope.loadingFinal = true;
		
		Users.ApproveUser(data, UserSingle.userID).then(function(response) { 
			
			if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
			if(response.MySqlError) {$scope.errorMessage = response.MySqlError; $scope.showMessage = true;}
			if(response.redirect) {Globals.redirect(response.redirect);}
			$scope.loadingFinal = false;
		});
				
	}
	
	$scope.declineUser = function() {
		Users.DeleteUser(UserSingle.userID).then(function(data) { 
			$location.path("/admin/settings/users");
		});			
	}
}]);

SidDillonIntranet.controller('UserProfileController', ['$scope', '$location', 'UserSingle', 'StoreList', 'Users', 'Permissions', function($scope, $location, UserSingle, StoreList, Users, Permissions) {
	$scope.UserSingle = {};
	if (UserSingle.userType == 2 || UserSingle.userType == 3) {
		$scope.hideUserTypeContent = true;
		$scope.IsNotAdminUser = true;
		$scope.CurrentJobTitle = UserSingle.JobTitle
		$scope.showUserNotifyIndicator = true;
		if(UserSingle.isReceivingNotifications == 1) {
			$scope.receivingNotificaitons = true;
			$scope.SignedUptext = "You are receiving marketing notifications.";
			$scope.addedRemoveText = "removed";
			$scope.SignedUpEmailNotificaitons = true;
		} else {
			$scope.SignedUptext = "You are not receiving marketing notifications.";
			$scope.addedRemoveText = "added";
			$scope.notSignedUpEmailNotificaitons = true;
		}	
	}
	
	$scope.UserSingle.userFirstName = UserSingle.firstName;
	$scope.UserSingle.userLastName = UserSingle.lastName;
	$scope.UserSingle.userEmail = UserSingle.email;
	$scope.storeDropDown = UserSingle.store;
				
	$scope.UserSingle.userStore = UserSingle.store;
	$scope.UserSingle.userNameLogin = UserSingle.userName;
	
	$scope.currentStore = UserSingle.name
	
	$scope.userFormSubmit = function(data) {
		$scope.loadingFinal = true;
		$scope.showSuccess = false;
		
		Users.SaveProfile(data, UserSingle.userID).then(function(response) { 
			if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
			if(response.redirect) {Globals.redirect(response.redirect); $scope.showMessage = true;}
			
			if(response.MySqlError) {$scope.errorMessage = response.MySqlError; $scope.showMessage = true;}
						
			if(response.userUpdated) {$scope.errorMessage = "Profile Updated"; $scope.showSuccess = true; $scope.showMessage = true;}
			$scope.loadingFinal = false;
		});
	}
	
	
}]);

SidDillonIntranet.controller('AddUserController', ['$scope', '$location', 'StoreList', 'Users', function($scope, $location, StoreList, Users) {
	$scope.storesList = StoreList;
	
	$scope.userFormSubmit = function(data) {
		$scope.loadingFinal = true;
		$scope.showSuccess = false;
		
		Users.NewUser(data).then(function(response) { 
			if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
			if(response.redirect) {Globals.redirect(response.redirect); $scope.showMessage = true;}
			if(response.MySqlError) {$scope.errorMessage = response.MySqlError; $scope.showMessage = true;}
				
			$scope.loadingFinal = false;
		});
		
	}
	
	$scope.changedValue = function(item) {
		if(item == 2 || item == 3) {
			$scope.StoreSelectDropDown = true;
			$scope.showPermissions = true;
			$scope.IsNotAdminUser = true;
		} else {
			$scope.StoreSelectDropDown = false;
			$scope.showPermissions = false;
			$scope.IsNotAdminUser = false;
		}
	}
	
	$scope.referralStatus = function(data) {
		angular.forEach(StoreList, function(value, key) {
  			if(value.id === data) {
  				if(value.isReferralClubStore == 1) {	
  					$scope.showReferralPermissions = true;	
  				} else {
	  				$scope.showReferralPermissions = false;
	  			}  				
  			} 
		});
	}
	
	
}]);



SidDillonIntranet.controller('EditProfileController', ['$scope', '$location', '$timeout', 'UserSingle', 'StoreList', 'Users', 'Permissions', function($scope, $location, $timeout, UserSingle, StoreList, Users, Permissions) {
	$scope.UserSingle = {};
	
	$scope.currentStore = UserSingle.store;
	if(UserSingle.isReceivingNotifications == 1) {
		$scope.receivingNotificaitons = true;
	} else {
		$scope.receivingNotificaitons = false;
	}
	$scope.userFullName = UserSingle.firstName + " " + UserSingle.lastName;
	$scope.CurrentJobTitle = UserSingle.JobTitle;
	$scope.settingsUsersSelected = true;
	
	$scope.UserSingle.userFirstName = UserSingle.firstName;
	$scope.UserSingle.userLastName = UserSingle.lastName;
	$scope.UserSingle.userEmail = UserSingle.email;
	$scope.storeDropDown = UserSingle.store;
				
	$scope.UserSingle.userStore = UserSingle.store;
	$scope.UserSingle.userNameLogin = UserSingle.userName;
	
	$scope.UserSingle.userType = UserSingle.userType;
	$scope.UserSingle.deactiveReason = UserSingle.deactiveReasonStatus;
	
	if(UserSingle.userType == 1) {
		$scope.AdminSelect = true;
	} else if (UserSingle.userType == 2 || UserSingle.userType == 3) {
		$scope.StoreSelectDropDown = true;	
		$scope.showPermissions = true;
		$scope.IsNotAdminUser = true;
	}
	
	if(UserSingle.userType == 1) {
		$scope.AdminSelect = true;
	} else if(UserSingle.userType == 2) {
		$scope.GeneralManager = true;
	} else if(UserSingle.userType == 3) {
		$scope.SalesManager = true;
	}
	
	if(UserSingle.isReferralUser == 1) {
		$scope.isReferralUser = 1;
	} else {
		$scope.isReferralUser = 0;
	}
	
	$scope.storesList = StoreList;
	
	angular.forEach(StoreList, function(value, key) {
  		if(UserSingle.store === value.id) {
  			if(value.isReferralClubStore == 1) {	;
  				$scope.showReferralPermissions = true;	
  			} else {
	  			$scope.showReferralPermissions = false;
	  		}
  					
  		} 
	});

	if(UserSingle.isActive == 1) {
		$scope.UserSingle.userIsActive = 1;
		$scope.ShowDeactiveReasonText = false;
	} else {
		$scope.UserSingle.userIsActive = 0;
		$scope.ShowDeactiveReasonText = true;
	}

	var MarketingPermission = [];
	var ReferralPermission = [];
	var ReferralAdminPermission = [];
	
	
	
	
	angular.forEach(Permissions, function(value, key) {
  		//alert("Permission Key Value: " + key);	
  		if(value.PermissionType == "Marketing") {
  			MarketingPermission.push(value.storeID)
  		}
  		
  		if(value.PermissionType == "Referral") {
  			ReferralPermission.push(value.storeID)	
  		}
  		
  		if(value.PermissionType == "ReferralAdmin") {
  			ReferralAdminPermission.push(value.storeID)	
  		}
	});
	//alert(MarketingPermission)
	
	$scope.getMarketingValue = function(storeID) {
		var checked;
		
		for(var i=0; i<= MarketingPermission.length; i++) {
			if(storeID== MarketingPermission[i]) {
		         checked = true;
		    }
		}
		return checked;
		//console.log(storeID)	
	}
		
	$scope.getReferralValue = function(storeID) {
		var checked;
		
		for(var i=0; i<= ReferralPermission.length; i++) {
			if(storeID== ReferralPermission[i]) {
		         checked = true;
		    }
		}
		return checked;
	}
	
	$scope.getReferralAdminValue= function(storeID) {
  		var checked;
		
		for(var i=0; i<= ReferralAdminPermission.length; i++) {
			if(storeID== ReferralAdminPermission[i]) {
		         checked = true;
		    }
		}
		return checked;	
	}
	
	
	
			
	$scope.referralStatus = function(data) {
		angular.forEach(StoreList, function(value, key) {
  			if(value.id === data) {
  				if(value.isReferralClubStore == 1) {	
  					$scope.showReferralPermissions = true;	
  				} else {
	  				$scope.showReferralPermissions = false;
	  			}  				
  			} 
		});
	}
	
			
	$scope.changedValue = function(item) {
		if(item == 2 || item == 3) {
			$scope.StoreSelectDropDown = true;
			$scope.showPermissions = true;
			$scope.IsNotAdminUser = true;
		} else {
			$scope.StoreSelectDropDown = false;
			$scope.showPermissions = false;
			$scope.IsNotAdminUser = false;
		}
	}
		
	
		
			
	$scope.userFormSubmit = function(data) {
		$scope.loadingFinal = true;
		$scope.showSuccess = false;
		
		Users.EditUser(data, UserSingle.userID).then(function(response) { 
			if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
			if(response.redirect) {Globals.redirect(response.redirect); $scope.showMessage = true;}
			
			if(response.MySqlError) {$scope.errorMessage = response.MySqlError; $scope.showMessage = true;}
						
			if(response.userUpdated) {
				$scope.errorMessage = "Profile Updated"; 
				$scope.showSuccess = true; 
				$scope.showMessage = true;
				
				$timeout(function(){
				  $scope.userFullName = response.userUpdated
				});	
				
			}
			$scope.loadingFinal = false;
		});
	}
	
	$scope.ShowUserDeactivation = function(element) {
		//alert(element)
		if(element == 0) {
			$scope.ShowDeactiveReasonText = 1;
		} else {
			$scope.ShowDeactiveReasonText = 0;
		}	
	}
	
}]);	
		
SidDillonIntranet.controller('PostListCtrl', ['$scope', 'NewPosts', 'ApprovedPosts', 'DeclinedPosts', 'PostedPosts', function($scope, NewPosts, ApprovedPosts, DeclinedPosts, PostedPosts) {	
	$scope.NewPostsList = NewPosts;
	$scope.ApprovedPostsList = ApprovedPosts;
	$scope.DeclinedPostsList = DeclinedPosts;
	$scope.PostedPostsList = PostedPosts;

}]);		

SidDillonIntranet.controller('AdminSinglePostCtrl', ['$scope', '$sce', '$http', '$routeParams', 'SocialMediaPostSingle', 'SocialMediaList', function($scope, $sce, $http, $routeParams, SocialMediaPostSingle, SocialMediaList) {
	
	$scope.SocialMediaSingle = SocialMediaPostSingle;
	
	$scope.SinglePost = {}
	
	$scope.SinglePost.socialMediaContent = SocialMediaPostSingle.Content;
	$scope.SinglePost.socialMediaKeyWords = SocialMediaPostSingle.KeyWords;
	
	
	if(SocialMediaPostSingle.isPostActive == 1) {
		$scope.ShowFormContent = true;
		$scope.ShowApproveDeclineButtons = true;
	} else {
		$scope.ShowFormContent = false;
		$scope.ShowApproveDeclineButtons = false;
	}
	
	$scope.PostContent = $sce.trustAsHtml(SocialMediaPostSingle.Content);
	
	
	if(SocialMediaPostSingle.isDeclined == 1) {
		$scope.showStatusPostSection = true;
		$scope.alertColor = "alert-danger";
		$scope.adminReadOnlyView = true;
		$scope.ShowDeclinedNotes = true;
	}
	
	if(SocialMediaPostSingle.isApproved == 1) {
		$scope.showStatusPostSection = true;
		$scope.alertColor = "alert-success";
		$scope.adminReadOnlyView = true;
		$scope.showPostButton = true;
	} 
	
	if(SocialMediaPostSingle.isPosted == 1) {
		$scope.ShowIsPostedIndicator = true;
		$scope.showPostButton = false;
	}
	
	
				
	$scope.options = {
		filebrowserUploadUrl: CurrentAppPath + "system/uploadImages",
		filebrowserImageUploadUrl: CurrentAppPath + "system/uploadImages",
	 	filebrowserImageWindowWidth: '640',
	    filebrowserImageWindowHeight: '480',
	    baseUrl: '/view/'
	};
		
				
		
		
			
				
				
				//admin form submit
		$scope.markPostSubmit = function() {
			$scope.loadingFinal = true;
			$scope.showSuccess = false;
					
					
			SocialMediaList.MarkAsPosted(SocialMediaPostSingle.socialID).then(function(response) { 
				if(response.finished) {
					$scope.errorMessage = response.finished.msg; 
					$scope.showSuccess = true; 
					$scope.showMessage = true; 
					$scope.ShowIsPostedIndicator = true;
					$scope.showPostButton = false;
					SocialMediaPostSingle.isPostedDate = response.finished.date;
					$scope.postedDateTime = response.finished.time;
					$scope.PostedOrDeclinedNotesHeader = "Posted On";
				}		
				$scope.loadingFinal = false;
			});
		}
						
	
	$scope.declinePostFormSubmit = function(data) {
		$scope.loadingFinalDecline = true;
		SocialMediaList.DeclinePost(data, SocialMediaPostSingle.socialID).then(function(response) { 
			if(response.errorMessage) {$scope.showMessageDecline = true; $scope.errorMessageDecline = response.errorMessage}
			if(response.redirect) {
				$scope.showStatusPostSection = true;
				$scope.alertColor = "alert-danger";
				$scope.showDeclinePost = false;
				$scope.ShowFormContent = false;
				$scope.ShowApproveDeclineButtons = false;
				SocialMediaPostSingle.isPostActive = 0;
				SocialMediaPostSingle.StatusText = "Declined";
				SocialMediaPostSingle.ApprovedDeclinedByFullName = response.redirect.FullName;
				SocialMediaPostSingle.ApprovedDeclinedDate = response.redirect.DateSubmitted;
				SocialMediaPostSingle.isDeclinedNotes = response.redirect.ReasonNotes;
				$scope.adminReadOnlyView = true;
				$scope.ShowDeclinedNotes = true;
				
			}
			$scope.loadingFinalDecline = false;	
		});	
	}
			
			
			
	
	$scope.adminApprovePost = function() {
		SocialMediaList.ApprovePost(SocialMediaPostSingle.socialID).then(function(response) { 
			if(response.redirect) {
				$scope.ShowFormContent = false;
				$scope.ShowApproveDeclineButtons = false;
				$scope.showStatusPostSection = true;
				SocialMediaPostSingle.StatusText = "Approved";
				SocialMediaPostSingle.ApprovedDeclinedByFullName = response.redirect.FullName;
				SocialMediaPostSingle.ApprovedDeclinedDate = response.redirect.DateSubmitted;
				$scope.adminReadOnlyView = true;
				$scope.alertColor = "alert-success";
				$scope.showPostButton = true;
				SocialMediaPostSingle.isPostActive = 0;
			}
		});					
	}
			
	$scope.saveDraft = function(data) {
		$scope.loadingFinal = true;
				
		SocialMediaList.SaveDraft(data, SocialMediaPostSingle.socialID).then(function(response) { 
			if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
			if(response.DraftSaved) {$scope.errorMessage = response.DraftSaved; $scope.showSuccess = true; $scope.showMessage = true;}
			$scope.loadingFinal = false;	
		});
	}
						
	$scope.showDeclinePopupForm = function() {
		$scope.showDeclinePost = true;
	}
			
	$scope.hideDeclinePopupForm = function() {
		$scope.showDeclinePost = false;
		$scope.showMessageDecline = false;
	}
			
}]);
	
//settings
SidDillonIntranet.controller('settingsMainCtrl', ['$scope', 'Settings', 'System', function($scope, Settings, System) {
	$scope.settingsMainSelected = true
			
	$scope.MainSettings = {};
			
	$scope.MainSettings.summaryEmailReminder = Settings.SummaryEmailReminder;
	$scope.MainSettings.urgentPostEmail = Settings.urgentEmail;
			
	$scope.mainSettingsSubmit = function(data) {
		$scope.loadingFinal = true;
		$scope.showSuccess = false;
				
		System.SaveMainSettings(data).then(function(response) {
			if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
							
			if(response.MySqlError) {$scope.errorMessage = response.MySqlError; $scope.showMessage = true;}
			if(response.updatedSetings) {$scope.errorMessage = response.updatedSetings; $scope.showMessage = true; $scope.showSuccess = true}
			$scope.loadingFinal = false;
		});				
	}
}]);

SidDillonIntranet.controller('settingsToDoListCtrl', ['$scope', '$sce', 'SettingsToDoList', 'System', function($scope, $sce, SettingsToDoList, System) {
	$scope.settingsToDoListSelected = true
			
	$scope.subContent = 'view/admin/settings/toDoList.html';
	$scope.ToDoListSettings = {}
			
	$scope.ToDoListSettings.toDoTwitter = SettingsToDoList.TwitterPosts;
	$scope.ToDoListSettings.facebookpost = SettingsToDoList.facebookPosts;
	$scope.ToDoListSettings.blogpost = SettingsToDoList.blogPost;
			
	$scope.toDoListSubmit = function(data) {
		$scope.loadingFinal = true;
		$scope.showSuccess = false;
				
		System.SaveToDoListSettings(data).then(function(response) { 	
			if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
			if(response.updatedSetings) {$scope.errorMessage = response.updatedSetings; $scope.showMessage = true; $scope.showSuccess = true}
			$scope.loadingFinal = false;
		});
	}
}]);

SidDillonIntranet.controller('settingsStoreListCtrl', ['$scope', '$sce', '$http', 'Stores', 'StoresList', function($scope, $sce, $http, Stores, StoresList) {
	$scope.settingsStoreListSelected = true;
	$scope.StoreList = Stores;
	
	$scope.deleteStore = function(deletingId, index) {
		StoresList.DeleteStore(deletingId).then(function(response) { 	
			$scope.StoreList.splice(index, 1);
		});
	}   

				
}]);

SidDillonIntranet.controller('settingsSingleStoreCtrl', ['$scope', '$sce', 'StoresList', 'State', 'StoreInfo', 'StoreWebsites', 'Websites', 'StoreAdminUsers', function($scope, $sce, StoresList, State, StoreInfo, StoreWebsites, Websites, StoreAdminUsers) {
	$scope.StoreSingle = {};
	$scope.settingsStoreListSelected = true;
	
	$scope.UserStoreAdmins = StoreAdminUsers
	
	$scope.DefaultUser = StoreInfo.ReferralDefaultUser;
	$scope.StoreSingle.referralUserDefault = StoreInfo.ReferralDefaultUser;
	
	
	if(State == "Edit") {
		$scope.SingleStoreHeader = "Edit Store";	
		$scope.showBreadCrumbs = true;
		
		$scope.storeName = StoreInfo.name;
		
		
		
		$scope.StoreSingle.storeName = StoreInfo.name;
		
		$scope.StoreSingle.storeFacebookUrl = StoreInfo.facebookLink;
		$scope.StoreSingle.storeTwitterURL = StoreInfo.twitterURL;
		$scope.StoreSingle.storeYouTubeUrl = StoreInfo.youtubeURL;
		$scope.StoreSingle.storeInstagramUrl = StoreInfo.instagramURL;
		$scope.StoreSingle.storeGooglePlusUrl = StoreInfo.GooglePlusURL;
		$scope.StoreSingle.storeStreet = StoreInfo.Street;
		$scope.StoreSingle.storeCity = StoreInfo.City;
		$scope.StoreSingle.storeState = StoreInfo.State;
		$scope.StoreSingle.storeZip = StoreInfo.Zip;
		$scope.StoreSingle.storePhone = StoreInfo.Phone;
		
		$scope.StoreSingle.referralEmailDefault = StoreInfo.ReferralStoreEmailDefault;
		
		if(StoreInfo.isReferralClubStore == 1) {
			$scope.StoreSingle.isReferralStore = 1;
			$scope.showReferralEmail = true;
		} else {
			$scope.StoreSingle.isReferralStore = 0;
		}

		$scope.showWebsites = true;		
		
		
		//websites
		$scope.Website = {};
				
		var websiteRows = [];
		var i = 0;
					
		var websiteContentLength = StoreWebsites.length;
			
		if(websiteContentLength > 0) {
			$scope.WebsiteRowsPresent = true;
			for (var key in StoreWebsites) {
				websiteRows.push(StoreWebsites[key]);
			}
		} 
			
		$scope.addWebsiteRow = function() {	
			i++;
			$scope.WebsiteRowsPresent = true;	
			websiteRows.push(i);					
		};
				
		$scope.Websites = websiteRows;
				
		var websiteID = [];
		var name = [];
		var provider = [];
				
		for (var key in StoreWebsites) {
			websiteID.push(StoreWebsites[key].websiteID);
			name.push(StoreWebsites[key].name);
			provider.push(StoreWebsites[key].ThirdParty)
		}
				
		$scope.Website.websiteID = websiteID
		$scope.Website.webName = name;
		$scope.Website.thirdparty = provider;
		
		
		$scope.websitesSubmit = function(data) {
			$scope.loadingFinal = true;
				
			Websites.SaveWebsites(data, StoreInfo.id).then(function(response) { 	
				if(response.success) {$scope.showWebsiteMessage = true; $scope.showSuccess = true; $scope.websiteMessage = response.success;}
				$scope.loadingFinal = false;
			});
		}
		
		$scope.deleteWebsite = function(deletingId, index){
			Websites.DeleteStore(deletingId).then(function(response) { 	
				$scope.Websites.splice(index, 1);
			});
		}
		
	} else {
		$scope.SingleStoreHeader = "Add New Store";	
	}
			
	
		
	$scope.ShowReferralEmail = function(element) {
		
		//alert(element)
		if(element == 0) {
			$scope.showReferralEmail = false;
		} else {
			$scope.showReferralEmail = true;
		}		
	}		
	
			
	$scope.storeSubmit = function(data) {	
		$scope.loadingFinal = true;
		if(State == "Edit") {
			StoresList.SaveStore(data, StoreInfo.id).then(function(response) { 	
				if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
				if(response.storeUpdated) {$scope.errorMessage = response.storeUpdated; $scope.showMessage = true; $scope.showSuccess = true; $scope.storeName = response.updatedStoreName;}
				$scope.loadingFinal = false;
			});
			
			
		} else {
			StoresList.AddStore(data).then(function(response) { 	
				if(response.errorMessage) {$scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
				if(response.redirect) {Globals.redirect(response.redirect); $scope.showMessage = true;}
				$scope.loadingFinal = false;
			});	
		}
	};			
}]);

SidDillonIntranet.controller('settingsUsersListCtrl', ['$scope', '$sce', '$timeout', 'UsersList', 'Users', function($scope, $sce, $timeout, UsersList, Users) {
	var userID;
	var RowUpdated;
	$scope.settingsUsersSelected = true;
			
	$scope.usersList = UsersList;
			

	
	//$scope.editedItem = {};
			
	$scope.deactivateUser = function(Id, index) {
		userID = Id;
		RowUpdated = index;
		$scope.ShowDeactivatePopup = true;
		//angular.copy(UsersList[RowUpdated], $scope.editedItem);				
	}
	
	$scope.hideDeactivateUserForm = function() {
		$scope.ShowDeactivatePopup = false;
	}
	
	$scope.deactiveUserForm = function(data) {
		$scope.loadingDeactivateFinal= true;
		Users.DeactivateUser(data, userID).then(function(data) { 
			if(data.errorMessage) {
				$scope.showDeactivateMessage = true;
				$scope.errorDeactivateMessage = data.errorMessage;
			}
			if(data.success) {
				$scope.ShowDeactivatePopup = false;
				$timeout(function(){
					UsersList[RowUpdated].isActive = 2;
				});
				
			}
			
			$scope.loadingDeactivateFinal = false;
		
		});	
	}
	
		// pagination
	$scope.curPage = 0;
	$scope.pageSize = 10;

	$scope.numberOfPages = function() {
 		return Math.ceil($scope.usersList.length / $scope.pageSize);
 	};
	
	
}]);

SidDillonIntranet.controller('settingsLogsCtrl', ['$scope', '$sce', 'errorLogsPage', function($scope, $sce, errorLogsPage) {
	$scope.settingsLogsSelected = true;
	$scope.logs = $sce.trustAsHtml(errorLogsPage);
}]);

SidDillonIntranet.controller('SystemController', ['$scope', '$sce', 'CurrentSession', 'PHPInfo', function($scope, $sce, CurrentSession, PHPInfo) {
	$scope.settingsSystemSelected = true;
	
	
	$scope.currentSessionUserID = CurrentSession.user;
	$scope.currentSessionUserStore = CurrentSession.storeID;
	$scope.currentSessionUserType = CurrentSession.userType;
	
	$scope.phpInfo = $sce.trustAsHtml(PHPInfo);
}]);


SidDillonIntranet.controller('cronJobListCtrl', ['$scope', '$sce', 'AppPath', function($scope, $sce, AppPath) {
	$scope.settingsCronSelected = true;
	$scope.CurrentAppPath = AppPath;
}]);

SidDillonIntranet.controller('AddSocialMediaPostController', ['$scope', '$sce', '$http', '$routeParams', '$location', 'StoreToDoList', 'SocialMediaList', 'StoreID', function($scope, $sce, $http, $routeParams, $location, StoreToDoList, SocialMediaList, StoreID) {
	$scope.breakCrumbSocialMediaLink = "#/admin/" + StoreID + "/your-to-do-list";
	$scope.postState = "Add";
	$scope.PostSingleHeader = "Submit " + $routeParams.type + " Post";
	$scope.socialMediaListText = "Store To Do List";
	$scope.socialMediaIcon = "public/images/" + $routeParams.type + "Icon.png";
	
				
	$scope.options = {
		filebrowserUploadUrl: CurrentAppPath + "system/uploadImages",
		filebrowserImageUploadUrl: CurrentAppPath + "system/uploadImages",
	 	filebrowserImageWindowWidth: '640',
	    filebrowserImageWindowHeight: '480',
		baseUrl: '/view/'
	};
					
						
	$scope.postFormSubmit = function(data) {
		$scope.loadingFinal = true;
		$scope.showSuccess = false;
		SocialMediaList.AddPost(data, $routeParams.storeID).then(function(response) { 
			if(response.errorMessage) { $scope.errorMessage = response.errorMessage; $scope.showMessage = true;}
			if(response.redirect) {Globals.redirect(response.redirect); $scope.showMessage = true; $scope.loadingFinal = true;}
			$scope.loadingFinal = false;
		});		
	}
	
	//prevent excess required posts
	switch($routeParams.type) {
		case "facebook":
			$scope.SocialMediaType = 1;
			break;
		case "twitter":
			$scope.SocialMediaType = 2;
			break;
		case "blog":
			$scope.SocialMediaType = 3;
			break;
	}
		
	
}]);


SidDillonIntranet.controller('SocialMediaPostController', ['$scope', '$sce', '$http', '$routeParams', '$location', 'SocialMediaList', 'PostSingle', function($scope, $sce, $http, $routeParams, $location, SocialMediaList, PostSingle) {
	$scope.SocialMediaSingle = PostSingle;	
	
	$scope.PostContent = $sce.trustAsHtml(PostSingle.Content);
					
	if(PostSingle.isApproved == 1) {
		$scope.showStatusPostSection = true;
		$scope.alertColor = "alert-success";
		$scope.adminReadOnlyView = true;
		$scope.showPostButton = true;
	}
	
	if(PostSingle.isDeclined == 1) {
		$scope.showStatusPostSection = true;
		$scope.alertColor = "alert-danger";
		$scope.adminReadOnlyView = true;
		$scope.ShowDeclinedNotes = true;
	}
	
	if(PostSingle.isPosted == 1) {
		$scope.ShowIsPostedIndicator = true;
	}				

}]);

SidDillonIntranet.controller('AdminReferralStoreInfoController', ['$scope', '$sce', '$location', '$routeParams', 'ReferralsList', function($scope, $sce, $location, $routeParams, ReferralsList) {
	$scope.storeName = ReferralsList.name;
	
	$scope.storeID = $routeParams.storeID;
	
	$scope.JanuaryPayout = Math.round(ReferralsList.January);
	$scope.FebruaryPayout = Math.round(ReferralsList.February);
	$scope.MarchPayout = Math.round(ReferralsList.March);
	$scope.AprilPayout = Math.round(ReferralsList.April);
	$scope.MayPayout = Math.round(ReferralsList.May);
	$scope.JunePayout = Math.round(ReferralsList.June);
	$scope.JulyPayout = Math.round(ReferralsList.July);
	$scope.AugustPayout = Math.round(ReferralsList.August);
	$scope.SeptemberPayout = Math.round(ReferralsList.September);
	$scope.OctoberPayout = Math.round(ReferralsList.October);
	$scope.NovemberPayout = Math.round(ReferralsList.November);
	$scope.DecemberPayout = Math.round(ReferralsList.December);
	
	$scope.JanuaryCount = ReferralsList.JanuaryCount;
	$scope.FebruaryCount = ReferralsList.FebruaryCount;
	$scope.MarchCount = ReferralsList.MarchCount;	
	$scope.AprilCount = ReferralsList.AprilCount;
	$scope.MayCount = ReferralsList.MayCount;
	$scope.JuneCount = ReferralsList.JuneCount;
	$scope.JulyCount = ReferralsList.JulyCount;
	$scope.AugustCount = ReferralsList.AugustCount;
	$scope.SeptemberCount = ReferralsList.SeptemberCount;
	$scope.OctoberCount = ReferralsList.OctoberCount;
	$scope.NovemberCount = ReferralsList.NovemberCount;
	$scope.DecemberCount = ReferralsList.DecemberCount;
	
	$scope.TotalPayoutByYear = parseInt(ReferralsList.January) + 
							   parseInt(ReferralsList.February) + 
							   parseInt(ReferralsList.March) + 
							   parseInt(ReferralsList.April) + 
							   parseInt(ReferralsList.May) + 
							   parseInt(ReferralsList.June) + 
							   parseInt(ReferralsList.July) + 
							   parseInt(ReferralsList.August) +
							   parseInt(ReferralsList.September) + 
							   parseInt(ReferralsList.October) +
							   parseInt(ReferralsList.November) +
							   parseInt(ReferralsList.December)
							   
	
	
	$scope.ReferralCount = function(Count) {
		return "Total Count: " + Count
	}
	
	//$scope.ReferralToolTipText = "Data based on this current year: Jan, 1 " + d.getFullYear() + " - Dec, 31 " + d.getFullYear();						   
							   
	var d = new Date();
	$scope.currentYear = d.getFullYear()
}]);

SidDillonIntranet.controller('AdminReferralMonthInfoController', ['$scope', '$routeParams', 'ReferralsByMonth', function($scope, $routeParams, ReferralsByMonth) {
	var d = new Date();
	$scope.CurrentYear = d.getFullYear();
	
	$scope.MonthSelected = $routeParams.month;
	$scope.storeID = $routeParams.storeID;
	
	$scope.Referrals = ReferralsByMonth;
	var d = new Date();
	
	$scope.calculateAge = function(submittedDate, submittedTime) {
		
		var unixTimeStampDay = submittedDate
		var unixTimeStampHours = submittedTime;
		
		var unixTimeDifference = Date.now() - (Date.parse(unixTimeStampDay + " " + unixTimeStampHours))
		//1 day = 86400 seconds
		var DaysOld = unixTimeDifference / 86400 / 1000;
		
		return Math.round(DaysOld);
	}
	
	$scope.currentDate = Date.now();
	
	// pagination
	$scope.curPage = 0;
	$scope.pageSize = 10;



	$scope.numberOfPages = function() {
 		return Math.ceil($scope.Referrals.length / $scope.pageSize);
 	};
}]);

SidDillonIntranet.controller('AdminReferralSingleController', ['$scope', '$routeParams', '$timeout', '$sce', '$window', 'ReferralSingle', 'SalesmanByStore', 'Referrals', function($scope, $routeParams, $timeout, $sce, $window, ReferralSingle, SalesmanByStore, Referrals) {
	$scope.MonthSelected = $routeParams.month
	$scope.storeID = $routeParams.storeID;
	
	$scope.ReferralObject = ReferralSingle;
	
	
	
	$scope.ShowReadOnlySalesComments = true;
	
	
	$scope.CurrentAmountEarned = parseInt(ReferralSingle.PaidAmount);
	
	if(ReferralSingle.notes) {
		$scope.showNotes = true;	
	}
	
	
	$scope.SaveSoldDataButton = "Mark As Sold"
	$scope.LostNotesSubmit = "Mark As Lost";
	$scope.LostNotes = ReferralSingle.LostNotes;
	
	
	$scope.StoreSalesman = SalesmanByStore;
	
	$scope.referralSalesman = ReferralSingle.salesmanUserID;
	$scope.CurrentSalesman = ReferralSingle.salesmanUserID;
	
	
	//if refferal is active
	if(ReferralSingle.Status == 0) {
		$scope.showMarkAsSoldLostBtns = true;
		$scope.showSalesAddComments = true;
		$scope.ShowSalesManEdit = true;
	}
	
	//if referral is sold
	if(ReferralSingle.Status == 1) {
		$scope.ShowSoldDateSection = true;
		$scope.ShowReadOnlySalesComments = true;
		$scope.ShowReopenReferralButton = true;
		$scope.ShowMarkAsPaidBtn = true;
	}
	
	//if referral is paid
	if(ReferralSingle.Status == 2) {
		$scope.ShowSoldDateSection = true;
		$scope.ShowReferralCount = true;
		$scope.ShowReadOnlySalesComments = true;
	}
	
	//if referral is lost
	if(ReferralSingle.Status == 3) {
		$scope.ShowLostSection = true;	
		$scope.ShowReadOnlySalesComments = true;
		$scope.ShowReopenReferralButton = true;
	}
	
	if(ReferralSingle.referralSidDillonRelation == 1) {
		$scope.ShowWarningBar = true;
		$scope.isReferralMessage = true;
	}
	
	if(ReferralSingle.LostDataEntered == 1) {
		$scope.showLostFormData = true;
	}
	
	if(ReferralSingle.SoldDataEntered == 1) {
		$scope.ShowSoldInfoForm = true;	
		var dateFormat = ReferralSingle.isSoldDate.split("-");
		$scope.soldDate = dateFormat[1] + dateFormat[2] + dateFormat[0];
	}
	
	
	$scope.Salesman = ReferralSingle.firstName + " " + ReferralSingle.lastName
	
	$scope.getCurrentAge = function() {
		
		var unixTimeStampDay = ReferralSingle.submittedDate;
		var unixTimeStampHours = ReferralSingle.submittedTime;
		
		var unixTimeDifference = Date.now() - (Date.parse(unixTimeStampDay + " " + unixTimeStampHours))
		//1 day = 86400 seconds
		var DaysOld = unixTimeDifference / 86400 / 1000;
		
		return Math.round(DaysOld);
	}
	
	$scope.saveSalesMan = function(id) {
		Referrals.SaveSalesman(id, ReferralSingle.referralID).then(function(response) { 
			if(response.errorMessage) {
				$scope.SavedMessage = response.errorMessage;
			}
			
			if(response.success) {
				$scope.SavedMessage = "Salesperson has been reassigned"	;
				$timeout(function(){
					$scope.SavedMessage = "";
				}, 2000);	
			}
			
		});	
		
		
	}
	
	$scope.MarkLost = function() {
		$scope.showLostFormData = true;
		$scope.ShowSoldInfoForm = false;
	}
	
	$scope.MarkSold = function() {
		$scope.ShowSoldInfoForm = true;
		$scope.showLostFormData = false;
	}
		
	$scope.lostDataEntered = function(data) {
		if($window.confirm("Are you sure you want Mark this referral as Lost?")) {
			$scope.loadingLostData = true;
			Referrals.MarkAsLost(data, ReferralSingle.referralID).then(function(response) { 
				if(response.errorMessage) {
					$scope.LostDataMessage = $sce.trustAsHtml(response.errorMessage); 
					$scope.showLostDataMessage = true; 
					$scope.showLostSuccess = false;
				}
				if(response.success) {
					$timeout(function(){
					  ReferralSingle.Status = '3';
					  $scope.ShowMarkAsPaidBtn = false;
					  $scope.ShowMarkLostForm = false;
					  $scope.showSalesAddComments = false;
					  $scope.showMarkAsSoldLostBtns = false;
					  $scope.ShowLostSection = true;
					  ReferralSingle.LostNotes = response.success.lostNotes;
					  $scope.ShowReadOnlySalesComments = true;
					  $scope.showLostFormData = false;
					  $scope.ShowReopenReferralButton = true;
					  $scope.ShowSalesManEdit = false;
					});	
					$scope.showLostSuccess = true;
				}
				$scope.loadingLostData = false;
			});			
		}
		

	}
	
	$scope.reopenReferralStatus = function() {
		Referrals.ResetReferralStatus(ReferralSingle.referralID).then(function(response) { 
			if(response.success) {
				$timeout(function(){
					ReferralSingle.Status = '0';
					$scope.ShowReopenReferralButton = false;
					$scope.ShowLostSection = false;
					$scope.showMarkAsSoldLostBtns = true;
					$scope.ShowSalesManEdit = true;
					$scope.ShowSoldDateSection = false;
					$scope.ShowMarkAsPaidBtn = false;
				});
			}
		});	
	}
	
	$scope.soldDataEntered = function(data) {
		$scope.loadingSoldData = true;
		Referrals.MarkAsSold(data, ReferralSingle.referralID).then(function(response) { 
			if(response.errorMessage) {
				$scope.soldDateMessage = $sce.trustAsHtml(response.errorMessage); 
				$scope.showSoldDataMessage = true;
			}
			
			if(response.success) {
				$timeout(function(){
					$scope.ShowSoldInfoForm = false;
					ReferralSingle.Status = '1';
				 	$scope.ShowSoldDateSection = true;
					$scope.showMarkAsSoldLostBtns = false;
					$scope.ShowMarkAsPaidBtn = true;
					ReferralSingle.dealNumber = response.success.dealNumber;
					ReferralObject.soldStockNumber = response.success.soldStockNumber;
					ReferralObject.isSoldDate = response.success.isSoldDate;
					$scope.showSalesAddComments = false;
					$scope.ShowReadOnlySalesComments = true;
					$scope.ShowSalesManEdit = false;
					$scope.ShowReopenReferralButton = true;
				});
				
			}
			$scope.loadingSoldData = false;
		});	
	}
	
	
	$scope.MarkAsPaid = function() {
		$scope.MarkAsPaidPopup = true;	
	}
	
	$scope.hideMarkAsPaidPopup = function() {
		$scope.MarkAsPaidPopup = false;	
	}
	
	$scope.markAsPaidForm = function(data) {
		$scope.loadingPaid = true;
		Referrals.MarkAsPaid(ReferralSingle.referralID, ReferralSingle.memberID, data).then(function(response) { 
			if(response.errorMessage) {
				$scope.paidMessage = response.errorMessage; 
				$scope.showPaidMessage = true;
			}
			if(response.success) {
				$timeout(function(){
				  $scope.CurrentStatus = '2';
				  $scope.ShowMarkAsPaidBtn = false;
				  $scope.ShowReferralCount = true;
				  $scope.CurrentAmountEarned = response.success.earnedAmount;
				  $scope.WonCount = response.success.level;
				  $scope.ShowReopenReferralButton = false;
				  $scope.MarkAsPaidPopup = false;	
				});	
			}
			
			if(response.MySqlError) {
				$scope.showSoldMessage = false;
				$scope.errorSoldMessage = $sce.trustAsHtml(response.MySqlError);
				$scope.showSoldDataMessage = true;
			}
			$scope.loadingPaid = false;
		});
	}

}]);

SidDillonIntranet.controller('NonAdminStoreReferralsController', ['$scope', '$routeParams', 'ReferralsList', function($scope, $routeParams, ReferralsList) {
	$scope.storeName = ReferralsList.name;
	
	$scope.storeID = $routeParams.storeID;
	
	$scope.JanuaryPayout = Math.round(ReferralsList.January);
	$scope.FebruaryPayout = Math.round(ReferralsList.February);
	$scope.MarchPayout = Math.round(ReferralsList.March);
	$scope.AprilPayout = Math.round(ReferralsList.April);
	$scope.MayPayout = Math.round(ReferralsList.May);
	$scope.JunePayout = Math.round(ReferralsList.June);
	$scope.JulyPayout = Math.round(ReferralsList.July);
	$scope.AugustPayout = Math.round(ReferralsList.August);
	$scope.SeptemberPayout = Math.round(ReferralsList.September);
	$scope.OctoberPayout = Math.round(ReferralsList.October);
	$scope.NovemberPayout = Math.round(ReferralsList.November);
	$scope.DecemberPayout = Math.round(ReferralsList.December);
	
	$scope.JanuaryCount = ReferralsList.JanuaryCount;
	$scope.FebruaryCount = ReferralsList.FebruaryCount;
	$scope.MarchCount = ReferralsList.MarchCount;	
	$scope.AprilCount = ReferralsList.AprilCount;
	$scope.MayCount = ReferralsList.MayCount;
	$scope.JuneCount = ReferralsList.JuneCount;
	$scope.JulyCount = ReferralsList.JulyCount;
	$scope.AugustCount = ReferralsList.AugustCount;
	$scope.SeptemberCount = ReferralsList.SeptemberCount;
	$scope.OctoberCount = ReferralsList.OctoberCount;
	$scope.NovemberCount = ReferralsList.NovemberCount;
	$scope.DecemberCount = ReferralsList.DecemberCount;
	
	$scope.TotalPayoutByYear = parseInt(ReferralsList.January) + 
							   parseInt(ReferralsList.February) + 
							   parseInt(ReferralsList.March) + 
							   parseInt(ReferralsList.April) + 
							   parseInt(ReferralsList.May) + 
							   parseInt(ReferralsList.June) + 
							   parseInt(ReferralsList.July) + 
							   parseInt(ReferralsList.August) +
							   parseInt(ReferralsList.September) + 
							   parseInt(ReferralsList.October) +
							   parseInt(ReferralsList.November) +
							   parseInt(ReferralsList.December)
							   
	
	
	$scope.ReferralCount = function(Count) {
		return "Total Count: " + Count
	}
	
	//$scope.ReferralToolTipText = "Data based on this current year: Jan, 1 " + d.getFullYear() + " - Dec, 31 " + d.getFullYear();						   
							   
	var d = new Date();
	$scope.currentYear = d.getFullYear()
}]);

SidDillonIntranet.controller('NonAdminReferralMonthInfoController', ['$scope', '$routeParams', 'ReferralsByMonth', function($scope, $routeParams, ReferralsByMonth) {
	var d = new Date();
	$scope.CurrentYear = d.getFullYear();
	
	$scope.MonthSelected = $routeParams.month;
	$scope.storeID = $routeParams.storeID;
	
	$scope.Referrals = ReferralsByMonth;
	var d = new Date();
	
	$scope.calculateAge = function(submittedDate, submittedTime) {
		
		var unixTimeStampDay = submittedDate
		var unixTimeStampHours = submittedTime;
		
		var unixTimeDifference = Date.now() - (Date.parse(unixTimeStampDay + " " + unixTimeStampHours))
		//1 day = 86400 seconds
		var DaysOld = unixTimeDifference / 86400 / 1000;
		
		return Math.round(DaysOld);
	}
	
	$scope.currentDate = Date.now();
	
	
	// pagination
	$scope.curPage = 0;
	$scope.pageSize = 10;



	$scope.numberOfPages = function() {
 		return Math.ceil($scope.Referrals.length / $scope.pageSize);
 	};
	
	
}]);

SidDillonIntranet.controller('NonAdminReferralSingleController', ['$scope', '$routeParams', '$timeout', '$sce', '$window', 'ReferralSingle', 'Referrals', 'SalesmanByStore', function($scope, $routeParams, $timeout, $sce, $window, ReferralSingle, Referrals, SalesmanByStores) {
	$scope.MonthSelected = $routeParams.month
	$scope.storeID = $routeParams.storeID;
	$scope.ReferralObject = ReferralSingle;
	
	
	
	
	
	$scope.CurrentAmountEarned = parseInt(ReferralSingle.PaidAmount);
	
	$scope.showSalesAddComments = false;
	$scope.ShowReadOnlySalesComments = true;
	
	
	$scope.SaveSoldDataButton = "Mark As Sold"
	$scope.LostNotesSubmit = "Mark As Lost";
	$scope.LostNotes = ReferralSingle.LostNotes;
	
	$scope.Salesman = ReferralSingle.firstName + " " + ReferralSingle.lastName
	
	$scope.StoreSalesman = SalesmanByStores;
	
	$scope.referralSalesman = ReferralSingle.salesmanUserID;
	$scope.CurrentSalesman = ReferralSingle.salesmanUserID;
	
	
	if(ReferralSingle.notes) {
		$scope.showNotes = true;	
	}
	
	if(ReferralSingle.SoldDataEntered == 1) {
		$scope.ShowSoldInfoForm = true;	
		var dateFormat = ReferralSingle.isSoldDate.split("-");
		$scope.soldDate = dateFormat[1] + dateFormat[2] + dateFormat[0];
	}
	
	if(ReferralSingle.LostDataEntered == 1) {
		$scope.showLostFormData = true;
	}
	
	$scope.ReferralNotes = ReferralSingle.notes;
	
	$scope.getCurrentAge = function() {
		
		var unixTimeStampDay = ReferralSingle.submittedDate;
		var unixTimeStampHours = ReferralSingle.submittedTime;
		
		var unixTimeDifference = Date.now() - (Date.parse(unixTimeStampDay + " " + unixTimeStampHours))
		//1 day = 86400 seconds
		var DaysOld = unixTimeDifference / 86400 / 1000;
		
		return Math.round(DaysOld);
	}
	//if refferal is active
	if(ReferralSingle.Status == 0) {
		$scope.showMarkAsSoldLostBtns = true;
		$scope.ShowSalesManEdit = true;
	}	
	
	//if referral is sold
	if(ReferralSingle.Status == 1) {
		$scope.ShowSoldDateSection = true;
		$scope.ShowMarkAsPaidBtn = true;
		$scope.ShowSoldInfoForm = false;
		$scope.ShowReopenReferralButton = true;
	}
	
	//if referral is paid
	if(ReferralSingle.Status == 2) {
		$scope.ShowSoldDateSection = true;
		$scope.ShowReferralCount = true;
	}
	
	//if referral is lost
	if(ReferralSingle.Status == 3) {
		$scope.ShowLostSection = true;	
		$scope.showLostFormData = false;
		$scope.ShowReopenReferralButton = true;
	}
	
	if(ReferralSingle.referralSidDillonRelation == 1) {
		$scope.ShowWarningBar = true;
		$scope.isReferralMessage = true;
	}
	
	$scope.MarkLost = function() {
		$scope.showLostFormData = true;
		$scope.ShowSoldInfoForm = false;
	}
	
	$scope.MarkSold = function() {
		$scope.ShowSoldInfoForm = true;
		$scope.showLostFormData = false;
	}
	
	$scope.MarkAsPaid = function() {
		$scope.MarkAsPaidPopup = true;	
	}
	
	$scope.hideMarkAsPaidPopup = function() {
		$scope.MarkAsPaidPopup = false;	
	}
	
	$scope.markAsPaidForm = function(data) {
		$scope.loadingPaid = true;
		Referrals.MarkAsPaid(ReferralSingle.referralID, ReferralSingle.memberID, data).then(function(response) { 
			if(response.errorMessage) {
				$scope.paidMessage = response.errorMessage; 
				$scope.showPaidMessage = true;
			}
			if(response.success) {
				$timeout(function(){
				  $scope.CurrentStatus = '2';
				  $scope.ShowMarkAsPaidBtn = false;
				  $scope.ShowReferralCount = true;
				  $scope.CurrentAmountEarned = response.success.earnedAmount;
				  $scope.WonCount = response.success.level;
				  $scope.ShowReopenReferralButton = false;
				  $scope.MarkAsPaidPopup = false;	
				});	
			}
			
			if(response.MySqlError) {
				$scope.showSoldMessage = false;
				$scope.errorSoldMessage = $sce.trustAsHtml(response.MySqlError);
				$scope.showSoldDataMessage = true;
			}
			$scope.loadingPaid = false;
		});
	}
	
	$scope.referralSaleComments = function(data) {
		$scope.loadingFinalSaleComment = true;
		Referrals.SalesComments(ReferralSingle.referralID, data).then(function(response) { 
			if(response.success) {
				$scope.showSuccess = true;
				$scope.UpdatedMessage = response.success;
				$scope.showSaleCommentMessage = true;
			}
			
			if(response.MySqlError) {
				$scope.showSuccess = false;
				$scope.UpdatedMessage = response.MySqlError;
				$scope.showSaleCommentMessage = true;
			}
			
			
			$scope.loadingFinalSaleComment = false;
		});		
	}
	
	$scope.lostDataEntered = function(data) {
		if($window.confirm("Are you sure you want Mark this referral as Lost?")) {
			$scope.loadingLostData = true;
			Referrals.MarkAsLost(data, ReferralSingle.referralID).then(function(response) { 
				if(response.errorMessage) {
					$scope.LostDataMessage = $sce.trustAsHtml(response.errorMessage); 
					$scope.showLostDataMessage = true; 
					$scope.showLostSuccess = false;
				}
				if(response.success) {
					$timeout(function(){
					  $scope.CurrentStatus = '3';
					  $scope.ShowMarkAsPaidBtn = false;
					  $scope.ShowMarkLostForm = false;
					  $scope.showSalesAddComments = false;
					  $scope.showMarkAsSoldLostBtns = false;
					  $scope.ShowLostSection = true;
					  $scope.ReasonLostNotes = response.success.lostNotes;
					  $scope.ShowReadOnlySalesComments = true;
					  $scope.showLostFormData = false;
					  $scope.ShowReopenReferralButton = true;
					  $scope.ShowSalesManEdit = false;
					});	
					$scope.showLostSuccess = true;
				}
				$scope.loadingLostData = false;
			});			
		}
	}
	
	
	$scope.soldDataEntered = function(data) {
		$scope.loadingSoldData = true;
		Referrals.MarkAsSold(data, ReferralSingle.referralID).then(function(response) { 
			if(response.errorMessage) {
				$scope.soldDateMessage = $sce.trustAsHtml(response.errorMessage); 
				$scope.showSoldDataMessage = true;
			}
			
			if(response.success) {
				$timeout(function(){
					$scope.ShowSoldInfoForm = false;
					$scope.CurrentStatus = '1';
				 	$scope.ShowSoldDateSection = true;
					$scope.showMarkAsSoldLostBtns = false;
					$scope.ShowMarkAsPaidBtn = true;
					$scope.DealNumber = response.success.dealNumber;
					$scope.SoldStockNumber = response.success.soldStockNumber;
					$scope.SoldDate = response.success.isSoldDate;
					$scope.showSalesAddComments = false;
					$scope.ShowReadOnlySalesComments = true;
					$scope.ShowSalesManEdit = false;
					$scope.ShowReopenReferralButton = true;
				});
				
			}
			$scope.loadingSoldData = false;
		});	
	}
	
	$scope.saveSalesMan = function(id) {
		Referrals.SaveSalesman(id, ReferralSingle.referralID).then(function(response) { 
			if(response.errorMessage) {
				$scope.SavedMessage = response.errorMessage;
			}
			
			if(response.success) {
				$scope.SavedMessage = "Salesperson has been reassigned"	;
				$timeout(function(){
					$scope.SavedMessage = "";
				}, 2000);	
			}
		});	
	}
	
	$scope.reopenReferralStatus = function() {
		Referrals.ResetReferralStatus(ReferralSingle.referralID).then(function(response) { 
			if(response.success) {
				$timeout(function(){
					$scope.CurrentStatus = '0';
					$scope.ShowReopenReferralButton = false;
					$scope.ShowLostSection = false;
					$scope.showMarkAsSoldLostBtns = true;
					$scope.ShowSalesManEdit = true;
					$scope.ShowSoldDateSection = false;
					$scope.ShowMarkAsPaidBtn = false;
				});
			}
		});	
	}
	
}]);




SidDillonIntranet.controller('AddReferralUserController', ['$scope', '$sce', '$timeout', '$location', 'Stores', 'Users', function($scope, $sce, $timeout, $location, Stores, Users) {
	$scope.referralUsersSelected = true;
	$scope.storesList = Stores;
	
	$scope.referralUserFormSubmit = function(data) {
		$scope.loadingFinal = true;
		$scope.showSuccess = false;
		Users.NewReferralUser(data).then(function(response) { 
			if(response.errorMessage) {
				$scope.showMessage = true;
				$scope.errorMessage = response.errorMessage;
			}
			if(response.success) {
				$scope.showMessage = true;
				$scope.errorMessage = $sce.trustAsHtml(response.success);
				$scope.showSuccess = true;
				
				$timeout(function(){
					$location.path("/admin/settings/referral-users");
				}, 3000);		
			}
			
			if(response.MySqlError) {
				$scope.showMessage = true;
				$scope.errorMessage = response.MySqlError;
			}
			
			$scope.loadingFinal = false;
		});
	}
}]);

SidDillonIntranet.controller('EditReferralUserController', ['$scope', '$sce', '$timeout', 'Stores', 'UserSingle', 'Users', function($scope, $sce, $timeout, Stores, UserSingle, Users) {
	$scope.referralUsersSelected = true;
	$scope.storesList = Stores;
	$scope.UserFullName = UserSingle.firstName + " " + UserSingle.lastName;
	
	$scope.UserSingle = {};
	
	$scope.UserSingle.userFirstName = UserSingle.firstName;
	$scope.UserSingle.userLastName = UserSingle.lastName;
	$scope.UserSingle.userEmail = UserSingle.email;
	
	$scope.storeDropDown = UserSingle.store;
	
	$scope.currentStore = UserSingle.store;
	$scope.CurrentJobTitle = UserSingle.JobTitle;
	
	$scope.UserSingle.userNameLogin = UserSingle.userName;
	
	$scope.referralEditUserFormSubmit = function(data) {
		$scope.loadingFinal = true;
		$scope.showSuccess = false;
		Users.EditReferralUser(data, UserSingle.userID).then(function(response) {
			if(response.errorMessage) {
				$scope.showMessage = true;
				$scope.errorMessage = response.errorMessage;
			}
			if(response.success) {
				$scope.showMessage = true;
				$scope.errorMessage = "User Updated";
				$scope.showSuccess = true;
				
				$timeout(function(){
					$scope.UserFullName = response.success.firstName + " " + response.success.lastName
				});
				
			}
			if(response.MySqlError) {
				$scope.showMessage = true;
				$scope.errorMessage = response.MySqlError;
			}
			$scope.loadingFinal = false;
		});
	}
	
}]);

SidDillonIntranet.controller('IndividualSalesmanReferralsController', ['$scope', 'SalesmanReferrals', function($scope, SalesmanReferrals) {
	$scope.Referrals = SalesmanReferrals;
	
	$scope.calculateAge = function(submittedDate, submittedTime) {
		
		var unixTimeStampDay = submittedDate
		var unixTimeStampHours = submittedTime;
		
		var unixTimeDifference = Date.now() - (Date.parse(unixTimeStampDay + " " + unixTimeStampHours))
		//1 day = 86400 seconds
		var DaysOld = unixTimeDifference / 86400 / 1000;
		
		return Math.round(DaysOld);
	}
	
	// pagination
	$scope.curPage = 0;
	$scope.pageSize = 15;

	$scope.numberOfPages = function() {
 		return Math.ceil($scope.Referrals.length / $scope.pageSize);
 	};
 	
 	//based on this
 	//http://plnkr.co/edit/OTTAqykqGsJFrFyJYOjD?p=preview
 	$scope.monthSelectedFilter = function(element) {
 		console.log($scope.monthSelected);
 		var ReferralMonth = element.submittedDate.split('-');
 		if(!$scope.monthSelected) return true;
 		
 		return ReferralMonth[1] == $scope.monthSelected;
 	}
 	
 	var d = new Date();
 	$scope.CurrentYear =d.getFullYear()
 	
}]);

SidDillonIntranet.controller('IndividualSalesmanReferralSingleController', ['$scope', '$sce', '$filter', 'ReferralSingle', 'Referrals' ,function($scope, $sce, $filter, ReferralSingle, Referrals) {
	$scope.ReferralID = ReferralSingle.referralID;
	$scope.SubmittedDate = ReferralSingle.submittedDate;
	$scope.ShowReadOnlySalesman = true;
	$scope.Salesman = ReferralSingle.firstName + " " + ReferralSingle.lastName
	
	
	$scope.getCurrentAge = function() {
		
		var unixTimeStampDay = ReferralSingle.submittedDate;
		var unixTimeStampHours = ReferralSingle.submittedTime;
		
		var unixTimeDifference = Date.now() - (Date.parse(unixTimeStampDay + " " + unixTimeStampHours))
		//1 day = 86400 seconds
		var DaysOld = unixTimeDifference / 86400 / 1000;
		
		return Math.round(DaysOld);
	}
	
	$scope.CurrentStatus = ReferralSingle.Status;
	$scope.ReferredFirstName = ReferralSingle.FirstName + " " + ReferralSingle.LastName;
	$scope.ReferredPhone = ReferralSingle.phone;
	$scope.ReferredEmail = ReferralSingle.memberEmail;
	
	
	$scope.ReferredVehicle = ReferralSingle.DesiredVehicle;
	$scope.ReferredExpectedPurchase = ReferralSingle.expectedDatePurchase;
	$scope.ReferralNotes = ReferralSingle.notes;
		
	$scope.SidDillonMemberName = ReferralSingle.memberFirstName + " " + ReferralSingle.memberLastName;
	$scope.SidDillonClubID = "SD"+ReferralSingle.ClubID;
	
	$scope.WonCount = ReferralSingle.referralWinnerCount;
	$scope.CurrentAmountEarned = ReferralSingle.PaidAmount;
	
	$scope.DealNumber = ReferralSingle.dealNumber;
	$scope.SoldStockNumber = ReferralSingle.soldStockNumber;
	$scope.SoldDate = ReferralSingle.isSoldDate;
	$scope.SalesCommentsReadOnly = ReferralSingle.SaleComments;
	
	$scope.ReasonLostNotes = ReferralSingle.LostNotes;
	
	$scope.saleComments = ReferralSingle.SaleComments;
	
	$scope.dealNumber = ReferralSingle.dealNumber;
	$scope.soldStockNumber = ReferralSingle.soldStockNumber;
	
	$scope.LostNotes = ReferralSingle.LostNotes;
	
	$scope.ShowSalesmanSaleDataForm = true;
	
	$scope.Salesman = false;
	
	$scope.SaveSoldDataButton = "Save Sale Data"
	$scope.LostNotesSubmit = "Save Lost Reason";
	
	if(ReferralSingle.SoldDataEntered == 1) {
		$scope.ShowSoldInfoForm = true;	
		
		var dateFormat = ReferralSingle.isSoldDate.split("-");
		$scope.soldDate = dateFormat[1] + dateFormat[2] + dateFormat[0];
	}
	
	
	if(ReferralSingle.LostDataEntered == 1) {
		$scope.showLostFormData = true;
	}
	
	//$scope.soldDate = $filter('date')(ReferralSingle.isSoldDate, 'mediumDate');
	
	
	
	//if referral is active
	if(ReferralSingle.Status == 0) {
		$scope.showSalesAddComments = true;	
		$scope.showMarkAsSoldLostBtns = true;
	}
	
	//if referral is sold
	if(ReferralSingle.Status == 1) {
		$scope.ShowSoldDateSection = true;
		$scope.ShowReadOnlySalesComments = true;
		$scope.ShowSoldInfoForm = false;
	}
	
	//if referral is paid
	if(ReferralSingle.Status == 2) {
		$scope.ShowReferralCount = true;	
		$scope.ShowSoldDateSection = true;
		$scope.ShowReadOnlySalesComments = true;
	}

	
	//if referral is inactive
	if(ReferralSingle.Status == 3) {
		$scope.ShowLostSection = true;
		$scope.ShowReadOnlySalesComments = true;
	}
	
	
	
	$scope.referralSaleComments = function(data) {
		$scope.loadingFinalSaleComment = true;
		Referrals.SalesComments(ReferralSingle.referralID, data).then(function(response) { 
			if(response.success) {
				$scope.showSuccess = true;
				$scope.UpdatedMessage = response.success;
				$scope.showSaleCommentMessage = true;
			}
			
			if(response.MySqlError) {
				$scope.showSuccess = false;
				$scope.UpdatedMessage = response.MySqlError;
				$scope.showSaleCommentMessage = true;
			}
			
			
			$scope.loadingFinalSaleComment = false;
		});		
	}
	

	$scope.MarkSold = function() {
		$scope.ShowSoldInfoForm = true;
		$scope.showLostFormData = false;
	}
	
	$scope.MarkLost= function() {
		$scope.ShowSoldInfoForm = false;
		$scope.showLostFormData = true;
	}
	
	$scope.soldDataEntered = function(data) {
		$scope.loadingSoldData = true;
		Referrals.SalesmanSoldData(data, ReferralSingle.referralID).then(function(response) { 
			if(response.errorMessage) {
				$scope.showSuccess = false;
				$scope.showSoldDataMessage = true;
				$scope.soldDateMessage = $sce.trustAsHtml(response.errorMessage);
			}
			if(response.success) {
				$scope.showSuccess = true;
				$scope.showSoldDataMessage = true;
				$scope.soldDateMessage = $sce.trustAsHtml(response.success);
				
			}
			
			if(response.MySqlError) {
				//$scope.showSuccess = false;
				//$scope.UpdatedMessage = response.MySqlError;
				//$scope.showSaleCommentMessage = true;
			}
			
			
			$scope.loadingSoldData = false;
		});		
	}
	
	
	$scope.lostDataEntered = function(data) {
		$scope.loadingLostData = true;
		$scope.showLostDataMessage = true;
		Referrals.SalesmanLostData(data, ReferralSingle.referralID).then(function(response) { 
			if(response.errorMessage) {
				$scope.showSoldDataMessage = true;
				$scope.LostDataMessage = $sce.trustAsHtml(response.errorMessage);
			}
			
			if(response.success) {
				$scope.showLostSuccess = true;
				$scope.LostDataMessage = $sce.trustAsHtml(response.success);
			}
			
			if(response.MySqlError) {
				$scope.showLostSuccess = false;
				$scope.LostDataMessage = $sce.trustAsHtml(response.MySqlError);
				//$scope.showSoldDataMessage = true;
			}
			
			
			$scope.loadingLostData = false;
		});	
	}
	
	
	
}]);

SidDillonIntranet.controller('settingsReferralTierController', ['$scope', '$timeout', 'Tiers', 'Referrals', function($scope, $timeout, Tiers, Referrals) {
	$scope.settingsReferralTierSelected = true;
	$scope.ReferralTiers = Tiers;
	
	var tierLevelName = [];
	var tierLevelPrice = [];
				
	for (var key in Tiers) {
		tierLevelName.push(Tiers[key].Level);
		tierLevelPrice.push(Tiers[key].Amount);	
	}
				
	$scope.tierLevelName = tierLevelName
	$scope.tierLevelPrice = tierLevelPrice;
	
	//adaption of this
	//http://jsfiddle.net/odiseo/twSFK/7/
	var CurrentIndex;
	$scope.EditTierLevelClick = function(selectedIndex) {
		CurrentIndex = selectedIndex;
	}
	
	$scope.editTier = function(index) {
		return CurrentIndex === index;
	}
	
	
	$scope.tierLevelEdit = function(data, id, index) {
		$scope.loadingFinal = true;
		Referrals.SaveTier(data, id).then(function(response) { 
			
			if(response.success) {
				$scope.showMessage = true;
				$scope.errorMessage = response.success.msg;
				$scope.showSuccess = true;
				
				$timeout(function(){
					CurrentIndex = '';
					Tiers[index].Level = response.success.NewTierName;
					Tiers[index].Amount = response.success.NewAmount;
				});
			}
			
			if(response.MySqlError) {
				$scope.showMessage = true;
				$scope.errorMessage = response.MySqlError
			}
			
			$scope.EditSingleTier = '';
			$scope.loadingFinal = false;
		});
	}
	
}]);

SidDillonIntranet.controller('AllReferralMembersController', ['$scope', '$timeout', 'Referrals', 'AllReferralMembers', function($scope, $timeout, Referrals, AllReferralMembers) {
	$scope.settingsAllReferralMembersSelected = true;
	$scope.ReferralMembers = AllReferralMembers
	
 	$scope.ActiveStatus = "";
 	
 	//based on this
 	//http://plnkr.co/edit/OTTAqykqGsJFrFyJYOjD?p=preview
 	$scope.FilterActiveStatus = function(element) {
 		console.log("value: " + $scope.memberStatus);
 		console.log('active status:' + element.ActiveState);
 		if(!$scope.memberStatus) return true;
 		return element.ActiveState == $scope.memberStatus;
 	}
	
	
	
	// pagination
	$scope.curPage = 0;
	$scope.pageSize = 15;

	$scope.numberOfPages = function() {
 		return Math.ceil($scope.ReferralMembers.length / $scope.pageSize);
 	};
	
	var referralMember;
	var indexValue;
	
	$scope.deactivateUser = function(id, index) {
		referralMember = id;
		indexValue = index;
		$scope.DeactivateUserPopup = true;
	}
	
	$scope.hideDeactivateUserForm = function() {
		$scope.DeactivateUserPopup = false;
	}
	
	$scope.deactiveUserForm = function(data) {
		$scope.loadingDeactivateFinal= true;
		Referrals.DeactivateUser(data, referralMember).then(function(data) { 
			if(data.errorMessage) {
				$scope.showDeactivateMessage = true;
				$scope.errorDeactivateMessage = data.errorMessage;
			}
			if(data.success) {
				$scope.DeactivateUserPopup = false;
				$timeout(function(){
					AllReferralMembers[indexValue].ActiveState = 0;
				});
				
			}
			
			$scope.loadingDeactivateFinal = false;
		
		});	
	}
	
	
}]);

SidDillonIntranet.controller('ReferralMemberController', ['$scope', '$timeout', 'MemberSingle', 'PaidReferrals', 'ActiveReferrals', 'InactiveReferrals', 'Referrals', function($scope, $timeout, MemberSingle, PaidReferrals, ActiveReferrals, InactiveReferrals, Referrals) {
	$scope.settingsAllReferralMembersSelected = true;
	
	$scope.MemberSingle = {};
	
	$scope.memberFullName = MemberSingle.memberFirstName + " " + MemberSingle.memberLastName; 
	$scope.ClubID = MemberSingle.ClubID
	
	$scope.MemberSingle.memberFirstName = MemberSingle.memberFirstName;
	$scope.MemberSingle.memberLastName = MemberSingle.memberLastName;
	$scope.MemberSingle.memberEmail = MemberSingle.memberEmail;
	$scope.MemberSingle.memberPhone = MemberSingle.phone;
	
	$scope.MemberSingle.memberAddress = MemberSingle.address;
	$scope.MemberSingle.memberCity = MemberSingle.city;
	$scope.MemberSingle.memberZip = MemberSingle.zip;
	
	$scope.State = MemberSingle.state;
	$scope.PaidReferrals = PaidReferrals;
	
	var GrandTotal = 0;
	
	for (var key in PaidReferrals) {
		GrandTotal += parseInt(PaidReferrals[key].PaidAmount);
	}
	
	if(MemberSingle.isSidDillonRelation == 1) {
		$scope.ShowWarningBar = true;
		$scope.isReferralMember = true;
		$scope.MemberSingle.isSidDillonEmployee = true;
	}
	
	$scope.MemberSingle.deactiveStatus = MemberSingle.DeactiveStatusReason;
	
	$scope.GrandTotalNumber = GrandTotal;
	$scope.ActiveReferrals = ActiveReferrals;
	$scope.InactiveReferrals = InactiveReferrals;
	
	$scope.referralMemberSubmit = function(data) {
		
		$scope.loadingFinal = true;
		Referrals.AdminSaveMember(data, MemberSingle.ClubID).then(function(response) {
			if(response.errorMessage) {
				$scope.showMessage = true;
				$scope.errorMessage = response.errorMessage;
				$scope.showSuccess = false;
			}
			if(response.success) {
				$scope.showMessage = true;
				$scope.errorMessage = response.success.msg;
				$scope.showSuccess = true;
				
				$timeout(function(){
					if(response.success.SidDillonEmployee == 1) {
						$scope.ShowWarningBar = true;
						$scope.isReferralMember = true;
					} else {
						$scope.ShowWarningBar = false;
					}
					
					if(response.success.ActiveMember == 1) {
						$scope.ActiveStatus = "Active";
						$scope.MemberSingle.deactiveStatus = "";
					} else {
						$scope.ActiveStatus = "Disabled";
					}
					
				});
				
			}
			if(response.MySqlError) {
				//$scope.showMessage = true;
				//$scope.errorMessage = response.MySqlError;
			}
			$scope.loadingFinal = false;
		});
		
	}
	
	if(MemberSingle.ActiveState == 1) {
		$scope.ActiveStatus = "Active";
		$scope.MemberSingle.isActiveStatus = 1;
		$scope.ShowDeactiveReason = false;
	} else {
		$scope.ActiveStatus = "Disabled";
		$scope.MemberSingle.isActiveStatus = 0;
		$scope.ShowDeactiveReason = true;
	}
	
	
	$scope.ShowDeactiveChange = function(element) {
		if(element == 0) {
			$scope.ShowDeactiveReason = true;
		} else {
			$scope.ShowDeactiveReason = false;
		}		
	}	
	
	
}]);

SidDillonIntranet.controller('AddReferralMemberController', ['$scope', '$timeout', '$location', '$sce', 'Referrals', function($scope, $timeout, $location, $sce, Referrals) {
	$scope.settingsAllReferralMembersSelected = true;
	$scope.newReferralMemberSubmit = function(data) {
		$scope.loadingFinal = true;
		Referrals.NewMember(data).then(function(response) { 
			
			if(response.errorMessage) {
				$scope.errorMessage = $sce.trustAsHtml(response.errorMessage); 
				$scope.showMessage = true;
			}
			
			if(response.success) {
				$scope.showMessage = true; 
				$scope.showSuccess = true;
				$scope.errorMessage = $sce.trustAsHtml("<strong>Account Created</strong><br />An email will be sent to the new user to create their credentails");
				
				$timeout(function(){
					$location.path('/admin/settings/referral-members');
				}, 3000);
				
			}
			$scope.loadingFinal = false;
		});	
		
	}
}]);



SidDillonIntranet.controller('ReferralsReportingController', ['$scope', 'System', 'ReferralStores', function($scope, System, ReferralStores) {
	$scope.referralReportingSelected = true;
	
	$scope.ReferralStores = ReferralStores;
	
	$scope.referralReportingCreate = function(data) {
		$scope.loadingFinal = true;
		System.ReferralReporting(data).then(function(response) { 
			if(response.errorMessage) {
				$scope.errorMessage = response.errorMessage; 
				$scope.showMessage = true;
				$scope.showSuccess = false;
			}
			if(response.success) {
				$scope.errorMessage = "Report Created";
				$scope.showMessage = true;
				$scope.showSuccess = true;
				window.open(CurrentAppPath + response.success.NewFileDownload, '_blank', '');
			}
			
			$scope.loadingFinal = false;
		});
	}
	
}]);


